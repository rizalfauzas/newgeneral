/*
 Navicat Premium Data Transfer

 Source Server         : SQL
 Source Server Type    : SQL Server
 Source Server Version : 15004236
 Source Host           : 127.0.0.1:1433
 Source Catalog        : dbGeneral
 Source Schema         : dbo

 Target Server Type    : SQL Server
 Target Server Version : 15004236
 File Encoding         : 65001

 Date: 28/11/2023 15:34:58
*/


-- ----------------------------
-- Table structure for groups
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[groups]') AND type IN ('U'))
	DROP TABLE [dbo].[groups]
GO

CREATE TABLE [dbo].[groups] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [groups] varchar(100) COLLATE Latin1_General_CI_AS  NULL
)
GO

ALTER TABLE [dbo].[groups] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of groups
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[groups] ON
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1', N'Administrator')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'2', N'Staff')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'3', N'Atasan')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1002', N'Cabang')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1003', N'Area')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1004', N'Region')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1005', N'BSI')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1006', N'Asuransi')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1007', N'Keuangan')
GO

INSERT INTO [dbo].[groups] ([id], [groups]) VALUES (N'1008', N'Akutansi')
GO

SET IDENTITY_INSERT [dbo].[groups] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for menu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[menu]') AND type IN ('U'))
	DROP TABLE [dbo].[menu]
GO

CREATE TABLE [dbo].[menu] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [nama_menu] varchar(100) COLLATE Latin1_General_CI_AS  NULL,
  [icon] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [link] varchar(50) COLLATE Latin1_General_CI_AS  NULL,
  [kat_menu] tinyint  NULL,
  [status] tinyint  NULL
)
GO

ALTER TABLE [dbo].[menu] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[menu] ON
GO

INSERT INTO [dbo].[menu] ([id], [nama_menu], [icon], [link], [kat_menu], [status]) VALUES (N'1', N'Beranda', N'bx bx-tachometer', N'beranda', N'0', N'1')
GO

INSERT INTO [dbo].[menu] ([id], [nama_menu], [icon], [link], [kat_menu], [status]) VALUES (N'2', N'Produksi', N'bx bx-paste', N'produksi', N'0', N'1')
GO

SET IDENTITY_INSERT [dbo].[menu] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for produksi
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[produksi]') AND type IN ('U'))
	DROP TABLE [dbo].[produksi]
GO

CREATE TABLE [dbo].[produksi] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [status_produksi] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tgl_nota] date  NULL,
  [jenis_asuransi] int  NULL,
  [tertanggung] int  NULL,
  [asuransi_leader] int  NULL,
  [no_polis] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [jangka_waktu_dari] date  NULL,
  [jangka_waktu_sampai] date  NULL,
  [rate_ppn] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [gross_premi_rate] float(53)  NULL,
  [gross_premi] float(53)  NULL,
  [diskon_asd_persen] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [diskon_asd] float(53)  NULL,
  [materai] float(53)  NULL,
  [biaya_polis] float(53)  NULL,
  [biaya_admin_broker] float(53)  NULL,
  [jumlah_sebelum_admin_broker] float(53)  NULL,
  [jumlah_setelah_admin_broker] float(53)  NULL,
  [premi_after_diskon] float(53)  NULL,
  [keterangan_tambahan_pembayaran_di_kwitansi] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [gross_broker_rate] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [gross_broker] float(53)  NULL,
  [diskon_broker_persen] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [diskon_broker] float(53)  NULL,
  [wpc_tertanggung] int  NULL,
  [wpc_asuransi] int  NULL,
  [net_brokerage] float(53)  NULL,
  [premi_to_ceding] float(53)  NULL,
  [unit_bisnis] int  NULL,
  [account_officer] int  NULL,
  [account_invoice] int  NULL,
  [status] int  NULL,
  [tsi] float(53)  NULL,
  [keterangan] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [currency] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [premi_after_diskon_broker] float(53)  NULL,
  [no_nota] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [create_by] int  NULL,
  [create_date] datetime2(7)  NULL
)
GO

ALTER TABLE [dbo].[produksi] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of produksi
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[produksi] ON
GO

SET IDENTITY_INSERT [dbo].[produksi] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for produksi_rincianAsuransi
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[produksi_rincianAsuransi]') AND type IN ('U'))
	DROP TABLE [dbo].[produksi_rincianAsuransi]
GO

CREATE TABLE [dbo].[produksi_rincianAsuransi] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [asuransi] int  NULL,
  [share] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tsi] float(53)  NULL,
  [gross_premi] float(53)  NULL,
  [diskon] float(53)  NULL,
  [premi] float(53)  NULL,
  [fee_broker] float(53)  NULL,
  [gross_brokerage] float(53)  NULL,
  [hut_premi] float(53)  NULL,
  [biaya_polis] float(53)  NULL,
  [biaya_materai] float(53)  NULL,
  [jumlah_sebelum_pajak] float(53)  NULL,
  [ppn_asli] float(53)  NULL,
  [ppn] float(53)  NULL,
  [pph_23] float(53)  NULL,
  [net_premi] float(53)  NULL,
  [jn_pajak] float(53)  NULL,
  [dpp] float(53)  NULL,
  [nilai_klaim] float(53)  NULL,
  [keterangan_klaim] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [create_date] datetime  NULL,
  [create_by] int  NULL
)
GO

ALTER TABLE [dbo].[produksi_rincianAsuransi] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of produksi_rincianAsuransi
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[produksi_rincianAsuransi] ON
GO

SET IDENTITY_INSERT [dbo].[produksi_rincianAsuransi] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for produksi_tsi
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[produksi_tsi]') AND type IN ('U'))
	DROP TABLE [dbo].[produksi_tsi]
GO

CREATE TABLE [dbo].[produksi_tsi] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [id_produksi] int  NULL,
  [no_polis] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [nama] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [tanggal_awal] date  NULL,
  [tanggal_akhir] date  NULL,
  [tsi] float(53)  NULL,
  [premi_rate] float(53)  NULL,
  [premi_gross] float(53)  NULL,
  [asd_diskon_persen] float(53)  NULL,
  [asd_diskon] float(53)  NULL,
  [premi] float(53)  NULL,
  [broker_diskon_persen] float(53)  NULL,
  [broker_diskon] float(53)  NULL,
  [by_polis] float(53)  NULL,
  [materai] float(53)  NULL,
  [adm_broker] float(53)  NULL,
  [jumlah] float(53)  NULL,
  [create_date] datetime2(7)  NULL,
  [create_by] int  NULL
)
GO

ALTER TABLE [dbo].[produksi_tsi] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of produksi_tsi
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[produksi_tsi] ON
GO

SET IDENTITY_INSERT [dbo].[produksi_tsi] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for rolemenu
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[rolemenu]') AND type IN ('U'))
	DROP TABLE [dbo].[rolemenu]
GO

CREATE TABLE [dbo].[rolemenu] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [id_menu] int  NULL,
  [id_groups] int  NULL
)
GO

ALTER TABLE [dbo].[rolemenu] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of rolemenu
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[rolemenu] ON
GO

INSERT INTO [dbo].[rolemenu] ([id], [id_menu], [id_groups]) VALUES (N'1', N'1', N'1')
GO

INSERT INTO [dbo].[rolemenu] ([id], [id_menu], [id_groups]) VALUES (N'2', N'2', N'1')
GO

SET IDENTITY_INSERT [dbo].[rolemenu] OFF
GO

COMMIT
GO


-- ----------------------------
-- Table structure for users
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[users]') AND type IN ('U'))
	DROP TABLE [dbo].[users]
GO

CREATE TABLE [dbo].[users] (
  [id] int  IDENTITY(1,1) NOT NULL,
  [nama] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [username] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [password] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [id_groups] int  NULL,
  [status] int  NULL,
  [create_by] int  NULL,
  [create_date] datetime2(7)  NULL,
  [idbank] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [branch_code] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [code_area] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [code_region] varchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [asuransi] int  NULL
)
GO

ALTER TABLE [dbo].[users] SET (LOCK_ESCALATION = TABLE)
GO


-- ----------------------------
-- Records of users
-- ----------------------------
BEGIN TRANSACTION
GO

SET IDENTITY_INSERT [dbo].[users] ON
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'2', N'sabri', N'sabri', N'haha', N'2', N'1', N'1', NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'1003', N'Administrator', N'admin', N'haha', N'1', N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6320', N'KCP GUNUNG RAYA KANDIS', N'BSI-18256', N'12345678', N'1003', N'1', NULL, NULL, NULL, N'18256', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6399', N'KCP PONDOK CILEGON INDAH', N'BSI-10629', N'12345678', N'1004', N'1', NULL, NULL, NULL, N'10629', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6478', N'KCP BANDUNG RANCAEKEK 1', N'BSI-10360', N'12345678', N'1005', N'1', NULL, NULL, NULL, N'10360', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6558', N'KCP TULUNGAGUNG', N'BSI-10202', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10202', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6639', N'KCP SAPE', N'BSI-18304', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18304', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6720', N'KCP POSO', N'BSI-10601', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10601', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6802', N'KCP SAWAHLUNTO', N'BSI-10615', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10615', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6882', N'KCP BALIKPAPAN BARU', N'BSI-10296', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10296', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6962', N'KCP BANJARNEGARA S PARMAN', N'BSI-10348', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10348', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7041', N'KCP JKT FATMAWATI', N'BSI-10067', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10067', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7118', N'KC TANGERANG BSD PAHLAWAN SERIBU', N'BSI-19012', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19012', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7198', N'KCP BEUREUNEUN 2', N'BSI-19287', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19287', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7280', N'KCP RANTAU PANJANG 1', N'BSI-19375', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19375', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7286', N'RAMAYANA', N'ramayana', N'12345678', N'1006', N'1', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7287', N'RAMAYANA', N'ramayana', N'12345678', N'1006', N'1', NULL, NULL, NULL, NULL, NULL, NULL, N'7')
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6239', N'KC BEKASI', N'BSI-10007', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10007', N'17', N'1', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6240', N'KCP PINRANG', N'BSI-19213', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19213', N'46', N'2', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6241', N'KC MEDAN KOTA', N'BSI-10008', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10008', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6242', N'KC PEKANBARU', N'BSI-10022', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10022', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6243', N'KC RANTAU PRAPAT', N'BSI-10030', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10030', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6244', N'KC BATAM', N'BSI-10034', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10034', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6245', N'KC PADANGSIDEMPUAN', N'BSI-10043', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10043', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6246', N'KC DUMAI', N'BSI-10044', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10044', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6247', N'KC BINJAI', N'BSI-10047', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10047', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6248', N'KC PEMATANGSIANTAR', N'BSI-10049', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10049', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6249', N'KC TANJUNG PINANG', N'BSI-10059', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10059', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6250', N'KCP TEBING TINGGI', N'BSI-10087', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10087', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6251', N'KCP MEDAN KAMPUNG BARU', N'BSI-10088', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10088', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6252', N'KCP MEDAN AKSARA', N'BSI-10089', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10089', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6253', N'KCP MEDAN SETIA BUDI', N'BSI-10090', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10090', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6254', N'KC PEKANBARU HARAPAN RAYA', N'BSI-10147', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10147', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6255', N'KCP PEKANBARU PANAM', N'BSI-10148', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10148', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6256', N'KCP PANGKALAN KERINCI', N'BSI-10149', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10149', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6257', N'KCP TEMBILAHAN', N'BSI-10150', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10150', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6258', N'KCP STABAT', N'BSI-10158', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10158', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6259', N'KCP PANGKALAN BRANDAN', N'BSI-10159', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10159', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6260', N'KCP TANJUNG BALAI KARIMUN', N'BSI-10172', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10172', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6261', N'KCP PANYABUNGAN', N'BSI-10191', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10191', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6262', N'KCP SIBUHUAN', N'BSI-10192', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10192', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6263', N'KC DURI', N'BSI-10193', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10193', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6264', N'KCP BAGAN BATU', N'BSI-10194', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10194', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6265', N'KCP KISARAN', N'BSI-10204', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10204', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6266', N'KCP TANJUNG BALAI', N'BSI-10206', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10206', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6267', N'KCP MEDAN GATOT SUBROTO', N'BSI-10213', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10213', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6268', N'KCP MEDAN SIMPANG LIMUN', N'BSI-10214', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10214', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6269', N'KCP KOTA PINANG', N'BSI-10223', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10223', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6270', N'KCP BATAMINDO', N'BSI-10224', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10224', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6271', N'KCP BENGKALIS', N'BSI-10227', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10227', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6272', N'KCP TANJUNG UBAN', N'BSI-10228', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10228', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6273', N'KCP MEDAN KRAKATAU', N'BSI-10266', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10266', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6274', N'KCP MEDAN BELAWAN', N'BSI-10267', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10267', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6275', N'KCP MEDAN TOMANG ELOK', N'BSI-10268', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10268', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6276', N'KCP MEDAN ISKANDAR MUDA', N'BSI-10269', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10269', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6277', N'KCP MEDAN PULO BRAYAN', N'BSI-10270', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10270', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6278', N'KCP UJUNG BATU', N'BSI-10293', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10293', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6279', N'KCP PEKANBARU NANGKA', N'BSI-10294', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10294', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6280', N'KCP AEK KANOPAN', N'BSI-10306', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10306', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6281', N'KCP BATAM BATU AJI', N'BSI-10312', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10312', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6282', N'KCP BATAM CENTER', N'BSI-10313', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10313', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6283', N'KCP GUNUNG TUA', N'BSI-10327', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10327', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6284', N'KCP SIPIROK', N'BSI-10328', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10328', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6285', N'KCP INDRAPURA', N'BSI-10333', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10333', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6286', N'KCP NATUNA', N'BSI-10342', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10342', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6287', N'KC MEDAN RAYA', N'BSI-10353', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10353', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6288', N'KC SIBOLGA', N'BSI-10356', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10356', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6289', N'KC LUBUK PAKAM', N'BSI-10361', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10361', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6290', N'KCP BATAM BENGKONG', N'BSI-10417', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10417', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6291', N'KCP DUMAI SUKAJADI', N'BSI-10423', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10423', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6292', N'KCP PEKANBARU SUDIRMAN', N'BSI-10446', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10446', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6293', N'KCP BINTAN CENTER', N'BSI-10475', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10475', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6294', N'KCP TELUK KUANTAN', N'BSI-10484', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10484', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6295', N'KCP MEDAN MARELAN RAYA', N'BSI-10486', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10486', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6296', N'KCP BANGKINANG', N'BSI-10504', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10504', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6297', N'KCP MEDAN SUKARAMAI', N'BSI-10507', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10507', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6298', N'KCP FLAMBOYAN', N'BSI-10508', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10508', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6299', N'KCP KABANJAHE', N'BSI-10539', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10539', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6300', N'KCP MEDAN MUCHTAR BASRI', N'BSI-10544', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10544', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6301', N'KCP MEDAN PADANG BULAN', N'BSI-10545', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10545', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6302', N'KCP SELAT PANJANG', N'BSI-10546', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10546', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6303', N'KCP BAGAN SIAPIAPI', N'BSI-10549', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10549', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6304', N'KCP RENGAT', N'BSI-10550', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10550', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6305', N'KCP ANAMBAS', N'BSI-10568', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10568', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6306', N'KCP PERBAUNGAN', N'BSI-10613', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10613', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6307', N'KCP MEDAN RINGROAD', N'BSI-10616', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10616', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6308', N'KCP SIAK', N'BSI-10667', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10667', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6309', N'KCP BATAM BOTANIA GARDEN', N'BSI-10672', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10672', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6310', N'KCP MEDAN ADAM MALIK', N'BSI-18015', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18015', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6311', N'KCP PEKANBARU SUDIRMAN ATAS', N'BSI-18019', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18019', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6312', N'KCP BATAM RADEN PATAH', N'BSI-18031', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18031', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6313', N'KCP PANAM RAYA', N'BSI-18060', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18060', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6314', N'KCP MEDAN JUANDA', N'BSI-18114', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18114', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6315', N'KCP MEDAN IMAM BONJOL', N'BSI-18116', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18116', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6316', N'KCP PEMATANGSIANTAR', N'BSI-18117', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18117', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6317', N'KCP PANGKALAN KERINCI MAHARAJA', N'BSI-18137', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18137', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6318', N'KCP BATU AJI', N'BSI-18192', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18192', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6319', N'KCP TANJUNG BALAI KARIMUN 2', N'BSI-18193', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18193', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6640', N'KCP SURABAYA MERR 2', N'BSI-19006', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19006', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6641', N'KCP MALANG SOETTA', N'BSI-19021', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19021', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6642', N'KC KEDIRI HASSANUDIN', N'BSI-19022', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19022', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6643', N'KCP MATARAM PEJANGGIK 2', N'BSI-19023', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19023', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6644', N'KCP SURABAYA DIPONEGORO 2', N'BSI-19037', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19037', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6645', N'KCP DENPASAR MAHENDRADATTA', N'BSI-19042', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19042', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6646', N'KCP SIDOARJO AHMAD YANI', N'BSI-19056', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19056', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6647', N'KCP GRESIK DR SUTOMO', N'BSI-19058', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19058', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6648', N'KC PASURUAN SUDIRMAN', N'BSI-19059', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19059', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6649', N'KCP JOMBANG WAHID HASYIM', N'BSI-19060', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19060', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6650', N'KCP BANYUWANGI A YANI', N'BSI-19061', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19061', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6651', N'KCP JEMBER UNIVERSITAS JEMBER', N'BSI-19066', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19066', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6652', N'KCP MADIUN S PARMAN', N'BSI-19067', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19067', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6653', N'KC BIMA SOETTA 2', N'BSI-19073', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19073', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6654', N'KCP SIDOARJO GATEWAY', N'BSI-19086', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19086', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6655', N'KCP SURABAYA HR MUHAMMAD', N'BSI-19096', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19096', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6656', N'KCP BANGKALAN TRUNOJYO', N'BSI-19097', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19097', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6657', N'KCP MALANG KEPANJEN SULTAN AGUNG', N'BSI-19115', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19115', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6658', N'KCP MOJOKERTO MOJOPAHIT 2', N'BSI-19121', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19121', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6659', N'KCP JOMBANG MOJOAGUNG', N'BSI-19146', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19146', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6660', N'KCP MALANG PAKIS KEMBAR', N'BSI-19148', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19148', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6661', N'KCP BANYUWANGI ROGOJAMPI 2', N'BSI-19149', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19149', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6662', N'KCP BANYUWANGI PURWOHARJO', N'BSI-19150', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19150', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6663', N'KCP LOMBOK SELONG', N'BSI-19153', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19153', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6664', N'KCP JOMBANG PLOSO', N'BSI-19164', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19164', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6665', N'KCP MOJOKERTO MOJOSARI', N'BSI-19165', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19165', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6666', N'KCP GRESIK MENGANTI', N'BSI-19166', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19166', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6667', N'KCP MALANG BULULAWANG', N'BSI-19168', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19168', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6668', N'KCP NGANJUK YOS SUDARSO', N'BSI-19169', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19169', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6669', N'KCP KEDIRI PARE LAWU', N'BSI-19170', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19170', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6670', N'KCP KEDIRI TENDEAN', N'BSI-19171', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19171', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6671', N'KCP BLITAR TANJUNG', N'BSI-19205', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19205', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6672', N'KCP MAGETAN MT HARYONO 2', N'BSI-19206', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19206', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6673', N'KCP PONOROGO COKROAMINOTO', N'BSI-19208', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19208', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6674', N'KCP LUMAJANG IMAM BONJOL', N'BSI-19215', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19215', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6675', N'KCP LOMBOK AIKMEL', N'BSI-19216', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19216', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6676', N'KCP LOMBOK PRAYA', N'BSI-19217', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19217', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6677', N'KCP SURABAYA MULYOSARI', N'BSI-19237', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19237', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6678', N'KCP BOJONEGORO SUROPATI', N'BSI-19239', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19239', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6679', N'KCP LAMONGAN WAHIDIN', N'BSI-19240', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19240', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6680', N'KCP PAMEKASAN JOKOTOLE', N'BSI-19242', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19242', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6681', N'KCP SURABAYA PERAK', N'BSI-19243', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19243', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6682', N'KCP PROBOLINGGO SUDIRMAN', N'BSI-19270', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19270', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6683', N'KCP SINGARAJA UDAYANA', N'BSI-19273', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19273', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6684', N'KCP BERTAIS MANDALIKA', N'BSI-19382', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19382', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6685', N'KCP PANDAAN SQUARE', N'BSI-19385', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19385', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6686', N'KC MAKASSAR', N'BSI-10015', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10015', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6687', N'KC PALU', N'BSI-10038', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10038', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6688', N'KC MANADO', N'BSI-10052', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10052', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6689', N'KC JAYAPURA', N'BSI-10054', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10054', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6690', N'KC GORONTALO', N'BSI-10062', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10062', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6691', N'KC TERNATE', N'BSI-10064', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10064', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6692', N'KC MAMUJU', N'BSI-10065', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10065', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6693', N'KC BONE', N'BSI-10118', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10118', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6694', N'KCP MAKASSAR PANAKUKKANG', N'BSI-10119', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10119', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6695', N'KCP BULUKUMBA', N'BSI-10120', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10120', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6696', N'KCP BARRU', N'BSI-10121', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10121', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6697', N'KCP PALOPO', N'BSI-10122', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10122', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6698', N'KC LUWUK', N'BSI-10184', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10184', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6699', N'KCP PARIGI MOUTONG', N'BSI-10185', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10185', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6700', N'KCP BITUNG', N'BSI-10257', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10257', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6701', N'KC SORONG', N'BSI-10258', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10258', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6702', N'KCP SENGKANG', N'BSI-10284', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10284', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6703', N'KCP MAKASSAR TAMALANREA', N'BSI-10285', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10285', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6704', N'KCP PALU PLAZA', N'BSI-10317', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10317', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6705', N'KCP MOROWALI', N'BSI-10318', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10318', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6706', N'KCP JAYAPURA ABEPURA', N'BSI-10344', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10344', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6707', N'KC AMBON', N'BSI-10354', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10354', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6708', N'KC KENDARI A SILONDAE 1', N'BSI-10366', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10366', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6709', N'KCP MAKASSAR UNISMUH', N'BSI-10380', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10380', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6710', N'KCP JAYAPURA SENTANI', N'BSI-10395', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10395', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6711', N'KCP MANADO KAIRAGI', N'BSI-10398', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10398', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6712', N'KCP PALU TADULAKO', N'BSI-10444', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10444', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6713', N'KCP KOTAMOBAGU', N'BSI-10464', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10464', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6714', N'KCP MAROS 1', N'BSI-10466', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10466', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6715', N'KCP TOLITOLI', N'BSI-10511', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10511', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6716', N'KCP PALU BASUKI RAHMAT', N'BSI-10536', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10536', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6717', N'KCP POLEWALI', N'BSI-10542', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10542', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6718', N'KCP GOWA HASANUDDIN', N'BSI-10579', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10579', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6719', N'KCP AMPANA', N'BSI-10596', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10596', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6721', N'KCP BAUBAU', N'BSI-10668', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10668', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6722', N'KCP AMBON DIPONEGORO', N'BSI-10704', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10704', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6723', N'KCP BACAN', N'BSI-10705', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10705', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6724', N'KCP BULA', N'BSI-10717', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10717', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6725', N'KC MAKASSAR 2', N'BSI-18014', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18014', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6726', N'KCP PALU M YAMIN', N'BSI-18050', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18050', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6727', N'KCP KENDARI MT HARYONO', N'BSI-18051', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18051', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6728', N'KCP MAKASSAR VETERAN', N'BSI-18056', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18056', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6729', N'KCP TERNATE HASAN ESA', N'BSI-18069', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18069', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6730', N'KC PALOPO', N'BSI-18071', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18071', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6731', N'KC PAREPARE', N'BSI-18072', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18072', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6732', N'KCP PANGKEP', N'BSI-18235', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18235', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6733', N'KCP MAKASSAR TAMALANREA 2', N'BSI-18237', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18237', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6734', N'KCP BULUKUMBA SAM RATULANGI', N'BSI-18238', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18238', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6735', N'KCP TAKALAR', N'BSI-18239', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18239', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6736', N'KCP MUNA', N'BSI-18294', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18294', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6737', N'KCP KOLAKA', N'BSI-18295', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18295', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6738', N'KCP JAILOLO', N'BSI-18298', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18298', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6739', N'KCP TOBELO', N'BSI-18299', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18299', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6740', N'KCP MOROTAI', N'BSI-18300', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18300', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6741', N'KCP LABUHA', N'BSI-18301', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18301', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6742', N'KCP BELOPA', N'BSI-18305', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18305', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6743', N'KCP MASAMBA', N'BSI-18306', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18306', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6744', N'KCP TOMONI', N'BSI-18307', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18307', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6745', N'KCP MAJENE', N'BSI-18309', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18309', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6746', N'KCP SIDRAP', N'BSI-18310', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18310', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6747', N'KCP ENREKANG', N'BSI-18311', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18311', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6748', N'KCP WATANSOPPENG', N'BSI-18312', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18312', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6749', N'KCP MAKASSAR PETTARANI', N'BSI-19008', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19008', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6750', N'KCP KENDARI A SILONDAE 2', N'BSI-19039', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19039', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6751', N'KCP PALU WOLTER MONGINSIDI', N'BSI-19041', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19041', N'33', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6752', N'KC MANADO MANTOS', N'BSI-19043', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19043', N'27', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6753', N'KCP SINJAI', N'BSI-19210', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19210', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6754', N'KCP GOWA SUNGGUMINASA ', N'BSI-19214', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19214', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6755', N'KCP MAKASSAR PANNAMPU', N'BSI-19267', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19267', N'25', N'7', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6756', N'KC PALEMBANG DEMANG', N'BSI-10019', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10019', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6757', N'KC PADANG 1', N'BSI-10027', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10027', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6758', N'KC JAMBI GATOT SUBROTO', N'BSI-10032', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10032', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6759', N'KC BANDAR LAMPUNG DIPONEGORO', N'BSI-10037', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10037', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6760', N'KC BUKITTINGGI 1', N'BSI-10039', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10039', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6761', N'KC BENGKULU S PARMAN', N'BSI-10056', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10056', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6762', N'KC PANGKAL PINANG', N'BSI-10063', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10063', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6763', N'KC PRABUMULIH SUDIRMAN 1', N'BSI-10137', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10137', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6764', N'KCP PALEMBANG PASAR 16 ILIR', N'BSI-10139', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10139', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6765', N'KCP PALEMBANG SIMPANG PATAL', N'BSI-10140', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10140', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6766', N'KCP SOLOK A DAHLAN 1', N'BSI-10157', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10157', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6767', N'KCP MUARA BUNGO M YAMIN', N'BSI-10167', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10167', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6768', N'KCP JAMBI SIPIN 1', N'BSI-10168', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10168', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6769', N'KC BANDAR JAYA', N'BSI-10180', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10180', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6770', N'KC METRO', N'BSI-10181', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10181', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6771', N'KCP PRINGSEWU 1', N'BSI-10182', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10182', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6772', N'KCP BANDAR LAMPUNG KEDATON', N'BSI-10183', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10183', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6773', N'KCP PAYAKUMBUH', N'BSI-10186', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10186', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6774', N'KCP CURUP', N'BSI-10209', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10209', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6775', N'KCP PADANG ULAK KARANG', N'BSI-10220', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10220', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6776', N'KCP PADANG BANDAR BUAT', N'BSI-10221', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10221', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6777', N'KCP PADANG PARIAMAN', N'BSI-10222', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10222', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6778', N'KCP PADANG PANJANG', N'BSI-10225', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10225', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6779', N'KCP PASAMAN BARAT', N'BSI-10226', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10226', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6780', N'KCP PALEMBANG RADIAL', N'BSI-10239', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10239', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6781', N'KCP PALEMBANG KM 6', N'BSI-10240', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10240', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6782', N'KCP KAYU AGUNG', N'BSI-10290', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10290', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6783', N'KCP SUNGAI LILIN 1', N'BSI-10291', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10291', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6784', N'KCP PULAU PUNJUNG 1', N'BSI-10300', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10300', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6785', N'KCP SAROLANGUN 1', N'BSI-10310', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10310', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6786', N'KCP UNIT 2 TULANG BAWANG', N'BSI-10316', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10316', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6787', N'KCP LUBUK BASUNG', N'BSI-10320', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10320', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6788', N'KCP LAHAT', N'BSI-10335', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10335', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6789', N'KCP TANJUNG PANDAN', N'BSI-10352', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10352', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6790', N'KCP BATUSANGKAR', N'BSI-10385', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10385', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6791', N'KCP BANDAR LAMPUNG TELUK BETUNG', N'BSI-10411', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10411', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6792', N'KCP JAMBI DR SUTOMO', N'BSI-10437', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10437', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6793', N'KCP PALEMBANG VETERAN', N'BSI-10443', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10443', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6794', N'KCP LUBUK SIKAPING', N'BSI-10465', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10465', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6795', N'KCP KALIANDA', N'BSI-10489', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10489', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6796', N'KCP KOTABUMI', N'BSI-10490', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10490', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6797', N'KCP PAINAN', N'BSI-10496', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10496', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6798', N'KCP KUALA TUNGKAL', N'BSI-10527', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10527', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6799', N'KCP BUKITTINGGI PASAR AUR', N'BSI-10538', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10538', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6800', N'KCP TANJUNG ENIM', N'BSI-10559', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10559', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6801', N'KCP PALEMBANG JAKABARING', N'BSI-10585', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10585', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6803', N'KCP SUNGAI LIAT TOWN SQUARE', N'BSI-10638', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10638', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6804', N'KCP PANGKALAN', N'BSI-10640', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10640', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6805', N'KCP LIWA', N'BSI-10643', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10643', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6806', N'KCP MUARA BULIAN', N'BSI-10645', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10645', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6807', N'KC PADANG BELAKANG OLO', N'BSI-18013', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18013', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6808', N'KC PALEMBANG SUDIRMAN', N'BSI-18016', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18016', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6809', N'KCP BANDAR LAMPUNG TANJUNG KARANG', N'BSI-18026', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18026', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6810', N'KC JAMBI PATTIMURA', N'BSI-18040', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18040', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6811', N'KCP BUKITTINGGI SUDIRMAN', N'BSI-18043', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18043', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6812', N'KCP BENGKULU SUDIRMAN', N'BSI-18044', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18044', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6813', N'KCP BANDAR LAMPUNG TELUK BETUNG 2', N'BSI-18057', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18057', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6814', N'KC BATURAJA RAHMAN HAMIDI', N'BSI-18061', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18061', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6815', N'KC LUBUK LINGGAU', N'BSI-18062', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18062', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6816', N'KCP PALEMBANG SUKODADI', N'BSI-18064', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18064', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6817', N'KCP BENGKULU PANORAMA', N'BSI-18067', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18067', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6818', N'KCP PADANG BY PASS', N'BSI-18109', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18109', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6819', N'KCP PADANG GAJAH MADA', N'BSI-18110', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18110', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6820', N'KCP PALEMBANG DEMANG', N'BSI-18121', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18121', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6821', N'KCP RAJABASA', N'BSI-18161', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18161', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6822', N'KCP PAYAKUMBUH SUDIRMAN', N'BSI-18212', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18212', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6823', N'KCP BANDAR LAMPUNG ANTASARI', N'BSI-18240', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18240', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6824', N'KCP PRINGSEWU A YANI 2', N'BSI-18242', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18242', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6825', N'KCP BELITANG SUDIRMAN', N'BSI-18260', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18260', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6826', N'KCP KOTA MARTAPURA', N'BSI-18261', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18261', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6827', N'KCP TUGU MULYO 1', N'BSI-18263', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18263', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6828', N'KCP INDRALAYA', N'BSI-18264', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18264', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6829', N'KCP MUARA ENIM', N'BSI-18265', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18265', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6830', N'KCP MUARA BELITI', N'BSI-18266', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18266', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6831', N'KCP PAGAR ALAM', N'BSI-18267', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18267', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6832', N'KCP SEKAYU', N'BSI-18275', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18275', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6833', N'KCP PALEMBANG MERDEKA', N'BSI-18276', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18276', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6834', N'KCP PANGKALAN BALAI', N'BSI-18277', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18277', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6835', N'KCP PALEMBANG OPI JAKABARING', N'BSI-18278', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18278', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6836', N'KCP JAMBI RIMBO BUJANG 1', N'BSI-18284', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18284', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6837', N'KCP SINGKUT', N'BSI-18285', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18285', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6838', N'KCP BANGKO 1', N'BSI-18286', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18286', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6839', N'KCP KUAMANG KUNING', N'BSI-18287', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18287', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6840', N'KCP IPUH MUKOMUKO', N'BSI-18289', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18289', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6841', N'KCP BENGKULU ARGAMAKMUR', N'BSI-18290', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18290', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6842', N'KCP SELUMA', N'BSI-18291', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18291', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6843', N'KCP BENGKULU AMPERA MANNA', N'BSI-18292', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18292', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6844', N'KCP PALEMBANG A RIVAI', N'BSI-19010', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19010', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6845', N'KCP PADANG KIS MANGUNSARKORO', N'BSI-19025', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19025', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6846', N'KCP JAMBI HAYAM WURUK', N'BSI-19027', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19027', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6847', N'KCP BANDAR LAMPUNG KEDATON 2', N'BSI-19028', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19028', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6848', N'KC BENGKULU S PARMAN 2', N'BSI-19038', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19038', N'9', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6849', N'KCP PRABUMULIH SUDIRMAN', N'BSI-19068', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19068', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6850', N'KCP PALEMBANG SUDIRMAN', N'BSI-19095', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19095', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6851', N'KCP METRO AH NASUTION', N'BSI-19114', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19114', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6852', N'KCP SRIBHAWONO', N'BSI-19158', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19158', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6853', N'KCP PRINGSEWU A YANI 3', N'BSI-19175', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19175', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6854', N'KCP MUARA TEBO', N'BSI-19203', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19203', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6855', N'KCP JAMBI SUNGAI BAHAR', N'BSI-19204', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19204', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6856', N'KCP SOLOK A DAHLAN 2', N'BSI-19221', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19221', N'31', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6857', N'KCP PANGKAL PINANG', N'BSI-19224', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19224', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6858', N'KCP TULANG BAWANG BARAT', N'BSI-19248', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19248', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6859', N'KCP BANDAR LAMPUNG NATAR', N'BSI-19249', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19249', N'3', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6860', N'KCP PALEMBANG KENTEN 2', N'BSI-19388', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19388', N'32', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6861', N'KCP SUNGAI PENUH', N'BSI-19391', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19391', N'18', N'8', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6862', N'KC BANJARMASIN', N'BSI-10016', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10016', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6863', N'KC BALIKPAPAN', N'BSI-10023', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10023', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6864', N'KC PONTIANAK', N'BSI-10026', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10026', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6865', N'KC SAMARINDA', N'BSI-10036', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10036', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6866', N'KC KUTAI KARTANEGARA', N'BSI-10041', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10041', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6867', N'KCP PALANGKARAYA 1', N'BSI-10066', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10066', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6868', N'KC MARTAPURA', N'BSI-10123', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10123', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6869', N'KCP BANJARMASIN A YANI', N'BSI-10124', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10124', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6870', N'KCP BATULICIN', N'BSI-10125', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10125', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6871', N'KCP BARABAI', N'BSI-10126', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10126', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6872', N'KC KETAPANG', N'BSI-10155', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10155', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6873', N'KCP SINTANG LINTAS MELAWI', N'BSI-10156', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10156', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6874', N'KC BONTANG', N'BSI-10178', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10178', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6875', N'KCP TARAKAN', N'BSI-10179', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10179', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6876', N'KCP NANGA PINOH', N'BSI-10242', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10242', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6877', N'KCP BANJARMASIN PS CEMPAKA', N'BSI-10253', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10253', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6878', N'KCP BANJARMASIN SENTRA ANTASARI', N'BSI-10254', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10254', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6879', N'KCP PELAIHARI', N'BSI-10286', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10286', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6880', N'KCP KOTABARU', N'BSI-10287', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10287', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6881', N'KCP BALIKPAPAN SEPINGGAN', N'BSI-10295', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10295', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6883', N'KCP PONTIANAK DIPONEGORO', N'BSI-10299', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10299', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6884', N'KCP SAMARINDA PAHLAWAN', N'BSI-10315', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10315', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6885', N'KCP SANGATTA', N'BSI-10326', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10326', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6886', N'KC PANGKALAN BUN', N'BSI-10368', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10368', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6887', N'KC SAMBAS', N'BSI-10370', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10370', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6888', N'KC TANJUNG', N'BSI-10377', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10377', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6889', N'KCP PONTIANAK A YANI 1', N'BSI-10403', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10403', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6890', N'KCP BALIKPAPAN KEBUN SAYUR', N'BSI-10410', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10410', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6891', N'KCP PONTIANAK SUNGAI JAWI', N'BSI-10447', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10447', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6892', N'KCP SAMARINDA HASAN BASRI', N'BSI-10450', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10450', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6893', N'KC SAMPIT', N'BSI-10462', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10462', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6894', N'KCP KAPUAS', N'BSI-10479', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10479', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6895', N'KCP KETAPANG MANIS MATA', N'BSI-10491', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10491', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6896', N'KCP SANGGAU', N'BSI-10525', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10525', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6897', N'KCP AMUNTAI', N'BSI-10532', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10532', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6898', N'KCP KUTAI SENDAWAR', N'BSI-10535', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10535', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6899', N'KCP PENAJAM', N'BSI-10541', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10541', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6900', N'KC SINGKAWANG', N'BSI-10548', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10548', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6901', N'KCP SAMARINDA SUDIRMAN', N'BSI-10576', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10576', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6902', N'KCP MUARA TEWEH', N'BSI-10636', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10636', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6903', N'KCP KUTAI TENGGARONG SEBERANG', N'BSI-10642', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10642', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6904', N'KCP NUNUKAN', N'BSI-10648', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10648', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6905', N'KCP BANJARMASIN A YANI 1', N'BSI-18009', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18009', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6906', N'KCP BALIKPAPAN SUDIRMAN 1', N'BSI-18023', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18023', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6907', N'KCP SAMARINDA JUANDA', N'BSI-18036', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18036', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6908', N'KCP PONTIANAK A YANI 3', N'BSI-18037', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18037', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6909', N'KC PALANGKARAYA 1', N'BSI-18048', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18048', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6910', N'KC BANJARBARU', N'BSI-18049', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18049', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6911', N'KCP SUNGAI DANAU', N'BSI-18087', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18087', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6912', N'KCP TARAKAN 2', N'BSI-18204', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18204', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6913', N'KCP BONTANG', N'BSI-18205', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18205', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6914', N'KCP SINTANG 2', N'BSI-18206', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18206', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6915', N'KCP MARTAPURA', N'BSI-18217', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18217', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6916', N'KCP BANJARMASIN A YANI 2', N'BSI-19007', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19007', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6917', N'KCP SAMARINDA BHAYANGKARA', N'BSI-19029', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19029', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6918', N'KCP BALIKPAPAN SUDIRMAN 2', N'BSI-19030', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19030', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6919', N'KCP PONTIANAK GUSTI SULUNG', N'BSI-19031', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19031', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6920', N'KCP PALANGKARAYA 2', N'BSI-19071', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19071', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6921', N'KCP PONTIANAK A YANI 2', N'BSI-19110', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19110', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6922', N'KCP BANJARBARU A YANI', N'BSI-19120', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19120', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6923', N'KCP PONTIANAK SIANTAN', N'BSI-19126', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19126', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6924', N'KCP SAMARINDA BUNG TOMO', N'BSI-19154', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19154', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6925', N'KCP PASER TANAH GROGOT', N'BSI-19183', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19183', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6926', N'KCP BANJARMASIN SULTAN ADAM', N'BSI-19185', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19185', N'6', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6927', N'KCP SANGGAU A YANI', N'BSI-19232', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19232', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6928', N'KCP SINGKAWANG MERDEKA', N'BSI-19233', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19233', N'36', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6929', N'KCP SANGATTA 2', N'BSI-19235', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19235', N'2', N'9', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6930', N'KCP PEKALONGAN WAHID HASYIM', N'BSI-10012', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10012', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6931', N'KC SOLO', N'BSI-10013', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10013', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6932', N'KC YOGYAKARTA', N'BSI-10029', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10029', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6933', N'KC SEMARANG PANDANARAN', N'BSI-10040', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10040', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6934', N'KC PURWOKERTO SUDIRMAN 1', N'BSI-10057', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10057', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6935', N'KC TEGAL GAJAHMADA', N'BSI-10112', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10112', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6936', N'KCP PEMALANG SUDIRMAN 1', N'BSI-10113', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10113', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6937', N'KCP KLATEN PEMUDA 1', N'BSI-10114', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10114', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6938', N'KCP SUKOHARJO SOLO BARU', N'BSI-10115', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10115', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6939', N'KCP SRAGEN', N'BSI-10116', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10116', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6940', N'KCP YOGYAKARTA KALIURANG', N'BSI-10163', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10163', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6941', N'KCP WONOSARI', N'BSI-10164', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10164', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6942', N'KC KUDUS A YANI 1', N'BSI-10187', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10187', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6943', N'KCP UNGARAN', N'BSI-10188', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10188', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6944', N'KCP MAGELANG GATOT SUBROTO', N'BSI-10189', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10189', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6945', N'KC CILACAP A YANI', N'BSI-10211', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10211', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6946', N'KCP BOYOLALI PANDANARAN 1', N'BSI-10234', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10234', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6947', N'KCP KARANGANYAR PALUR', N'BSI-10235', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10235', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6948', N'KCP SOLO PASAR KLIWON', N'BSI-10236', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10236', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6949', N'KCP SUKOHARJO KARTASURA', N'BSI-10237', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10237', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6950', N'KCP WONOGIRI SUDIRMAN', N'BSI-10238', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10238', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6951', N'KCP YOGYAKARTA KATAMSO', N'BSI-10243', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10243', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6952', N'KCP AMBARUKMO', N'BSI-10244', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10244', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6953', N'KCP GODEAN', N'BSI-10245', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10245', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6954', N'KCP SEMARANG KARANGAYU', N'BSI-10246', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10246', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6955', N'KCP PURBALINGGA SUDIRMAN', N'BSI-10248', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10248', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6956', N'KCP SOLO URIP SUMOHARJO', N'BSI-10281', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10281', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6957', N'KCP YOGYAKARTA KOTAGEDE', N'BSI-10305', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10305', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6958', N'KCP SEMARANG TIMUR', N'BSI-10321', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10321', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6959', N'KCP PURWODADI SUPRAPTO', N'BSI-10322', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10322', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6960', N'KCP TEMANGGUNG S PARMAN', N'BSI-10323', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10323', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6961', N'KCP GOMBONG', N'BSI-10324', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10324', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6963', N'KCP BREBES A YANI 1', N'BSI-10349', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10349', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6964', N'KC PATI', N'BSI-10367', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10367', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6965', N'KC SALATIGA', N'BSI-10373', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10373', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6966', N'KC KENDAL SOETTA', N'BSI-10378', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10378', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6967', N'KCP SOLO PASAR KLEWER', N'BSI-10406', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10406', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6968', N'KCP SEMARANG TENTARA PELAJAR', N'BSI-10451', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10451', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6969', N'KCP SOLO NUSUKAN', N'BSI-10453', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10453', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6970', N'KCP TEGAL SLAWI', N'BSI-10460', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10460', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6971', N'KCP CILACAP DIPONEGORO', N'BSI-10478', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10478', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6972', N'KCP JEPARA', N'BSI-10558', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10558', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6973', N'KCP YOGYAKARTA HOS COKROAMINOTO', N'BSI-10577', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10577', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6974', N'KCP PEKALONGAN KAJEN', N'BSI-10578', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10578', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6975', N'KCP SEMARANG NGALIYAN', N'BSI-10582', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10582', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6976', N'KCP SEMARANG BANYUMANIK', N'BSI-10584', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10584', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6977', N'KCP AJIBARANG PANCASAN 1', N'BSI-10594', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10594', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6978', N'KCP BANTUL MELIKAN LOR', N'BSI-10598', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10598', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6979', N'KCP GUBUG', N'BSI-10600', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10600', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6980', N'KCP MAGELANG MUNTILAN', N'BSI-10614', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10614', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6981', N'KCP SEMARANG PANDANARAN', N'BSI-10911', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10911', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6982', N'KCP YOGYAKARTA KUSUMANEGARA', N'BSI-18005', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18005', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6983', N'KC PEKALONGAN PEMUDA', N'BSI-18006', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18006', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6984', N'KC SEMARANG A YANI', N'BSI-18007', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18007', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6985', N'KC SOLO SLAMET RIYADI 2', N'BSI-18021', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18021', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6986', N'KCP PURWOKERTO SUDIRMAN', N'BSI-18038', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18038', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6987', N'KCP KUDUS A YANI', N'BSI-18047', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18047', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6988', N'KCP GODEAN 2', N'BSI-18073', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18073', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6989', N'KCP SLEMAN 2', N'BSI-18074', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18074', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6990', N'KCP KALIURANG', N'BSI-18075', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18075', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6991', N'KCP BANTUL SUDIRMAN 1', N'BSI-18076', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18076', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6992', N'KCP YOGYAKARTA FE UII', N'BSI-18077', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18077', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6993', N'KCP TEGAL SUTOYO', N'BSI-18078', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18078', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6994', N'KCP BREBES BUMIAYU', N'BSI-18079', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18079', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6995', N'KCP SEMARANG UNISSULA', N'BSI-18080', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18080', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6996', N'KCP UNGARAN DIPONEGORO 2', N'BSI-18081', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18081', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6997', N'KCP SEMARANG DURIAN SELATAN', N'BSI-18082', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18082', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6998', N'KCP MAGELANG SUDIRMAN', N'BSI-18083', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18083', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6999', N'KCP SRAGEN ATRIUM BLOK F', N'BSI-18141', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18141', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7000', N'KCP KLATEN DELANGGU', N'BSI-18142', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18142', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7001', N'KCP SUKOHARJO SLAMET RIYADI', N'BSI-18143', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18143', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7002', N'KCP BOYOLALI PANDANARAN 2', N'BSI-18144', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18144', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7003', N'KCP CILACAP S PARMAN', N'BSI-18207', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18207', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7004', N'KCP JEPARA PEMUDA 2', N'BSI-18215', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18215', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7005', N'KCP PURWOREJO', N'BSI-18313', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18313', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7006', N'KCP SEMARANG MT HARYONO', N'BSI-19005', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19005', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7007', N'KCP YOGYAKARTA KOLONEL SUGIYONO', N'BSI-19019', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19019', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7008', N'KCP SOLO VETERAN', N'BSI-19020', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19020', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7009', N'KCP TEGAL KS TUBUN', N'BSI-19032', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19032', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7010', N'KC PURWOKERTO KARANGKOBAR', N'BSI-19033', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19033', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7011', N'KCP BATANG', N'BSI-19063', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19063', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7012', N'KCP SLEMAN AFFANDI', N'BSI-19076', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19076', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7013', N'KCP KUDUS A YANI', N'BSI-19085', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19085', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7014', N'KCP YOGYAKARTA A DAHLAN', N'BSI-19098', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19098', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7015', N'KCP SEMARANG SUDIARTO', N'BSI-19099', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19099', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7016', N'KCP KARANGANYAR', N'BSI-19111', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19111', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7017', N'KCP CILACAP GATOT SUBROTO', N'BSI-19141', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19141', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7018', N'KCP BANYUMAS SOKARAJA', N'BSI-19142', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19142', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7019', N'KCP PURBALINGGA SOEKARNO HATTA', N'BSI-19143', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19143', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7020', N'KCP BREBES JATIBARANG', N'BSI-19144', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19144', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7021', N'KCP BREBES SUDIRMAN', N'BSI-19145', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19145', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7022', N'KCP KLATEN VETERAN', N'BSI-19162', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19162', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7023', N'KCP TEGAL BANJARAN', N'BSI-19163', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19163', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7024', N'KCP SRAGEN MASARAN', N'BSI-19172', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19172', N'39', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7025', N'KCP DEMAK SULTAN FATTAH', N'BSI-19177', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19177', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7026', N'KCP KEBUMEN', N'BSI-19181', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19181', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7027', N'KCP PURWODADI A YANI', N'BSI-19246', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19246', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7028', N'KCP KENDAL WELERI', N'BSI-19247', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19247', N'23', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7029', N'KCP PEMALANG SUDIRMAN 2', N'BSI-19250', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19250', N'37', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7030', N'KCP SLEMAN PRAMBANAN', N'BSI-19253', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19253', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7031', N'KCP KULON PROGO', N'BSI-19269', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19269', N'44', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7032', N'KCP PATI SUDIRMAN 2', N'BSI-19389', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19389', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7033', N'KCP REMBANG SUDIRMAN', N'BSI-19402', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19402', N'38', N'10', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7034', N'KC JAKARTA HASANUDIN', N'BSI-10003', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10003', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7035', N'KC JAKARTA MAYESTIK', N'BSI-10004', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10004', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7036', N'KCP JAKARTA MAMPANG PRAPATAN', N'BSI-10005', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10005', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7037', N'KC JKT PONDOK INDAH', N'BSI-10006', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10006', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7038', N'KC BOGOR', N'BSI-10017', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10017', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7039', N'KC JAKARTA SAHARJO', N'BSI-10033', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10033', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7040', N'KC DEPOK', N'BSI-10046', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10046', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7042', N'KCP JKT WOLTERMONGINSIDI', N'BSI-10068', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10068', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7043', N'KC JAKARTA CIBUBUR', N'BSI-10072', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10072', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7044', N'KCP JKT LENTENG AGUNG', N'BSI-10074', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10074', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7045', N'KC TANGERANG CIPUTAT', N'BSI-10075', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10075', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7046', N'KCP JKT PONDOK LABU', N'BSI-10076', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10076', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7047', N'KC TANGERANG BINTARO', N'BSI-10077', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10077', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7048', N'KCP DEPOK CINERE', N'BSI-10078', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10078', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7049', N'KCP TANGERANG PAMULANG', N'BSI-10079', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10079', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7050', N'KCP JKT CILANDAK', N'BSI-10080', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10080', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7051', N'KC BOGOR CIBINONG', N'BSI-10127', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10127', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7052', N'KCP BOGOR TAJUR', N'BSI-10128', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10128', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7053', N'KCP BOGOR DRAMAGA', N'BSI-10129', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10129', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7054', N'KCP BOGOR MERDEKA', N'BSI-10130', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10130', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7055', N'KCP BOGOR JALAN BARU', N'BSI-10131', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10131', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7056', N'KCP BOGOR CITEUREUP', N'BSI-10132', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10132', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7057', N'KCP TANGERANG VILLA MELATI MAS', N'BSI-10134', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10134', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7058', N'KCP JKT RASUNA SAID', N'BSI-10170', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10170', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7059', N'KCP JKT TEBET', N'BSI-10171', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10171', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7060', N'KCP JAKARTA DEWI SARTIKA', N'BSI-10177', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10177', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7061', N'KCP DEPOK MARGONDA', N'BSI-10198', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10198', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7062', N'KCP DEPOK CIMANGGIS', N'BSI-10199', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10199', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7063', N'KCP DEPOK SAWANGAN', N'BSI-10200', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10200', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7064', N'KCP DEPOK DUA', N'BSI-10201', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10201', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7065', N'KCP JKT PANGLIMA POLIM', N'BSI-10229', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10229', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7066', N'KCP JKT KEMANG', N'BSI-10260', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10260', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7067', N'KCP JAKARTA CIRACAS', N'BSI-10261', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10261', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7068', N'KCP TANGERANG CIRENDEU', N'BSI-10262', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10262', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7069', N'KCP BOGOR POMAD', N'BSI-10288', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10288', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7070', N'KCP JKT MEGA KUNINGAN', N'BSI-10311', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10311', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7071', N'KCP DEPOK KELAPA DUA', N'BSI-10329', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10329', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7072', N'KCP DEPOK NUSANTARA', N'BSI-10330', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10330', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7073', N'KCP JKT KEBAYORAN LAMA', N'BSI-10332', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10332', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7074', N'KCP TANGERANG BINTARO SEKTOR III', N'BSI-10343', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10343', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7075', N'KC JAKARTA KALIBATA', N'BSI-10372', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10372', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7076', N'KCP BSD PASAR MODERN', N'BSI-10376', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10376', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7077', N'KCP DEPOK FMIPA UI', N'BSI-10386', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10386', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7078', N'KCP BOGOR SUDIRMAN', N'BSI-10420', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10420', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7079', N'KCP JAKARTA GANDARIA', N'BSI-10422', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10422', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7080', N'BOGOR KOTA WISATA', N'BSI-10424', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10424', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7081', N'KCP JKT PONDOK PINANG', N'BSI-10432', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10432', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7082', N'KCP JKT RADIO DALAM', N'BSI-10463', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10463', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7083', N'KCP TANGERANG GRAHA RAYA', N'BSI-10522', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10522', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7084', N'KCP JKT SULTAN ISKANDAR MUDA', N'BSI-10551', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10551', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7085', N'KCP BOGOR SENTUL', N'BSI-10552', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10552', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7086', N'KCP JKT PEJATEN RAYA', N'BSI-10630', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10630', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7087', N'KCP TANGERANG KARAWACI', N'BSI-10657', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10657', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7088', N'KC JAKARTA FATMAWATI 2', N'BSI-18011', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18011', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7089', N'KC BOGOR PAJAJARAN BANTARJATI', N'BSI-18022', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18022', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7090', N'KC TANGERANG BSD ITC', N'BSI-18025', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18025', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7091', N'KC DEPOK MARGONDA 2', N'BSI-18035', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18035', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7092', N'KCP BOGOR TANAH SAREAL', N'BSI-18055', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18055', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7093', N'KCP JAKARTA CIBUBUR 1', N'BSI-18090', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18090', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7094', N'KCP JAKARTA DEWI SARTIKA 2', N'BSI-18091', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18091', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7095', N'KCP JAKARTA OTISTA', N'BSI-18093', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18093', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7096', N'KCP JAKARTA PANGLIMA POLIM 2', N'BSI-18096', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18096', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7097', N'KCP JAKARTA KALIBATA', N'BSI-18097', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18097', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7098', N'KCP UIN SYARIF HIDAYATULLAH', N'BSI-18098', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18098', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7099', N'KCP JAKARTA CILANDAK 1', N'BSI-18099', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18099', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7100', N'KCP BINTARO VETERAN', N'BSI-18100', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18100', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7101', N'KCP JAKARTA TEBET RAYA', N'BSI-18101', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18101', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7102', N'KCP JAKARTA TEMPO PAVILION', N'BSI-18124', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18124', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7103', N'KCP JAKARTA AMPERA RAYA', N'BSI-18129', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18129', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7104', N'KCP BOGOR TAJUR 2', N'BSI-18145', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18145', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7105', N'KCP JAKARTA CIBUBUR COUNTRY', N'BSI-18146', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18146', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7106', N'KCP BOGOR CIBINONG PRATAMA', N'BSI-18147', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18147', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7107', N'KCP BOGOR CISARUA', N'BSI-18149', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18149', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7108', N'KCP TANGERANG GADING SERPONG 2', N'BSI-18157', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18157', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7109', N'KCP BINTARO KEBAYORAN ARCADE 1', N'BSI-18158', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18158', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7110', N'KCP TANGERANG PAMULANG SILIWANGI', N'BSI-18159', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18159', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7111', N'KCP JAKARTA PONDOK PINANG 2', N'BSI-18160', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18160', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7112', N'KCP TANGERANG KARAWACI MUTIARA', N'BSI-18198', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18198', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7113', N'KCP DEPOK CINERE 1', N'BSI-18202', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18202', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7114', N'KCP DEPOK SAWANGAN SARI PLAZA', N'BSI-18203', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18203', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7115', N'KCP BOGOR CILEUNGSI RAYA', N'BSI-18232', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18232', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7116', N'KCP DEPOK PROKLAMASI', N'BSI-18233', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18233', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7117', N'KCP BOGOR CITEUREUP 3', N'BSI-18234', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18234', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7119', N'KCP BOGOR AHMAD YANI', N'BSI-19013', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19013', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7120', N'KCP JAKARTA FATMAWATI 2', N'BSI-19036', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19036', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7121', N'KCP DEPOK MARGONDA AARDEN', N'BSI-19057', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19057', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7122', N'KCP JAKARTA DEWI SARTIKA 3', N'BSI-19075', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19075', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7123', N'KCP JAKARTA CIBUBUR 2', N'BSI-19081', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19081', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7124', N'KCP BOGOR PAJAJARAN BANTARJATI', N'BSI-19101', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19101', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7125', N'KCP JAKARTA WARUNG BUNCIT', N'BSI-19103', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19103', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7126', N'KCP JAKARTA PASAR MINGGU', N'BSI-19109', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19109', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7127', N'KCP BOGOR CIBINONG RAYA', N'BSI-19112', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19112', N'10', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7128', N'KCP JAKARTA MAYESTIK', N'BSI-19119', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19119', N'13', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7129', N'KCP JAKARTA CIPUTAT', N'BSI-19123', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19123', N'20', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7130', N'KCP DEPOK PANCORAN MAS', N'BSI-19155', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19155', N'12', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7131', N'KCP TANGERANG PARAKAN', N'BSI-19195', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19195', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7132', N'KCP TANGERANG ALAM SUTRA', N'BSI-19196', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19196', N'43', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7133', N'KCP JAKARTA TEBET TIMUR', N'BSI-19266', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19266', N'16', N'11', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7134', N'KC ACEH', N'BSI-10011', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10011', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7135', N'KC SIMEULUE', N'BSI-10025', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10025', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7136', N'KCP LANGSA 1', N'BSI-10055', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10055', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7137', N'KCP MEULABOH NASIONAL', N'BSI-10111', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10111', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7138', N'KCP BLANGPIDIE', N'BSI-10154', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10154', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7139', N'KCP BIREUEN 1', N'BSI-10210', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10210', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7140', N'KCP ACEH DARUSSALAM', N'BSI-10216', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10216', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7141', N'KCP CALANG', N'BSI-10277', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10277', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7142', N'KCP KUALA SIMPANG CUT NYAK DIEN', N'BSI-10346', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10346', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7143', N'KC LHOKSEUMAWE 1', N'BSI-10365', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10365', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7144', N'KCP ULEE KARENG 1', N'BSI-10495', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10495', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7145', N'KCP TAKENGON SENGEDA 1', N'BSI-10652', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10652', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7146', N'KFO POS BANDA ACEH', N'BSI-10677', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10677', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7147', N'KFO POS LANGSA', N'BSI-10678', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10678', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7148', N'KCP BANDA ACEH PEUNAYONG', N'BSI-10912', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10912', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7149', N'KCP BANDA ACEH KEUTAPANG', N'BSI-10913', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10913', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7150', N'KCP BANDA ACEH UNSYIAH', N'BSI-10914', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10914', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7151', N'KCP LANGSA OPAK', N'BSI-10917', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10917', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7152', N'KCP BANDA ACEH DAUD BEUREUH', N'BSI-10923', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10923', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7153', N'KCP BANDA ACEH CUT MEUTIA', N'BSI-10926', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10926', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7154', N'KCP LHOKSEUMAWE PENDOPO', N'BSI-10927', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10927', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7155', N'KCP BANDA ACEH HASAN BATOH', N'BSI-10928', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10928', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7156', N'KCP SEUMADAM', N'BSI-10931', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10931', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7157', N'KCP PEUNAYONG 3', N'BSI-10955', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10955', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7158', N'KCP BEUREUNEUN MUTIARA', N'BSI-10957', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10957', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7159', N'KC BANDA ACEH AHMAD DAHLAN', N'BSI-18029', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18029', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7160', N'KC LHOKSEUMAWE SYECH SYAMSUDDIN', N'BSI-18053', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18053', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7161', N'KCP MEUREUDU 2', N'BSI-18172', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18172', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7162', N'KCP TEUKU UMAR', N'BSI-18173', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18173', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7163', N'KCP LAMBARO SOETTA 2', N'BSI-18175', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18175', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7164', N'KCP MEULABOH NASIONAL 2', N'BSI-18177', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18177', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7165', N'KCP LUENGBATA', N'BSI-18180', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18180', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7166', N'KCP NAGAN RAYA 1', N'BSI-18181', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18181', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7167', N'KCP SABANG 2', N'BSI-18182', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18182', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7168', N'KCP TAPAKTUAN MERDEKA 2', N'BSI-18184', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18184', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7169', N'KCP SUBULUSSALAM 2', N'BSI-18185', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18185', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7170', N'KCP TAKENGON SENGEDA 2', N'BSI-18220', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18220', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7171', N'KCP IDI RAYEUK 2', N'BSI-18224', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18224', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7172', N'KCP PUPUK ISKANDAR MUDA', N'BSI-18226', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18226', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7173', N'KCP SUKARAMAI', N'BSI-18227', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18227', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7174', N'KCP MEULABOH IMAM BONJOL', N'BSI-18327', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18327', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7175', N'KC SIGLI 1', N'BSI-18328', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18328', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7176', N'KCP LANGSA 2', N'BSI-18329', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18329', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7177', N'KCP BIREUEN CHIK JOHAN', N'BSI-18330', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18330', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7178', N'KCP BANDA ACEH LAMPRIET', N'BSI-19024', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19024', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7179', N'KC BANDA ACEH CUT MEUTIA', N'BSI-19045', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19045', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7180', N'KC BIREUEN SIMPANG IV', N'BSI-19046', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19046', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7181', N'KC BLANGPIDIE', N'BSI-19047', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19047', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7182', N'KC KUTACANE', N'BSI-19048', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19048', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7183', N'KC KUALA SIMPANG', N'BSI-19049', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19049', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7184', N'KC LANGSA DARUSSALAM', N'BSI-19050', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19050', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7185', N'KC LHOKSEUMAWE MERDEKA 3', N'BSI-19051', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19051', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7186', N'KC MEULABOH IMAM BONJOL', N'BSI-19052', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19052', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7187', N'KC SIGLI 2', N'BSI-19053', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19053', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7188', N'KC TAKENGON', N'BSI-19054', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19054', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7189', N'KC TAPAKTUAN', N'BSI-19055', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19055', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7190', N'KCP LAMPRIET', N'BSI-19277', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19277', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7191', N'KCP SABANG 3', N'BSI-19279', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19279', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7192', N'KCP SIMPANG SURABAYA', N'BSI-19280', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19280', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7193', N'KCP BLANGKEJEREN', N'BSI-19281', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19281', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7194', N'KCP IDI RAYEUK 3', N'BSI-19282', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19282', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7195', N'KCP LHOKSUKON 3', N'BSI-19284', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19284', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7196', N'KCP NAGAN RAYA 2', N'BSI-19285', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19285', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7197', N'KCP ULEE GLEE', N'BSI-19286', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19286', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7199', N'KCP BENER MERIAH PONDOK BARU 2', N'BSI-19288', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19288', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7200', N'KCP SINGKIL ISKANDAR MUDA', N'BSI-19289', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19289', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7201', N'KCP SUBULUSSALAM 3', N'BSI-19290', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19290', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7202', N'KCP DARUSSALAM T NYAK ARIEF 2', N'BSI-19291', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19291', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7203', N'KCP JANTHO SUDIRMAN', N'BSI-19292', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19292', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7204', N'KCP KEUTAPANG MATA IE 2', N'BSI-19293', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19293', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7205', N'KCP LAMBARO SOETTA 4', N'BSI-19294', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19294', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7206', N'KCP PADANG SIKABU', N'BSI-19295', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19295', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7207', N'KCP MEURAH DUA', N'BSI-19296', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19296', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7208', N'KCP KOTA FAJAR', N'BSI-19297', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19297', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7209', N'KCP LHOKNGA', N'BSI-19298', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19298', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7210', N'KCP SUKAMAKMUR', N'BSI-19299', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19299', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7211', N'KCP SAREE ', N'BSI-19300', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19300', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7212', N'KCP SEULIMUM', N'BSI-19301', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19301', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7213', N'KCP LAMNO', N'BSI-19302', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19302', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7214', N'KCP KUALA BATEE', N'BSI-19303', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19303', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7215', N'KCP CUT NYAK DIEN', N'BSI-19304', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19304', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7216', N'KCP INDRAJAYA', N'BSI-19305', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19305', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7217', N'KCP JAMBO TAPE', N'BSI-19307', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19307', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7218', N'KCP SEUTUI', N'BSI-19308', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19308', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7219', N'KCP MANGGENG', N'BSI-19309', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19309', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7220', N'KCP PEUKAN KOTA', N'BSI-19310', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19310', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7221', N'KCP JOHAN PAHLAWAN', N'BSI-19311', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19311', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7222', N'KCP SAMATIGA', N'BSI-19312', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19312', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7223', N'KCP SIGLI PADANG TIJI', N'BSI-19313', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19313', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7224', N'KCP SAKTI', N'BSI-19314', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19314', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7225', N'KCP SIGLI ISKANDAR MUDA', N'BSI-19315', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19315', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7226', N'KCP SIMPANG BALIK', N'BSI-19316', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19316', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7227', N'KCP LADANG RIMBA', N'BSI-19317', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19317', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7228', N'KCP TAPAKTUAN MEUKEK', N'BSI-19318', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19318', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7229', N'KCP LABUHAN HAJI', N'BSI-19319', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19319', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7230', N'KCP LANGSA ISKANDAR MUDA', N'BSI-19320', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19320', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7231', N'KCP CALANG ALI GUNO', N'BSI-19321', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19321', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7232', N'KCP TEUNOM', N'BSI-19322', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19322', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7233', N'KCP GRONGGRONG', N'BSI-19323', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19323', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7234', N'KCP KEMBANG TANJUNG', N'BSI-19324', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19324', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7235', N'KCP BANDAR BARU', N'BSI-19325', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19325', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7236', N'KCP BANDAR DUA', N'BSI-19326', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19326', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7237', N'KCP TAPAKTUAN BAKONGAN', N'BSI-19327', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19327', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7238', N'KCP LIPAT KAJANG', N'BSI-19328', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19328', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7239', N'KCP ALUE BILIE 2', N'BSI-19329', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19329', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7240', N'KCP SINABANG', N'BSI-19330', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19330', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7241', N'KCP KUALA MEULABOH', N'BSI-19331', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19331', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7242', N'KCP SEUNAGAN', N'BSI-19332', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19332', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7243', N'KCP MEUREUDU 3', N'BSI-19333', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19333', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7244', N'KCP TANGSE', N'BSI-19334', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19334', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7245', N'KCP TRIENGGADENG', N'BSI-19335', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19335', N'1', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7246', N'KCP SINGKIL BAHARI', N'BSI-19336', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19336', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7247', N'KCP JEUMPA PANGLIMA POLIM', N'BSI-19337', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19337', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7248', N'KCP KUTABLANG BIREUEN', N'BSI-19338', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19338', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7249', N'KCP KEJURUAN MUDA', N'BSI-19339', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19339', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7250', N'KCP LAUSER', N'BSI-19340', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19340', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7251', N'KCP BLANG JREUN', N'BSI-19342', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19342', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7252', N'KCP JAGONG', N'BSI-19343', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19343', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7253', N'KCP PULAU TIGA', N'BSI-19344', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19344', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7254', N'KCP LAWE SIGALAGALA ', N'BSI-19347', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19347', N'30', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7255', N'KCP PANTE BIDARI', N'BSI-19348', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19348', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7256', N'KCP PEUREULAK 2', N'BSI-19349', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19349', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7257', N'KCP KUTA BINJEI 2', N'BSI-19350', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19350', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7258', N'KCP RANTAU PANJANG 2', N'BSI-19351', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19351', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7259', N'KCP COT GIREK', N'BSI-19352', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19352', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7260', N'KCP GEUDONG 2', N'BSI-19353', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19353', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7261', N'KCP KREUNG GEUKEUH 2', N'BSI-19354', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19354', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7262', N'KCP ANGKUP', N'BSI-19355', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19355', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7263', N'KCP GANDAPURA', N'BSI-19356', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19356', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7264', N'KCP JEUNIB', N'BSI-19357', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19357', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7265', N'KCP TUALANG CUT', N'BSI-19358', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19358', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7266', N'KCP INDRA MAKMUR', N'BSI-19359', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19359', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7267', N'KCP GAMPONG JAWA', N'BSI-19360', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19360', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7268', N'KCP KRUENG MANE', N'BSI-19361', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19361', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7269', N'KCP PANTON LABU 3', N'BSI-19362', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19362', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7270', N'KCP MATANG GLUMPANG DUA 2', N'BSI-19363', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19363', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7271', N'KCP SIMPANG MULIENG', N'BSI-19366', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19366', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7272', N'KCP SAWANG', N'BSI-19367', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19367', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7273', N'KCP BATUPHAT', N'BSI-19368', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19368', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7274', N'KCP BLANG MANGAT', N'BSI-19369', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19369', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7275', N'KCP CUNDA', N'BSI-19370', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19370', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7276', N'KCP PUTRI HIJAU', N'BSI-19371', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19371', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7277', N'KCP BENER MERIAH SYIAH UTAMA 2', N'BSI-19372', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19372', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7278', N'KCP PEUDADA', N'BSI-19373', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19373', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7279', N'KCP SAMALANGA ', N'BSI-19374', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19374', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7281', N'KCP SUNGAI LIPUT', N'BSI-19376', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19376', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7282', N'KCP KARANG BARU', N'BSI-19377', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19377', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7283', N'KCP SAMUDERA', N'BSI-19378', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19378', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7284', N'KCP RAKAL', N'BSI-19379', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19379', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'7285', N'KCP LAMPAHAN 2', N'BSI-19380', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19380', N'24', N'12', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6321', N'KCP PEKANBARU RUMBAI', N'BSI-18258', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18258', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6322', N'KC MEDAN S PARMAN', N'BSI-19009', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19009', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6323', N'KCP PEKANBARU ARIFIN AHMAD', N'BSI-19026', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19026', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6324', N'KCP BATAM BUSINESS CENTER', N'BSI-19040', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19040', N'7', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6325', N'KCP BINJAI JAMIN GINTING', N'BSI-19087', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19087', N'29', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6326', N'KCP DURI HANG TUAH 2', N'BSI-19178', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19178', N'34', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6327', N'KCP LUBUK PAKAM SUDIRMAN', N'BSI-19218', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19218', N'28', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6328', N'KCP RANTAU PRAPAT', N'BSI-19219', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19219', N'35', N'3', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6329', N'KC JAKARTA THAMRIN', N'BSI-10002', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10002', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6330', N'KC BEKASI', N'BSI-10007', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10007', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6331', N'KC TANGERANG 1', N'BSI-10018', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10018', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6332', N'KC CILEGON TIRTAYASA 1', N'BSI-10020', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10020', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6333', N'KC JAKARTA TANJUNG PRIOK 1', N'BSI-10021', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10021', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6334', N'KCP JAKARTA RAWAMANGUN', N'BSI-10035', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10035', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6335', N'KC KEBON JERUK ARTERI', N'BSI-10045', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10045', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6336', N'KC JAKARTA KELAPA GADING', N'BSI-10058', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10058', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6337', N'KC JAKARTA PONDOK KELAPA', N'BSI-10061', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10061', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6338', N'KCP JAKARTA CIPULIR', N'BSI-10069', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10069', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6339', N'KCP JKT BURSA EFEK INDONESIA', N'BSI-10071', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10071', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6340', N'KCP JAKARTA CILILITAN', N'BSI-10073', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10073', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6341', N'KC BEKASI CIKARANG', N'BSI-10081', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10081', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6342', N'KCP BEKASI PONDOK GEDE', N'BSI-10082', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10082', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6343', N'KCP KARAWANG KERTABUMI', N'BSI-10083', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10083', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6344', N'KCP CIKAMPEK A YANI', N'BSI-10084', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10084', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6345', N'KCP BEKASI TIMUR', N'BSI-10085', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10085', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6346', N'KCP BEKASI TAMBUN', N'BSI-10086', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10086', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6347', N'KCP JKT TANAH ABANG', N'BSI-10108', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10108', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6348', N'KCP JKT CEMPAKA PUTIH', N'BSI-10109', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10109', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6349', N'KCP JKT PASAR BARU', N'BSI-10110', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10110', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6350', N'KC TANGERANG CILEDUG', N'BSI-10133', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10133', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6351', N'KCP TANGERANG MALABAR', N'BSI-10135', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10135', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6352', N'KCP TANGERANG CIKUPA 1', N'BSI-10136', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10136', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6353', N'KC SERANG', N'BSI-10142', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10142', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6354', N'KCP RANGKASBITUNG 1', N'BSI-10143', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10143', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6355', N'KCP JAKARTA HARCO MANGGA DUA', N'BSI-10144', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10144', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6356', N'KCP JAKARTA KOJA 1', N'BSI-10145', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10145', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6357', N'KCP JKT SUNTER', N'BSI-10146', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10146', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6358', N'KC JKT JATINEGARA', N'BSI-10169', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10169', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6359', N'KCP JKT CAKUNG', N'BSI-10173', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10173', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6360', N'KCP JAKARTA PONDOK BAMBU', N'BSI-10174', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10174', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6361', N'KCP JKT KLENDER', N'BSI-10175', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10175', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6362', N'KCP JKT UTAN KAYU', N'BSI-10176', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10176', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6363', N'KCP JAKARTA KEDOYA', N'BSI-10195', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10195', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6364', N'KCP JAKARTA TANJUNG DUREN', N'BSI-10196', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10196', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6365', N'KCP JAKARTA DURI KOSAMBI', N'BSI-10197', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10197', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6366', N'KCP JKT RAWASARI', N'BSI-10212', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10212', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6367', N'KCP TANGERANG CIMONE', N'BSI-10230', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10230', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6368', N'KCP JKT MUARA KARANG', N'BSI-10231', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10231', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6369', N'KCP JAKARTA KALIDERES', N'BSI-10232', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10232', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6370', N'KCP BEKASI KEMANG PRATAMA', N'BSI-10263', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10263', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6371', N'KCP CIKARANG METRO BOULEVARD', N'BSI-10264', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10264', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6372', N'KCP BEKASI KALIMALANG', N'BSI-10265', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10265', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6373', N'KCP JKT CIKINI', N'BSI-10275', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10275', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6374', N'KCP JKT GAJAH MADA', N'BSI-10276', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10276', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6375', N'KCP LABUAN', N'BSI-10292', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10292', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6376', N'KCP JKT PULO GADUNG', N'BSI-10314', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10314', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6377', N'KCP SERANG CIKANDE 1', N'BSI-10347', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10347', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6378', N'KCP JAKARTA CENGKARENG', N'BSI-10363', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10363', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6379', N'KCP JAKARTA SUDIRMAN', N'BSI-10379', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10379', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6380', N'KCP JKT PLAZA MANDIRI', N'BSI-10391', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10391', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6381', N'KCP JAKARTA TOMANG', N'BSI-10392', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10392', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6382', N'KCP JAKARTA RSIJ CEMPAKA PUTIH', N'BSI-10393', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10393', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6383', N'KCP PANDEGLANG 1', N'BSI-10401', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10401', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6384', N'KCP JKT MENTENG', N'BSI-10429', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10429', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6385', N'KCP JKT JATINEGARA TIMUR', N'BSI-10430', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10430', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6386', N'KCP JAKARTA PANGKALAN JATI', N'BSI-10433', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10433', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6387', N'KCP JAKARTA PS. REBO', N'BSI-10434', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10434', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6388', N'KCP JAKARTA S. PARMAN', N'BSI-10435', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10435', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6389', N'KCP BEKASI HARAPAN INDAH', N'BSI-10509', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10509', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6390', N'KCP BEKASI JATIASIH', N'BSI-10515', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10515', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6391', N'KCP BEKASI JATIBENING', N'BSI-10516', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10516', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6392', N'KCP JKT JOGLO', N'BSI-10517', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10517', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6393', N'KCP JKT PECENONGAN', N'BSI-10520', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10520', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6394', N'KCP JKT SIMPRUG', N'BSI-10523', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10523', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6395', N'KCP JKT CIPINANG JAYA', N'BSI-10553', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10553', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6396', N'KCP JKT KRAMAT RAYA', N'BSI-10571', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10571', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6397', N'KCP SERANG TIMUR', N'BSI-10583', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10583', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6398', N'KCP BEKASI KALIABANG', N'BSI-10603', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10603', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6400', N'KCP JAKARTA GRAHA MANDIRI', N'BSI-10650', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10650', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6401', N'KCP TANGERANG KARANG TENGAH', N'BSI-10656', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10656', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6402', N'KCP JAKARTA THE TOWER', N'BSI-10658', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10658', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6403', N'KCP JAKARTA MERUYA', N'BSI-10660', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10660', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6404', N'KCP JAKARTA PADEMANGAN', N'BSI-10669', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10669', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6405', N'KCP JAKARTA GADING BOULEVARD BARAT', N'BSI-10671', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10671', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6406', N'KC JAKARTA PEMUDA', N'BSI-18010', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18010', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6407', N'KC JAKARTA BENDUNGAN HILIR', N'BSI-18017', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18017', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6408', N'KCP JAKARTA GADING BOULEVARD RAYA', N'BSI-18024', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18024', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6409', N'KCP BEKASI A YANI', N'BSI-18030', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18030', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6410', N'KCP JAKARTA KELAPA DUA', N'BSI-18032', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18032', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6411', N'KCP CILEGON A YANI', N'BSI-18033', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18033', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6412', N'KCP TANGERANG CITY', N'BSI-18034', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18034', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6413', N'KC KARAWANG', N'BSI-18045', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18045', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6414', N'KCP JAKARTA BUARAN', N'BSI-18092', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18092', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6415', N'KCP JAKARTA LUBANG BUAYA', N'BSI-18094', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18094', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6416', N'KCP JAKARTA PASAR INDUK', N'BSI-18095', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18095', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6417', N'KCP JAKARTA KEMENTERIAN AGAMA', N'BSI-18122', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18122', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6418', N'KCP JAKARTA MAHKAMAH AGUNG', N'BSI-18123', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18123', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6419', N'KCP SEKRETARIAT MAHKAMAH AGUNG', N'BSI-18125', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18125', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6420', N'KCP JAKARTA SALEMBA', N'BSI-18126', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18126', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6421', N'KCP JAKARTA MENARA BIDAKARA', N'BSI-18127', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18127', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6422', N'KCP JAKARTA WISMA 46', N'BSI-18128', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18128', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6423', N'KCP JAKARTA CEMPAKA MAS', N'BSI-18154', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18154', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6424', N'KCP JAKARTA SUNTER 2', N'BSI-18155', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18155', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6425', N'KCP JAKARTA PLUMPANG', N'BSI-18156', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18156', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6426', N'KCP CIKARANG KOTA', N'BSI-18187', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18187', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6427', N'KCP BEKASI GRAND WISATA', N'BSI-18188', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18188', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6428', N'KCP BEKASI PONDOK GEDE', N'BSI-18189', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18189', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6429', N'KCP BEKASI WIBAWA MUKTI', N'BSI-18191', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18191', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6430', N'KCP TANGERANG CILEDUG 1', N'BSI-18196', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18196', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6431', N'KCP SERANG A YANI 1', N'BSI-18197', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18197', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6432', N'KCP TANGERANG CIKUPA 2', N'BSI-18199', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18199', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6433', N'KCP TANGERANG BALARAJA 3', N'BSI-18200', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18200', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6434', N'KCP JAKARTA KEMENTERIAN BUMN', N'BSI-19003', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19003', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6435', N'KCP JAKARTA WAHID HASYIM', N'BSI-19011', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19011', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6436', N'KCP TANGERANG DAAN MOGOT', N'BSI-19014', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19014', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6437', N'KCP CILEGON MERAK', N'BSI-19015', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19015', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6438', N'KCP BEKASI SQUARE', N'BSI-19034', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19034', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6439', N'KCP JAKARTA TANJUNG PRIOK KEBON BAWANG', N'BSI-19035', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19035', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6440', N'KCP JAKARTA PESANGGRAHAN', N'BSI-19079', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19079', N'21', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6441', N'KCP JAKARTA MT HARYONO', N'BSI-19080', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19080', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6442', N'KCP BEKASI GALAXY', N'BSI-19082', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19082', N'8', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6443', N'KCP TANGERANG JATIUWUNG', N'BSI-19090', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19090', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6444', N'KCP JAKARTA GADING BOULEVARD TIMUR', N'BSI-19104', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19104', N'14', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6445', N'KCP TANGERANG TANAH TINGGI', N'BSI-19131', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19131', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6446', N'KCP SERANG KRAGILAN', N'BSI-19199', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19199', N'42', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6447', N'KCP JAKARTA CIKINI 2', N'BSI-19255', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19255', N'17', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6448', N'KCP JAKARTA PANGKALAN JATI 2', N'BSI-19259', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19259', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6449', N'KCP JAKARTA PASAR JATINEGARA', N'BSI-19386', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19386', N'15', N'4', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6450', N'KC BANDUNG', N'BSI-10009', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10009', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6451', N'KC CIREBON', N'BSI-10024', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10024', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6452', N'KC TASIKMALAYA SUTISNA SENJAYA', N'BSI-10042', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10042', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6453', N'KC PURWAKARTA', N'BSI-10050', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10050', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6454', N'KC CIMAHI', N'BSI-10091', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10091', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6455', N'KC CIANJUR', N'BSI-10092', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10092', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6456', N'KCP SUKABUMI SUDIRMAN', N'BSI-10093', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10093', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6457', N'KC GARUT', N'BSI-10094', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10094', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6458', N'KCP BANDUNG BUAH BATU', N'BSI-10095', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10095', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6459', N'KCP BANDUNG SETIA BUDI', N'BSI-10096', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10096', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6460', N'KCP SUMEDANG', N'BSI-10097', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10097', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6461', N'KCP KUNINGAN', N'BSI-10151', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10151', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6462', N'KCP INDRAMAYU JATIBARANG', N'BSI-10152', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10152', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6463', N'KCP MAJALENGKA', N'BSI-10153', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10153', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6464', N'KCP CIAMIS', N'BSI-10190', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10190', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6465', N'KCP SUBANG OTISTA 1', N'BSI-10207', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10207', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6466', N'KCP CIREBON PLERED', N'BSI-10241', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10241', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6467', N'KCP BANJAR', N'BSI-10247', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10247', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6468', N'KCP CIANJUR CIPANAS 2', N'BSI-10271', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10271', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6469', N'KCP BANDUNG PAJAJARAN', N'BSI-10272', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10272', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6470', N'KCP INDRAMAYU', N'BSI-10297', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10297', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6471', N'KCP CIREBON CILEDUG', N'BSI-10298', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10298', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6472', N'KCP CIAWI', N'BSI-10325', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10325', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6473', N'KCP SUBANG PAMANUKAN', N'BSI-10334', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10334', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6474', N'KCP SUKABUMI CICURUG SETIA BUDI', N'BSI-10336', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10336', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6475', N'KC BANDUNG AHMAD YANI', N'BSI-10357', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10357', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6476', N'KCP BANDUNG METRO MARGAHAYU', N'BSI-10358', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10358', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6477', N'KCP BANDUNG UJUNG BERUNG', N'BSI-10359', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10359', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6479', N'KCP BANDUNG MOH TOHA', N'BSI-10414', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10414', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6480', N'KCP CIREBON SILIWANGI', N'BSI-10421', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10421', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6481', N'KCP SUKABUMI SURADE', N'BSI-10488', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10488', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6482', N'KCP GARUT KADUNGORA', N'BSI-10556', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10556', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6483', N'KCP INDRAMAYU PATROL', N'BSI-10560', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10560', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6484', N'KCP CIMAHI BAROS', N'BSI-10573', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10573', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6485', N'KCP BANDUNG ANTAPANI', N'BSI-10574', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10574', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6486', N'KCP JATINANGOR', N'BSI-10608', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10608', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6487', N'KCP TASIKMALAYA SINGAPARNA', N'BSI-10612', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10612', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6488', N'KC BANDUNG ASIA AFRIKA', N'BSI-18012', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18012', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6489', N'KCP CIREBON SISINGAMANGARAJA', N'BSI-18020', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18020', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6490', N'KCP TASIKMALAYA MASJID AGUNG', N'BSI-18046', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18046', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6491', N'KC SUKABUMI A YANI', N'BSI-18052', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18052', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6492', N'KCP BANDUNG ASTANA ANYAR', N'BSI-18063', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18063', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6493', N'KCP BANDUNG BUAH BATU 3', N'BSI-18102', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18102', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6494', N'KCP BANDUNG DAGO', N'BSI-18103', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18103', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6495', N'KCP SUROPATICORE', N'BSI-18104', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18104', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6496', N'KCP CIMAHI CIBABAT', N'BSI-18105', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18105', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6497', N'KCP BANDUNG SETRASARI', N'BSI-18106', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18106', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6498', N'KCP CINUNUK', N'BSI-18107', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18107', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6499', N'KCP GARUT', N'BSI-18108', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18108', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6500', N'KCP CIREBON PLERED 2', N'BSI-18138', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18138', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6501', N'KCP KUNINGAN A YANI 2', N'BSI-18139', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18139', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6502', N'KCP MAJALENGKA ABD HALIM 2', N'BSI-18140', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18140', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6503', N'KCP CIAMIS SUDIRMAN', N'BSI-18214', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18214', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6504', N'KCP CIANJUR ABDULLAH BIN NUH', N'BSI-18218', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18218', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6505', N'KCP KOPO SAYATI', N'BSI-18271', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18271', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6506', N'KCP SUBANG PAGADEN', N'BSI-18272', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18272', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6507', N'KCP CIMAREME', N'BSI-18273', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18273', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6508', N'KCP BANDUNG CITARUM', N'BSI-19004', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19004', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6509', N'KCP CIANJUR ABDULLAH BIN NUH 1', N'BSI-19016', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19016', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6510', N'KC BANDUNG SUNIARAJA', N'BSI-19017', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19017', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6511', N'KCP CIREBON SILIWANGI 2', N'BSI-19018', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19018', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6512', N'KCP SUKABUMI MARTADINATA', N'BSI-19062', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19062', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6513', N'KCP PURWAKARTA VETERAN', N'BSI-19064', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19064', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6514', N'KCP TASIKMALAYA A YANI', N'BSI-19065', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19065', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6515', N'KCP CIMAHI AMIR MAHMUD', N'BSI-19077', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19077', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6516', N'KCP BANDUNG SETIABUDI 2', N'BSI-19083', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19083', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6517', N'KCP MAJALENGKA JATIWANGI', N'BSI-19084', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19084', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6518', N'KCP BANDUNG BUAH BATU 2', N'BSI-19091', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19091', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6519', N'KCP INDRAMAYU SOEPRAPTO', N'BSI-19092', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19092', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6520', N'KCP BANDUNG MAJALAYA', N'BSI-19134', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19134', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6521', N'KCP BANDUNG CIJERAH', N'BSI-19135', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19135', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6522', N'KCP BANDUNG UJUNG BERUNG 1', N'BSI-19136', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19136', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6523', N'KCP CIANJUR CIRANJANG', N'BSI-19137', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19137', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6524', N'KCP CIANJUR WARUNG KONDANG', N'BSI-19138', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19138', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6525', N'KCP CIREBON ARJAWINANGUN', N'BSI-19139', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19139', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6526', N'KCP MAJALENGKA KADIPATEN', N'BSI-19140', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19140', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6527', N'KCP SUMEDANG TANJUNG SARI 2', N'BSI-19160', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19160', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6528', N'KCP PADALARANG', N'BSI-19161', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19161', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6529', N'KCP SUKABUMI CIBADAK SILIWANGI', N'BSI-19180', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19180', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6530', N'KCP SOREANG WAHID HASYIM', N'BSI-19188', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19188', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6531', N'KCP SUBANG OTISTA 2', N'BSI-19189', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19189', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6532', N'KCP PANGANDARAN', N'BSI-19190', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19190', N'11', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6533', N'KCP SUKABUMI PELABUHAN RATU', N'BSI-19197', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19197', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6534', N'KCP CIANJUR SUKANAGARA', N'BSI-19198', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19198', N'5', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6535', N'KCP BANDUNG KIARACONDONG', N'BSI-19258', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'19258', N'4', N'5', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6536', N'KC SURABAYA DARMO', N'BSI-10010', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10010', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6537', N'KC PAMEKASAN', N'BSI-10014', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10014', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6538', N'KC MALANG SUTOYO', N'BSI-10028', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10028', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6539', N'KC MATARAM HASANUDIN', N'BSI-10031', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10031', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6540', N'KCP KEDIRI HAYAM WURUK', N'BSI-10048', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10048', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6541', N'KC JEMBER', N'BSI-10051', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10051', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6542', N'KC BANYUWANGI', N'BSI-10060', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10060', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6543', N'KC SIDOARJO JENGGOLO', N'BSI-10098', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10098', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6544', N'KC GRESIK', N'BSI-10099', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10099', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6545', N'KC BOJONEGORO', N'BSI-10100', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10100', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6546', N'KCP MOJOKERTO', N'BSI-10101', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10101', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6547', N'KCP SURABAYA AMPEL MAS MANSYUR', N'BSI-10102', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10102', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6548', N'KCP TUBAN', N'BSI-10103', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10103', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6549', N'KCP JOMBANG', N'BSI-10104', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10104', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6550', N'KCP SURABAYA SUNGKONO', N'BSI-10105', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10105', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6551', N'KC SURABAYA RAYA', N'BSI-10106', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10106', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6552', N'KCP BANGKALAN', N'BSI-10117', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10117', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6553', N'KCP PASURUAN', N'BSI-10160', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10160', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6554', N'KCP PROBOLINGGO SOETTA', N'BSI-10161', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10161', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6555', N'KCP MALANG BATU', N'BSI-10162', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10162', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6556', N'KCP SUMBAWA', N'BSI-10165', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10165', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6557', N'KCP PANCOR', N'BSI-10166', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10166', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6559', N'KC MADIUN', N'BSI-10203', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10203', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6560', N'KCP BULELENG', N'BSI-10208', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10208', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6561', N'KCP SURABAYA WIYUNG 1', N'BSI-10250', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10250', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6562', N'KCP SURABAYA DHARMAHUSADA', N'BSI-10251', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10251', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6563', N'KCP MALANG KEPANJEN 1', N'BSI-10255', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10255', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6564', N'KCP KEDIRI PARE', N'BSI-10256', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10256', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6565', N'KC KUPANG', N'BSI-10259', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10259', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6566', N'KCP SURABAYA TANJUNG PERAK', N'BSI-10273', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10273', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6567', N'KCP SUMENEP', N'BSI-10282', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10282', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6568', N'KCP SAMPANG', N'BSI-10283', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10283', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6569', N'KCP PANDAAN A YANI', N'BSI-10302', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10302', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6570', N'KCP LUMAJANG', N'BSI-10303', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10303', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6571', N'KCP MALANG LAWANG', N'BSI-10304', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10304', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6572', N'KCP KRIAN', N'BSI-10307', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10307', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6573', N'KCP PRAYA', N'BSI-10308', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10308', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6574', N'KCP BIMA', N'BSI-10309', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10309', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6575', N'KCP LAMONGAN', N'BSI-10319', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10319', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6576', N'KCP NGANJUK', N'BSI-10331', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10331', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6577', N'KCP BONDOWOSO', N'BSI-10337', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10337', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6578', N'KCP SITUBONDO BASUKI RAHMAT', N'BSI-10338', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10338', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6579', N'KCP JEMBER BALUNG', N'BSI-10339', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10339', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6580', N'KCP DENPASAR RENON', N'BSI-10340', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10340', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6581', N'KCP PONOROGO SOETTA', N'BSI-10350', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10350', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6582', N'KCP GENTENG', N'BSI-10351', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10351', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6583', N'KC BLITAR', N'BSI-10374', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10374', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6584', N'KCP KUTA', N'BSI-10397', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10397', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6585', N'KCP SURABAYA KLAMPIS', N'BSI-10454', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10454', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6586', N'KCP SURABAYA JEMBATAN MERAH', N'BSI-10455', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10455', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6587', N'KCP NGAWI', N'BSI-10467', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10467', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6588', N'KCP SURABAYA KEDUNGDORO', N'BSI-10492', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10492', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6589', N'KCP SIDOARJO WARU', N'BSI-10497', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10497', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6590', N'KCP BOJONEGORO SUMBERREJO', N'BSI-10498', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10498', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6591', N'KCP SURABAYA UNAIR', N'BSI-10501', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10501', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6592', N'KCP MALANG KAWI', N'BSI-10512', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10512', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6593', N'KCP SURABAYA RUNGKUT', N'BSI-10528', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10528', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6594', N'KCP TRENGGALEK', N'BSI-10529', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10529', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6595', N'KCP MAGETAN', N'BSI-10590', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10590', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6596', N'KCP PACITAN', N'BSI-10591', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10591', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6597', N'KCP SURABAYA ITS', N'BSI-10646', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10646', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6598', N'KCP BANYUWANGI ROGOJAMPI', N'BSI-10654', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10654', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6599', N'KCP MALANG TUREN', N'BSI-10659', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10659', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6600', N'KCP SURABAYA PASAR ATOM', N'BSI-10670', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10670', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6601', N'KCP MALANG PASAR BESAR', N'BSI-10703', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10703', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6602', N'KCP SIDOARJO SEPANJANG', N'BSI-10719', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'10719', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6603', N'KC MALANG SUPRAPTO', N'BSI-18008', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18008', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6604', N'KC SURABAYA BASUKI RAHMAT', N'BSI-18018', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18018', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6605', N'KCP KEDIRI TRADE CENTER', N'BSI-18027', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18027', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6606', N'KCP JEMBER GAJAH MADA', N'BSI-18028', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18028', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6607', N'KC SURABAYA DHARMAWANGSA', N'BSI-18039', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18039', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6608', N'KCP DENPASAR GATOT SUBROTO', N'BSI-18041', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18041', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6609', N'KCP MATARAM PEJANGGIK 1', N'BSI-18042', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18042', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6610', N'KCP PROBOLINGGO', N'BSI-18054', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18054', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6611', N'KCP SURABAYA MERR 1', N'BSI-18058', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18058', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6612', N'KCP BIMA SOETTA', N'BSI-18070', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18070', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6613', N'KCP PASURUAN BALAIKOTA', N'BSI-18084', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18084', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6614', N'KCP BATU AGUS SALIM', N'BSI-18085', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18085', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6615', N'KCP SURABAYA MANUKAN', N'BSI-18130', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18130', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6616', N'KCP SIDOARJO GAJAH MADA', N'BSI-18131', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18131', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6617', N'KCP GRESIK SUDIRMAN', N'BSI-18132', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18132', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6618', N'KCP SURABAYA DIPONEGORO', N'BSI-18133', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18133', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6619', N'KCP MADIUN', N'BSI-18162', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18162', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6620', N'KCP TULUNGAGUNG TRADE CENTER', N'BSI-18163', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18163', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6621', N'KCP KEDIRI GUDANG GARAM', N'BSI-18164', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18164', N'22', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6622', N'KCP BANYUWANGI S PARMAN', N'BSI-18165', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18165', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6623', N'KCP JEMBER AMBULU ', N'BSI-18166', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18166', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6624', N'KCP BONDOWOSO A YANI', N'BSI-18167', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18167', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6625', N'KCP GENTENG DIPONEGORO', N'BSI-18168', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18168', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6626', N'KCP KENCONG ', N'BSI-18169', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18169', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6627', N'KCP SITUBONDO A YANI', N'BSI-18170', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18170', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6628', N'KCP SIDOARJO PONDOK CANDRA', N'BSI-18208', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18208', N'40', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6629', N'KCP JOMBANG A WAHID', N'BSI-18209', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18209', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6630', N'KCP JEMBRANA', N'BSI-18210', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18210', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6631', N'KCP SELONG', N'BSI-18211', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18211', N'45', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6632', N'KCP LAMONGAN PASAR BABAT', N'BSI-18245', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18245', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6633', N'KCP MOJOKERTO SURODINAWAN', N'BSI-18249', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18249', N'41', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6634', N'KCP PANDAAN KARTINI', N'BSI-18250', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18250', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6635', N'KCP MALANG SINGOSARI 1', N'BSI-18251', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18251', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6636', N'KCP MALANG SAWOJAJAR', N'BSI-18252', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18252', N'26', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6637', N'KCP JEMBER KARIMATA', N'BSI-18254', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18254', N'19', N'6', NULL)
GO

INSERT INTO [dbo].[users] ([id], [nama], [username], [password], [id_groups], [status], [create_by], [create_date], [idbank], [branch_code], [code_area], [code_region], [asuransi]) VALUES (N'6638', N'KCP DOMPU', N'BSI-18302', N'12345678', N'1002', N'1', NULL, NULL, NULL, N'18302', N'45', N'6', NULL)
GO

SET IDENTITY_INSERT [dbo].[users] OFF
GO

COMMIT
GO


-- ----------------------------
-- Auto increment value for groups
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[groups]', RESEED, 1008)
GO


-- ----------------------------
-- Auto increment value for menu
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[menu]', RESEED, 1001)
GO


-- ----------------------------
-- Auto increment value for produksi
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[produksi]', RESEED, 1)
GO


-- ----------------------------
-- Primary Key structure for table produksi
-- ----------------------------
ALTER TABLE [dbo].[produksi] ADD CONSTRAINT [PK__produksi__3213E83F37D22F5F] PRIMARY KEY CLUSTERED ([id])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Auto increment value for produksi_rincianAsuransi
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[produksi_rincianAsuransi]', RESEED, 1)
GO


-- ----------------------------
-- Auto increment value for produksi_tsi
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[produksi_tsi]', RESEED, 1)
GO


-- ----------------------------
-- Auto increment value for rolemenu
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[rolemenu]', RESEED, 1001)
GO


-- ----------------------------
-- Auto increment value for users
-- ----------------------------
DBCC CHECKIDENT ('[dbo].[users]', RESEED, 7287)
GO

