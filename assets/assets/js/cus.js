function message(icon,text)
{
    Swal.fire({
      icon: icon,
      title: 'success',
      text: text,
      showCancelButton: false,
      showConfirmButton: false,
      timer: 2000,
      timerProgressBar: true
    });
}

function warning(icon,text)
{
    Swal.fire({
      icon: icon,
      title: 'Warning',
      text: text,
    allowOutsideClick: false,
    })
}


function reloadTable()
{
    $('#tablena').DataTable().ajax.reload();
}

function formatRupiah(angka, prefix)
    {
        // var angks = parseFloat(angka).toFixed(2);
        // var angksFloat = angks.toString();
        // console.log(angksFloat);
        var angs = angka;

        // console.log(angs);
        
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
        // console.log(split);
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        // console.log(rupiah);
        // console.log(split[1]);

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

