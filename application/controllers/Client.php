<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Client_m');
        $this->load->library('Excel'); 

        if(!$this->Login_m->logged_id())
        {
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

    

    function index()
    {
        $data['title']          = "Client";
        $data['sub_menu']       = 1005;
        $data['page_id']        = 1007;

        $this->template->load('template','client/index',$data);
    }

    function getData()
    {
        $results = $this->Client_m->getDataTable();
        $data = [];
        $no = $_POST['start'];
        foreach ($results as $val) {
            $row = array();
            $row[] = ++$no;

            $aksi = '<a href="'.base_url('client/edit/'.$val->id).'" class="btn btn-sm btn-primary btn-label waves-effect waves-light"><i class=" ri-edit-line label-icon align-middle fs-16 me-2"></i> Edit</a> &nbsp; <a type="button" onclick="deleteRek('."'".$val->id."','delete'".')" class="btn btn-sm btn-danger btn-label waves-effect waves-light"><i class="las la-trash label-icon align-middle fs-16 me-2"></i> Delete</a>';

            $row[] = $aksi;
            $row[] = $val->nama;
            $row[] = $val->no_hp;
            $row[] = $val->email;
            $row[] = $val->alamat;

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Client_m->count_all(),
            "recordsFiltered"   => $this->Client_m->count_filtered(),
            "data"              => $data
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($output));

    }

    function save()
    {
        extract($_POST);

        $data = array(
            'nama'              => strtoupper($nama),
            'alamat'              => ($alamat),
            'alamat_kelurahan'              => ($alamat_kelurahan),
            'alamat_kota'              => ($alamat_kota),
            'no_hp'          => strtoupper($no_hp),
            'email'            => $email,
            'create_date'       => date('Y-m-d H:i:s'),
            'create_by'         => $this->session->userdata('id')
        );

        $a = $this->db->insert('client',$data);

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }

    }

    function edit($id)
    {
        $data['title']          = "Edit Rekening";
        $data['sub_menu']       = 1005;
        $data['page_id']        = 1007;

        $data['q']              = $this->db->query("select * from client where id = $id")->row();

        $this->template->load('template','client/edit',$data);
    }

    function update($id)
    {
        extract($_POST);

        $data = array(
            'nama'              => strtoupper($nama),
            'alamat'              => ($alamat),
            'alamat_kelurahan'              => ($alamat_kelurahan),
            'alamat_kota'              => ($alamat_kota),
            'no_hp'          => strtoupper($no_hp),
            'email'            => $email,
            'create_date'       => date('Y-m-d H:i:s'),
            'create_by'         => $this->session->userdata('id')
        );

        $a = $this->db->update('client', $data, "id = $id");

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }

    }

    function delete($id)
    {
        $a = $this->db->delete('client', array('id' => $id));  

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }
    }

}

