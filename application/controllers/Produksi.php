<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Produksi_m');

        if(!$this->Login_m->logged_id())
        {
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

    function a()
    {
        $a = '486.554.410,50';
       echo str_replace(['.', ',', ',', '.'], '', $a);

    }

    function index()
    {
        $data['title']          = "Nota Debet";
        $data['sub_menu']       = 0;
        $data['page_id']        = 2;

        $this->template->load('template','produksi/index',$data);
    }

    function getData()
    {
        $results = $this->Produksi_m->getDataTable();
        $data = [];
        $no = $_POST['start'];
        foreach ($results as $val) {
            $row = array();
            $row[] = ++$no;

            $aksi = '<a href="'.base_url('produksi/edit/'.$val->id).'" class="btn btn-sm btn-primary btn-label waves-effect waves-light"><i class=" las la-eye label-icon align-middle fs-16 me-2"></i> View</a> &nbsp; <a type="button" onclick="deleteNota('."'".$val->id."','delete'".')" class="btn btn-sm btn-danger btn-label waves-effect waves-light"><i class="las la-trash label-icon align-middle fs-16 me-2"></i> Delete</a>';
           
            $row[] = $aksi;
            $row[] = $val->no_nota;
            $row[] = date('d-m-Y',strtotime($val->tgl_nota));
            $row[] = $val->jenis_asuransina;
            $row[] = $val->namana;
            $row[] = number_format($val->tsi);
            $row[] = '<i>'.$val->nama.'</i>';
            $row[] = '<i>'.date('d-m-Y H:i:s',strtotime($val->create_date)).'</i>';

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Produksi_m->count_all(),
            "recordsFiltered"   => $this->Produksi_m->count_filtered(),
            "data"              => $data
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($output));

    }

    function save()
    {
        extract($_POST);

        $tsi                                   = str_replace('.', '', $tsi);
        $rate_ppn                              = str_replace('.', '', $rate_ppn);
        $gross_premi                           = str_replace('.', '', $gross_premi);
        $gross_premi_rate                      = str_replace('.', '', $gross_premi_rate);
        $diskon_asd_persen                     = str_replace('.', '', $diskon_asd_persen);
        $diskon_asd                            = str_replace('.', '', $diskon_asd);
        $materai                               = str_replace('.', '', $materai);
        $biaya_polis                           = str_replace('.', '', $biaya_polis);
        $biaya_admin_broker                    = str_replace('.', '', $biaya_admin_broker);
        $jumlah_sebelum_admin_broker           = str_replace('.', '', $jumlah_sebelum_admin_broker);
        $jumlah_setelah_admin_broker           = str_replace('.', '', $jumlah_setelah_admin_broker);
        $premi_after_diskon                    = str_replace('.', '', $premi_after_diskon);
        $gross_broker_rate                     = str_replace('.', '', $gross_broker_rate);
        $gross_broker                          = str_replace('.', '', $gross_premi);
        $diskon_broker_persen                  = str_replace('.', '', $diskon_broker_persen);
        $diskon_broker                         = str_replace('.', '', $diskon_broker);
        $net_brokerage                         = str_replace('.', '', $net_brokerage);
        $premi_to_ceding                       = str_replace('.', '', $premi_to_ceding);

        $data = array(
            'status_produksi'                    => $statusna,
            'no_nota'                              => 'PJM-GEN-'.time(),
            'tgl_nota'                              => $tgl_nota,
            'jenis_asuransi'                        => $jenis_asuransi,
            'tertanggung'                           => $tertanggung,
            'asuransi_leader'                       => $asuransi_leader,
            'share'                                 => $share_leader,
            'no_polis'                              => $no_polis,
            'tsi'                                   => str_replace(',', '.', $tsi),
            'currency'                              => $currency,
            // 'keterangan'                         => $keterangan,
            'jangka_waktu_dari'                     => $jangka_waktu_dari,
            'jangka_waktu_sampai'                   => $jangka_waktu_sampai,
            'rate_ppn'                              => str_replace(',', '.', $rate_ppn),
            'gross_premi'                           => str_replace(',', '.', $gross_premi),
            'gross_premi_rate'                      => str_replace(',', '.', $gross_premi_rate),
            'diskon_asd_persen'                     => str_replace(',', '.', $diskon_asd_persen),
            'diskon_asd'                            => str_replace(',', '.', $diskon_asd),
            'materai'                               => str_replace(',', '.', $materai),
            'biaya_polis'                           => str_replace(',', '.', $biaya_polis),
            'biaya_admin_broker'                    => str_replace(',', '.', $biaya_admin_broker),
            'jumlah_sebelum_admin_broker'           => str_replace(',', '.', $jumlah_sebelum_admin_broker),
            'jumlah_setelah_admin_broker'           => str_replace(',', '.', $jumlah_setelah_admin_broker),
            'premi_after_diskon'                    => str_replace(',', '.', $premi_after_diskon),
            'gross_broker_rate'                     => str_replace(',', '.', $gross_broker_rate),
            'gross_broker'                          => str_replace(',', '.', $gross_premi),
            'diskon_broker_persen'                  => str_replace(',', '.', $diskon_broker_persen),
            'diskon_broker'                         => str_replace(',', '.', $diskon_broker),
            'net_brokerage'                         => str_replace(',', '.', $net_brokerage),
            'premi_to_ceding'                       => str_replace(',', '.', $premi_to_ceding),
            'unit_bisnis'                           => $unit_bisnis,
            'account_officer'                       => $this->session->userdata('id'),
            'status'                                => $statusna,
            'create_date'                           => date('Y-m-d H:i:s'),
        );


        $q = $this->db->insert('produksi',$data);
        $id_produksi = $this->db->insert_id();



        $dataTsi = array();
        $index = 0; 
        foreach($no_polis_modal_tsi as $tsina) { 

            $tsi                   = str_replace('.', '', $tsi_modal_tsi); 
            $premi_gross           = str_replace('.', '', $premi_gross_modal_tsi); 
            $asd_diskon            = str_replace('.', '', $diskon_asd_modal_tsi); 
            $premi                 = str_replace('.', '', $premi_modal_tsi); 
            $broker_diskon         = str_replace('.', '', $diskon_broker_modal_tsi); 
            $by_polis              = str_replace('.', '', $biaya_polis_modal_tsi); 
            $materai               = str_replace('.', '', $biaya_materai_modal_tsi); 
            $adm_broker            = str_replace('.', '', $biaya_adm_modal_tsi); 
            $jumlah                = str_replace('.', '', $jumlah_modal_tsi);

            array_push($dataTsi, array(
                'id_produksi'           => $id_produksi,
                'no_polis'              => $tsina,
                'nama'                  => $keterangan_modal_tsi[$index],  
                'tanggal_awal'          => $tanggal_awal_modal_tsi[$index], 
                'tanggal_akhir'         => $tanggal_akhir_modal_tsi[$index], 
                'tsi'                   => str_replace(',', '.', $tsi[$index]), 
                'premi_rate'            => $rate_premi_modal_tsi[$index], 
                'premi_gross'           => str_replace(',', '.', $premi_gross[$index]), 
                'asd_diskon_persen'     => $diskon_asd_persen_modal_tsi[$index], 
                'asd_diskon'            => str_replace(',', '.', $asd_diskon[$index]), 
                'premi'                 => str_replace(',', '.', $premi[$index]), 
                'broker_diskon_persen'  => $diskon_broker_persen_modal_tsi[$index], 
                'broker_diskon'         => str_replace(',', '.', $broker_diskon[$index]), 
                'by_polis'              => str_replace(',', '.', $by_polis[$index]), 
                'materai'               => str_replace(',', '.', $materai[$index]), 
                'adm_broker'            => str_replace(',', '.', $adm_broker[$index]), 
                'jumlah'                => str_replace(',', '.', $jumlah[$index]), 
                'create_date'           => date('Y-m-d H:i:s'), 
                'create_by'             => $this->session->userdata('id'), 
          ));
          
          $index++;
        }

        $dataAsuransi = array();
        $next = 0; 
        foreach($nama_asuransi_modal_asuransi as $asuransina) { 

            $share                   = str_replace('.', '', $share_modal_asuransi);  
            $tsi                       = str_replace('.', '', $tsi_modal_asuransi); 
            $gross_premi               = str_replace('.', '', $gross_premi_modal_asuransi); 
            $diskon                    = str_replace('.', '', $diskon_modal_asuransi); 
            $premi                     = str_replace('.', '', $premi_modal_asuransi); 
            $fee_broker                = str_replace('.', '', $fee_broker_modal_asuransi); 
            $gross_brokerage           = str_replace('.', '', $gross_brokerage_modal_asuransi); 
            $hut_premi                 = str_replace('.', '', $hut_premi_modal_asuransi); 
            $biaya_polis               = str_replace('.', '', $biaya_polis_modal_asuransi); 
            $biaya_materai             = str_replace('.', '', $biaya_materai_modal_asuransi); 
            $jumlah_sebelum_pajak      = str_replace('.', '', $jumlah_sebelum_pajak_modal_asuransi); 
            $ppn_asli                  = str_replace('.', '', $ppn_asli_modal_asuransi); 
            $ppn                       = str_replace('.', '', $ppn_modal_asuransi); 
            $pph_23                    = str_replace('.', '', $pph_23_modal_asuransi); 
            $net_premi                 = str_replace('.', '', $net_premi_modal_asuransi); 
            $jn_pajak                  = str_replace('.', '', $jn_pajak_modal_asuransi); 
            $dpp                       = str_replace('.', '', $dpp_modal_asuransi); 
            $nilai_klaim               = str_replace('.', '', $nilai_klaim_modal_asuransi);

            array_push($dataAsuransi, array(
                'id_produksi'               => $id_produksi,
                'asuransi'                  => $asuransina,
                'share'                     => str_replace(',', '.', $share[$next]),  
                'tsi'                       => str_replace(',', '.', $tsi[$next]), 
                'gross_premi'               => str_replace(',', '.', $gross_premi[$next]), 
                'diskon'                    => str_replace(',', '.', $diskon_modal_asuransi[$next]), 
                'premi'                     => str_replace(',', '.', $premi[$next]), 
                'fee_broker'                => str_replace(',', '.', $fee_broker[$next]), 
                'gross_brokerage'           => str_replace(',', '.', $gross_brokerage[$next]), 
                'hut_premi'                 => str_replace(',', '.', $hut_premi[$next]), 
                'biaya_polis'               => str_replace(',', '.', $biaya_polis[$next]), 
                'biaya_materai'             => str_replace(',', '.', $biaya_materai[$next]), 
                'jumlah_sebelum_pajak'      => str_replace(',', '.', $jumlah_sebelum_pajak[$next]), 
                'ppn_asli'                  => str_replace(',', '.', $ppn_asli[$next]), 
                'ppn'                       => str_replace(',', '.', $ppn[$next]), 
                'pph_23'                    => str_replace(',', '.', $pph_23[$next]), 
                'net_premi'                 => str_replace(',', '.', $net_premi[$next]), 
                'jn_pajak'                  => str_replace(',', '.', $jn_pajak[$next]), 
                'dpp'                       => str_replace(',', '.', $dpp[$next]), 
                'nilai_klaim'               => str_replace(',', '.', $nilai_klaim[$next]), 
                'keterangan_klaim'          => $keterangan_klaim_modal_asuransi[$next], 
                'create_date'               => date('Y-m-d H:i:s'), 
                'create_by'                 => $this->session->userdata('id'), 
          ));
          
          $next++;
        }

        

        $dataInstallment = array();
        $install = 0;
        foreach ($installment_modalInstallment_instalment as $key => $installmentna) {

            $gross_premi                       = str_replace('.', '', $installment_modalInstallment_gross_premi);
            $diskon_asd                        = str_replace('.', '', $installment_modalInstallment_diskon_asd);
            $diskon_broker                     = str_replace('.', '', $installment_modalInstallment_diskon_broker);
            $biaya_polis                       = str_replace('.', '', $installment_modalInstallment_biaya_polis);
            $biaya_materai                     = str_replace('.', '', $installment_modalInstallment_biaya_materai);
            $biaya_admin_broker                = str_replace('.', '', $installment_modalInstallment_biaya_adm_broker);
            $jumlah_setelah_admin_materai      = str_replace('.', '', $installment_modalInstallment_biaya_net);
            $brokerage                         = str_replace('.', '', $installment_modalInstallment_brokerage);

            array_push($dataInstallment, array(
                'id_produksi'                       => $id_produksi,
                'installment'                       => $installmentna,
                'tanggal_kwitansi'                  => $installment_modalInstallment_tgl_kwitansi[$install],
                'tanggal_jatuh_tempo'               => $installment_modalInstallment_jatuh_tempo[$install],
                'gross_premi'                       => str_replace(',', '.', $gross_premi[$install]),
                'diskon_asd'                        => str_replace(',', '.', $diskon_asd[$install]),
                'diskon_broker'                     => str_replace(',', '.', $diskon_broker[$install]),
                'biaya_polis'                       => str_replace(',', '.', $biaya_polis[$install]),
                'biaya_materai'                     => str_replace(',', '.', $biaya_materai[$install]),
                'biaya_admin_broker'                => str_replace(',', '.', $biaya_admin_broker[$install]),
                'jumlah_setelah_admin_materai'      => str_replace(',', '.', $jumlah_setelah_admin_materai[$install]),
                'brokerage'                         => str_replace(',', '.', $brokerage[$install]),
                'create_date'                       => date('Y-m-d H:i:s'), 
                'create_by'                         => $this->session->userdata('id'), 
          ));
          
          $install++;
        }

        $dataTransper = array();
        $transper = 0;
        foreach ($id_bank as $key => $bankna) {
             array_push($dataTransper, array(
                'id_produksi'                       => $id_produksi,
                'id_bank'                           => $bankna,
                'create_date'                       => date('Y-m-d H:i:s'), 
                'create_by'                         => $this->session->userdata('id'), 
          ));
          
          $transper++;
        }

        $this->db->insert_batch('produksi_tujuanTransper', $dataTransper);
        $this->db->insert_batch('produksi_tsi', $dataTsi);
        $this->db->insert_batch('produksi_rincianAsuransi', $dataAsuransi);
        $this->db->insert_batch('produksi_installment', $dataInstallment);

        $installment_induk = $this->db->query("select * from produksi_installment where id_produksi = $id_produksi ")->row();

        if (!empty($installment_induk)) {

            $dataInstallmentDetail = array();
            $install_detail = 0;
            foreach ($installment_modalInstallment_dua_instalment as $key => $installmentnaDetail) {

                $gross_premi           = str_replace('.', '', $installment_modalInstallment_dua_gross_premi);
                $diskon_asd            = str_replace('.', '', $installment_modalInstallment_dua_diskon_asd);
                $premi                 = str_replace('.', '', $installment_modalInstallment_dua_premi);
                $brokerage             = str_replace('.', '', $installment_modalInstallment_dua_brokerage);
                $biaya_polis           = str_replace('.', '', $installment_modalInstallment_dua_biaya_polis);
                $biaya_materai         = str_replace('.', '', $installment_modalInstallment_dua_biaya_materai);
                $tax_index             = str_replace('.', '', $installment_modalInstallment_dua_tax_index);
                $dpp                   = str_replace('.', '', $installment_modalInstallment_dua_dpp);
                $ppn                   = str_replace('.', '', $installment_modalInstallment_dua_ppn);
                $pph_23                = str_replace('.', '', $installment_modalInstallment_dua_pph);
                $iuran_ojk             = str_replace('.', '', $installment_modalInstallment_dua_iuran_ojk);
                $jumlah                = str_replace('.', '', $installment_modalInstallment_dua_jumlah);

                 array_push($dataInstallmentDetail, array(
                    'id_installment'        => $installment_induk->id,
                    'installment'           => $installmentnaDetail,
                    'asuransi'              => $installment_modalInstallment_dua_asd[$install_detail],
                    'tanggal_tempo'         => $installment_modalInstallment_dua_tgl_tempo[$install_detail],
                    'gross_premi'           => str_replace(',', '.', $gross_premi[$install_detail]),
                    'diskon_asd'            => str_replace(',', '.', $diskon_asd[$install_detail]),
                    'premi'                 => str_replace(',', '.', $premi[$install_detail]),
                    'brokerage'             => str_replace(',', '.', $brokerage[$install_detail]),
                    'biaya_polis'           => str_replace(',', '.', $biaya_polis[$install_detail]),
                    'biaya_materai'         => str_replace(',', '.', $biaya_materai[$install_detail]),
                    'tax_index'             => str_replace(',', '.', $tax_index[$install_detail]),
                    'dpp'                   => str_replace(',', '.', $dpp[$install_detail]),
                    'ppn'                   => str_replace(',', '.', $ppn[$install_detail]),
                    'pph_23'                => str_replace(',', '.', $pph_23[$install_detail]),
                    'iuran_ojk'             => str_replace(',', '.', $iuran_ojk[$install_detail]),
                    'jumlah'                => str_replace(',', '.', $jumlah[$install_detail]),
                    'create_date'           => date('Y-m-d H:i:s'), 
                    'create_by'             => $this->session->userdata('id'), 
              ));
              
              $install_detail++;
            }

        //       echo "<pre>";
        // var_dump($dataInstallment);
        // echo "</pre>";
        // die();

          

            $this->db->insert_batch('produksi_installment_detail', $dataInstallmentDetail);
            
        } else {

            $response = array(
                'status'    => 'Gagal',
                'message'   => 'Mohon periksa kembali inputan Anda!'
            );
            
            $this->output->set_content_type('application/json')->set_output(json_encode($response));
            die();

        }

        

        if ($q) {
            $response = array(
                'status'    => 'success',
                'message'   => 'Data Berhasil Disimpan'
            );
        } else {
            $response = array(
                'status'    => 'failed',
                'message'   => 'Data Gagal Disimpan'
            );
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }

    function edit($id)
    {
        $data['title']          = "Nota Debet";
        $data['sub_menu']       = 0;
        $data['page_id']        = 2;

        $data['q']              = $this->Produksi_m->get_id($id);

        // var_dump($data['q']);
        // die();
        $data['tsi']            = $this->db->query("select * from produksi_tsi where id_produksi = $id")->result_array();
        $data['asuransi']       = $this->db->query("select * from produksi_rincianAsuransi where id_produksi = $id")->result_array();
        $data['installment']    = $this->db->query("select * from produksi_installment where id_produksi = $id ")->result_array();
        $data['installment_detail'] = $this->db->query("select a.*, c.asuransi asuransina from produksi_installment_detail a join produksi_installment b on a.id_installment = b.id join asuransi c on a.asuransi = c.id where b.id_produksi = $id order by a.installment asc")->result_array();

        $this->template->load('template','produksi/edit',$data);
    }

    function print_installment($id)
    {
        $this->load->library('Pdfgenerator');

         $data['q'] = $this->db->query("select a.* from produksi_installment a where a.id_produksi = $id")->result_array();

        $file_pdf = 'Cetak Installment';
        $paper = 'A4';
        $orientation = "landscape";
        $html = $this->load->view('print/installment',$data, true);     
        $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);

    }

    function print_invoice($id)
    {
        $this->load->library('Pdfgenerator');

        $data['q'] = $this->Produksi_m->invoice($id);

        $file_pdf = 'Cetak Invoice';
        $paper = 'A4';
        $orientation = "portrait";
        $html = $this->load->view('print/invoice',$data, true);     
        $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);

        // $this->load->view('print/invoice',$data);     
    }

    function print_lampiran()
    {
        $this->load->view('print/lampiran');
    }

    function print_kwitansi($id)
    {
        $this->load->library('Pdfgenerator');

        $data['q'] = $this->Produksi_m->invoice($id);

        $file_pdf = 'Cetak Kwitansi';
        $paper = 'A4';
        $orientation = "portrait";
        $html = $this->load->view('print/kwitansi',$data, true);     
        $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);
    }

    function print_objek_pertanggungan($id)
    {

        $this->load->library('Pdfgenerator');

        $data['q'] = $this->db->query("select * from produksi_tsi where id_produksi = $id")->result_array();

        $file_pdf = 'Cetak Objek Pertanggungan';
        $paper = 'A4';
        $orientation = "landscape";
        $html = $this->load->view('print/objek_pertanggungan',$data, true);     
        $this->pdfgenerator->generate($html, $file_pdf,$paper,$orientation);

        $this->load->view('print/objek_pertanggungan');
    }

    function delete($id)
    {
        $a = $this->db->delete('produksi', array('id' => $id));
        $b = $this->db->delete('produksi_tsi', array('id_produksi' => $id));
        $c = $this->db->delete('produksi_rincianAsuransi', array('id_produksi' => $id));

        $f = $this->db->query("delete w from produksi_installment_detail w join produksi_installment e on w.id_installment = e.id where e.id_produksi = $id ");

        $h = $this->db->delete('produksi_installment', array('id_produksi' => $id));

        if ($a && $b && $c && $f && $h) {
           $message = array(
                'status'    => 'success'
           );
            
        } else {
            $message = array(
                'status'    => 'failed'
           );
        }

         $this->output->set_content_type('application/json')->set_output(json_encode($message));

    }

}