<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Users_m');
        $this->load->library('Excel'); 

        if(!$this->Login_m->logged_id())
        {
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

    

    function index()
    {
        $this->load->view('development');
    }

    

}

