<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Rekening_m');
        $this->load->library('Excel'); 

        if(!$this->Login_m->logged_id())
        {
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

    

    function index()
    {
        $data['title']          = "Rekening";
        $data['sub_menu']       = 1005;
        $data['page_id']        = 1008;

        $this->template->load('template','rekening/index',$data);
    }

    function getData()
    {
        $results = $this->Rekening_m->getDataTable();
        $data = [];
        $no = $_POST['start'];
        foreach ($results as $val) {
            $row = array();
            $row[] = ++$no;

            $aksi = '<a href="'.base_url('rekening/edit/'.$val->id).'" class="btn btn-sm btn-primary btn-label waves-effect waves-light"><i class=" ri-edit-line label-icon align-middle fs-16 me-2"></i> Edit</a> &nbsp; <a type="button" onclick="deleteRek('."'".$val->id."','delete'".')" class="btn btn-sm btn-danger btn-label waves-effect waves-light"><i class="las la-trash label-icon align-middle fs-16 me-2"></i> Delete</a>';

            if ($val->status == 1) {
                $status = '<span class="badge badge-label bg-primary"><i class="mdi mdi-circle-medium"></i> Aktif</span>';
            } else {
                $status = '<span class="badge badge-label bg-danger"><i class="mdi mdi-circle-medium"></i>Tidak Aktif</span>';
            }
           
            $row[] = $aksi;
            $row[] = $val->namaBank;
            $row[] = $val->noRekening;
            $row[] = $val->atasNama;
            $row[] = $status;

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Rekening_m->count_all(),
            "recordsFiltered"   => $this->Rekening_m->count_filtered(),
            "data"              => $data
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($output));

    }

    function save()
    {
        extract($_POST);

        $data = array(
            'namaBank'          => strtoupper($namaBank),
            'noRekening'        => $noRekening,
            'atasNama'          => strtoupper($atasNama),
            'status'            => $status,
            'create_date'       => date('Y-m-d H:i:s'),
            'create_by'         => $this->session->userdata('id')
        );

        $a = $this->db->insert('rekening',$data);

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }

    }

    function edit($id)
    {
        $data['title']          = "Edit Rekening";
        $data['sub_menu']       = 1005;
        $data['page_id']        = 1008;

        $data['q']              = $this->db->query("select * from rekening where id = $id")->row();

        $this->template->load('template','rekening/edit',$data);
    }

    function update($id)
    {
        extract($_POST);

        $data = array(
            'namaBank'          => strtoupper($namaBank),
            'noRekening'        => $noRekening,
            'atasNama'          => strtoupper($atasNama),
            'status'            => $status,
            'create_date'       => date('Y-m-d H:i:s'),
            'create_by'         => $this->session->userdata('id')
        );

        $a = $this->db->update('rekening', $data, "id = $id");

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }

    }

    function delete($id)
    {
        $a = $this->db->delete('rekening', array('id' => $id));  

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }
    }

}

