<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Users_m');
        $this->load->library('Excel'); 

        if(!$this->Login_m->logged_id())
        {
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

    

    function index()
    {
        $data['title']          = "Beranda";
        $data['sub_menu']       = 0;
        $data['page_id']        = 1;

        $this->template->load('template','contents',$data);
    }

}

