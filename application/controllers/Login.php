<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Login_m');
    }

    public function index()
    {
      if($this->Login_m->logged_id() && $this->session->userdata('status') == 1)
      {
          //jika memang session sudah terdaftar, maka redirect ke halaman dahsboard
         
        redirect('beranda');

      } else {
          extract($_POST);
          

          $this->form_validation->set_rules('username', 'username', 'required');
          $this->form_validation->set_rules('password', 'Password', 'required');

          $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
            <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

          if ($this->form_validation->run() == TRUE) {
            $checking = $this->Login_m->check_login($username,$password);

            if ($checking != FALSE) {
                foreach ($checking as $apps) {


                  if(!empty($this->session->userdata('login_referrer'))){
                    $tmp_referrer = $this->session->userdata('login_referrer');
                    $this->session->unset_userdata('login_referrer');

                    $data_session = array(
                      'id'                => $apps->id,
                      'id_groups'         => $apps->id_groups,
                      'nama'              => $apps->nama,
                      'username'          => $apps->username,
                      'status'            => $apps->status,
                      'branch_code'       => $apps->branch_code,
                      'code_area'         => $apps->code_area,
                      'code_region'       => $apps->code_region,
                      'asuransi'          => $apps->asuransi,
                      );
          
                    $this->session->set_userdata($data_session);

                    redirect($tmp_referrer);
                  }else{

                    $data_session = array(
                      'id'                => $apps->id,
                      'id_groups'         => $apps->id_groups,
                      'nama'              => $apps->nama,
                      'username'          => $apps->username,
                      'status'            => $apps->status,
                      'branch_code'       => $apps->branch_code,
                      'code_area'         => $apps->code_area,
                      'code_region'       => $apps->code_region,
                      );
          
                    $this->session->set_userdata($data_session);

                    redirect(base_url("beranda"));
                  }
                }
                  
            } else {

                $data['title'] = "Login Error";
                  $this->session->set_flashdata('message','<div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                      <i class="mdi mdi-block-helper label-icon"></i><strong>Ooops...</strong> Username atau Password Anda salah! Silahkan coba lagi!
                      <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>');
                $this->load->view('login', $data);
            }

          } else {
              $data['title'] = "Login";
              $this->load->view('login',$data);
          }

    }
    }

  public function logout() 
  {
    $this->session->unset_userdata('userid');
    $this->session->unset_userdata('id_groups');
    session_destroy();
    redirect('login');
  }

  function update($id_user)
  {
        extract($_POST);

        $upload1 = $_FILES['avatar']['name'];
        $nmfile1 = $username.'_'.time();

        if($upload1 !== "") {
            $config['upload_path']          = './assets/assets/images/users/';
            $config['allowed_types']        = 'jpg|jpge|png|gif|webp';
            $config['max_size']             = 50000;
            $config['file_name']            = $nmfile1;
           
            $this->load->library('upload', $config);
            $this->upload->do_upload('avatar');               
            $data1 = $this->upload->data();

            $d = [
                'nama'      => $nama,
                'username'  => $username,
                'password'  => md5($password),
                'avatar'    => $data1['file_name'],
            ];

        } else {
           $d = [
                'nama'      => $nama,
                'username'  => $username,
                'password'  => md5($password),
            ];
        }

        if ($this->Login_m->update($id_user,$d)) {
            $message['status'] = 'success';
        } else {
            $message['status'] = 'failed';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($message));
  }

  function updatedariberanda()
  {
        extract($_POST);

        $d = [
            'nama'      => $nama,
            'username'  => $username,
            'password'  => $password,
        ];

        if ($this->db->update('users', $d, array('id' => $id_user))) {
            $message['status'] = 'success';
            $this->logout();
        } else {
            $message['status'] = 'failed';
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($message));
  }

}