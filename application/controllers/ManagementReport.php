<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ManagementReport extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Users_m');
        $this->load->library('Excel'); 

        if(!$this->Login_m->logged_id())
        {
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

    

    function index()
    {
        $data['title']          = "Management Report";
        $data['sub_menu']       = 1002;
        $data['page_id']        = 1004;
        
        $this->db->select('*');
        $this->db->from('proyeksi');
        $query = $this->db->get();
        $proyeksi = $query->result_array();

        // var_dump($proyeksi);exit();

        $data['proyeksi']       = $proyeksi;

        $this->template->load('template','report/managementReport',$data);

    }

    function save()
    {
        extract($_POST);

        // var_dump($_POST);exit();

        $data = array(
            'produk'    => $produk,
            'periode'   => $tahun.'-'.$bulan.'-01',
            'premi'     => $premi,
            'pendapatan'    => $pendapatan,
            'create_date'   => date('Y-m-d H:i:s'),
            'create_by'     => $this->session->userdata('id')
        );

        $q = $this->db->insert('proyeksi',$data);

        if ($q) {
            $message = array(
                'status'   => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                'status'   => 'gagal'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }
    }

    function excel()
    {
        extract($_POST);

        $data['aktualtahun']   = $aktual_tahun;

        $this->load->view('report/managementReport_print',$data);
    }

}

