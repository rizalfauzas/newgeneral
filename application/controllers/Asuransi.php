<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Asuransi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Login_m');
        $this->load->model('Asuransi_m');
        $this->load->library('Excel'); 

        if(!$this->Login_m->logged_id())
        {
            // session_destroy();
            $referrer_value = current_url().($_SERVER['QUERY_STRING']!=""?"?".$_SERVER['QUERY_STRING']:""); 
            $this->session->set_userdata('login_referrer', $referrer_value);
            redirect('login');         
        }
    }

    

    function index()
    {
        $data['title']          = "Asuransi";
        $data['sub_menu']       = 1005;
        $data['page_id']        = 1006;

        $this->template->load('template','asuransi/index',$data);
    }

    function getData()
    {
        $results = $this->Asuransi_m->getDataTable();
        $data = [];
        $no = $_POST['start'];
        foreach ($results as $val) {
            $row = array();
            $row[] = ++$no;

            $aksi = '<a href="'.base_url('asuransi/edit/'.$val->id).'" class="btn btn-sm btn-primary btn-label waves-effect waves-light"><i class=" ri-edit-line label-icon align-middle fs-16 me-2"></i> Edit</a> &nbsp; <a type="button" onclick="deleteRek('."'".$val->id."','delete'".')" class="btn btn-sm btn-danger btn-label waves-effect waves-light"><i class="las la-trash label-icon align-middle fs-16 me-2"></i> Delete</a>';

            $row[] = $aksi;
            $row[] = $val->asuransi;
            // $row[] = $val->no_hp;
            // $row[] = $val->email;
            // $row[] = $val->alamat;

            $data[] = $row;
        }

        $output = array(
            "draw"              => $_POST['draw'],
            "recordsTotal"      => $this->Asuransi_m->count_all(),
            "recordsFiltered"   => $this->Asuransi_m->count_filtered(),
            "data"              => $data
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($output));

    }

    function save()
    {
        extract($_POST);

        $data = array(
            'nama'              => strtoupper($nama),
            'alamat'              => strtoupper($alamat),
            'no_hp'          => strtoupper($no_hp),
            'email'            => $email,
            'create_date'       => date('Y-m-d H:i:s'),
            'create_by'         => $this->session->userdata('id')
        );

        $a = $this->db->insert('client',$data);
        $id_asuransi = $this->db->insert_id();


        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $dataRek = array();
            $index = 0; 
            foreach($currency as $rekna) { 
                array_push($dataRek, array(
                    'id_asuransi'           => $id_asuransi,
                    'currency'              => $rekna,
                    'bank'                  => $bank[$index],  
                    'no_rekening'          => $no_rekening[$index], 
                    'atas_nama'             => $atas_nama[$index], 
                    'create_date'           => date('Y-m-d H:i:s'), 
                    'create_by'             => $this->session->userdata('id'), 
              ));
              
              $index++;
            }


            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }

    }

    function edit($id)
    {
        $data['title']          = "Edit Asuransi";
        $data['sub_menu']       = 1005;
        $data['page_id']        = 1006;

        $data['q']              = $this->db->query("select * from asuransi where id = $id")->row();

        $this->template->load('template','asuransi/edit',$data);
    }

    function update($id)
    {
        extract($_POST);

        $data = array(
            'nama'              => strtoupper($nama),
            'alamat'              => strtoupper($alamat),
            'no_hp'          => strtoupper($no_hp),
            'email'            => $email,
            'create_date'       => date('Y-m-d H:i:s'),
            'create_by'         => $this->session->userdata('id')
        );

        $a = $this->db->update('client', $data, "id = $id");

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }

    }

    function delete($id)
    {
        $a = $this->db->delete('client', array('id' => $id));  

        if ($a) {
            $message = array(
                    'status'                => 'success'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        } else {
            $message = array(
                    'status'                => 'failed'
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($message));
        }
    }

}

