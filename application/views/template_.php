<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="dark" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="img-3">


<head>

    <meta charset="utf-8" />
    <title><?=$title.$this->session->userdata('idgroups')?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Proteksi Jaya Mandiri" name="description" />
    <meta content="Proteksi Jaya Mandiri" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/assets/images/logo.png">

    <!-- jsvectormap css -->
    <link href="<?=base_url()?>assets/assets/libs/jsvectormap/css/jsvectormap.min.css" rel="stylesheet" type="text/css" />

    <!--Swiper slider css-->
    <link href="<?=base_url()?>assets/assets/libs/swiper/swiper-bundle.min.css" rel="stylesheet" type="text/css" />

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    

    <!-- Layout config Js -->
    <script src="<?=base_url()?>assets/assets/js/layout.js"></script>
    <!-- Bootstrap Css -->
    <link href="<?=base_url()?>assets/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?=base_url()?>assets/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?=base_url()?>assets/assets/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- custom Css-->
    <link href="<?=base_url()?>assets/assets/css/custom.min.css" rel="stylesheet" type="text/css" />

    <!--datatable css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/dataTables.bootstrap5.min.css" />
    <!--datatable responsive css-->
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap.min.css" />

    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.2/css/buttons.dataTables.min.css">

</head>

<body >

    <!-- Begin page -->
    <div id="layout-wrapper">

        <header id="page-topbar">
    <div class="layout-width">
        <div class="navbar-header">
            <div class="d-flex">
                <!-- LOGO -->
                <div class="navbar-brand-box horizontal-logo">
                    <a href="<?=base_url('beranda')?>" class="logo logo-dark">
                        <span class="logo-sm">
                            <!-- <img src="<?=base_url()?>assets/assets/images/logo-sm.png" alt="" height="22"> -->
                        </span>
                        <span class="logo-lg">
                            <!-- <img src="<?=base_url()?>assets/assets/images/logo-dark.png" alt="" height="17"> -->
                        </span>
                    </a>

                    <a href="<?=base_url('beranda')?>" class="logo logo-light">
                        <span class="logo-sm">
                            <!-- <img src="<?=base_url()?>assets/assets/images/logo-sm.png" alt="" height="22"> -->
                        </span>
                        <span class="logo-lg">
                            <!-- <img src="<?=base_url()?>assets/assets/images/logo-light.png" alt="" height="17"> -->
                        </span>
                    </a>
                </div>

                <button type="button" class="btn btn-sm px-3 fs-16 header-item vertical-menu-btn topnav-hamburger"
                    id="topnav-hamburger-icon">
                    <span class="hamburger-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                </button>

                <!-- App Search-->
            </div>

            <div class="d-flex align-items-center">

                <div class="dropdown d-md-none topbar-head-dropdown header-item">
                    <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle"
                        id="page-header-search-dropdown" data-bs-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class="bx bx-search fs-22"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                        aria-labelledby="page-header-search-dropdown">
                        <form class="p-3">
                            <div class="form-group m-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search ..."
                                        aria-label="Recipient's username">
                                    <button class="btn btn-primary" type="submit"><i
                                            class="mdi mdi-magnify"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="dropdown topbar-head-dropdown ms-1 header-item">
                   <!--  <button type="button" class="btn btn-icon btn-topbar btn-ghost-secondary rounded-circle"
                        id="page-header-notifications-dropdown" data-bs-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <i class='bx bx-bell fs-22'></i>
                        <span
                            class="position-absolute topbar-badge fs-10 translate-middle badge rounded-pill bg-danger">0<span
                                class="visually-hidden">unread messages</span></span>
                    </button> -->
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-end p-0"
                        aria-labelledby="page-header-notifications-dropdown">

                        <div class="dropdown-head bg-primary bg-pattern rounded-top">
                            <div class="p-3">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h6 class="m-0 fs-16 fw-semibold text-white"> Notifications </h6>
                                    </div>
                                    
                                </div>
                            </div>

                           <!--  <div class="px-2 pt-2">
                                <ul class="nav nav-tabs dropdown-tabs nav-tabs-custom" data-dropdown-tabs="true"
                                    id="notificationItemsTab" role="tablist">
                                    <li class="nav-item waves-effect waves-light">
                                        <a class="nav-link active" data-bs-toggle="tab" href="#all-noti-tab" role="tab"
                                            aria-selected="true">
                                            All (4)
                                        </a>
                                    </li>
                                </ul>
                            </div> -->

                        </div>

                        <div class="tab-content" id="notificationItemsTabContent">
                            <div class="tab-pane fade show active py-2 ps-2" id="all-noti-tab" role="tabpanel">
                                <div data-simplebar style="max-height: 300px;" class="pe-2">
                                    
                                   <!--  <div class="text-reset notification-item d-block dropdown-item position-relative">
                                        <div class="d-flex">
                                            <div class="avatar-xs me-3">
                                                <span class="avatar-title bg-soft-info text-info rounded-circle fs-16">
                                                    <i class="bx bx-badge-check"></i>
                                                </span>
                                            </div>
                                            <div class="flex-1">
                                                <a href="#!" class="stretched-link">
                                                    <h6 class="mt-0 mb-2 lh-base">Your <b>Elite</b> author Graphic
                                                        Optimization <span class="text-secondary">reward</span> is
                                                        ready!
                                                    </h6>
                                                </a>
                                                <p class="mb-0 fs-11 fw-medium text-uppercase text-muted">
                                                    <span><i class="mdi mdi-clock-outline"></i> Just 30 sec ago</span>
                                                </p>
                                            </div>
                                            <div class="px-2 fs-15">
                                                <div class="form-check notification-check">
                                                    <input class="form-check-input" type="checkbox" value=""
                                                        id="all-notification-check01">
                                                    <label class="form-check-label"
                                                        for="all-notification-check01"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="my-3 text-center">
                                        <button type="button" class="btn btn-soft-success waves-effect waves-light">View
                                            All Notifications <i class="ri-arrow-right-line align-middle"></i></button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="dropdown ms-sm-3 header-item topbar-user">
                    <button type="button" class="btn" id="page-header-user-dropdown" data-bs-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <span class="d-flex align-items-center">
                            <img class="rounded-circle header-profile-user" src="<?=base_url()?>assets/assets/images/users/ava.png"
                                alt="Header Avatar">
                            <span class="text-start ms-xl-2">
                                <span class="d-none d-xl-inline-block ms-1 fw-medium user-name-text"><?=$this->session->userdata('nama')?></span>
                                <!-- <span class="d-none d-xl-block ms-1 fs-12 text-muted user-name-sub-text">Founder</span> -->
                            </span>
                        </span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-end">
                        <!-- item-->
                        <h6 class="dropdown-header">Welcome <?=$this->session->userdata('username')?>!</h6>
                       
                        <a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#modalubahpassword"><span
                                class="badge bg-soft-success text-success mt-1 float-end">New</span><i
                                class="mdi mdi-cog-outline text-muted fs-16 align-middle me-1"></i> <span
                                class="align-middle">Settings</span></a>
                        
                        <a href="<?=base_url('login/logout')?>" class="dropdown-item" onclick="return confirm('Anda yakin akan Logout?')" ><i
                                class="mdi mdi-logout text-muted fs-16 align-middle me-1"></i> <span
                                class="align-middle" data-key="t-logout">Logout</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

        <!-- ========== App Menu ========== -->
        <div class="app-menu navbar-menu">
            <!-- LOGO -->
            <div class="navbar-brand-box">
                <!-- Dark Logo-->
                <a href="" class="logo logo-dark">
                    <span class="logo-sm">
                    </span>
                    <span class="logo-lg">
                        <img src="<?=base_url()?>assets/assets/images/bsi.png" alt="" width="70%">
                    </span>
                </a>
                <!-- Light Logo-->
                <a href="<?=base_url('beranda')?>" class="logo logo-light">
                    <span class="logo-sm">
                        <img src="<?=base_url()?>assets/assets/images/bsi.png" alt="" width="70%">
                        <p>BSI GRIYA</p>
                    </span>
                    <span class="logo-lg">
                        <img src="<?=base_url()?>assets/assets/images/bsi.png" alt="" width="70%">
                        <h5 style="color: white;">BSI GRIYA</h5>
                    </span>
                </a>
                <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover" id="vertical-hover">
                    <i class="ri-record-circle-line"></i>
                </button>
            </div>

            <div id="scrollbar">
                <div class="container-fluid">

                    <div id="two-column-menu">
                    </div>

                    <ul class="navbar-nav" id="navbar-nav">
                        <li class="menu-title"><span data-key="t-menu">Menu</span></li>

                        <?php
                            $main = $this->db->query("select distinct a.* from menu a join rolemenu b on a.id = b.id_menu where b.id_groups = '".$this->session->userdata('id_groups')."' and a.kat_menu = 0 and a.status = '1' order by a.id asc");


                            // var_dump($this->session->userdata('id_groups'));
                            // die();

                            foreach ($main->result() as $m) {
                            $subs = $this->db->query("select a.* from menu a join rolemenu b on a.id = b.id_menu where b.id_groups = '".$this->session->userdata('id_groups')."' and a.kat_menu = '".$m->id."' and a.status = '1' order by a.id asc");

                                if ($subs->num_rows() > 0) {
                                    if ($sub_menu == $m->id) {
                                        $open = 'collapsed active';
                                        $show = 'show';
                                    }else{
                                        $open = '';
                                        $show = '';
                                    }

                                    //menu collapse
                                    
                                    echo    '
                                        <li class="nav-item">
                                            <a class="nav-link menu-link '.$open.' " data-bs-target="#sidebar'.$m->id.'" data-bs-toggle="collapse" role="button" aria-expanded="false" >
                                                <i class="'.$m->icon.'"></i> <span >'.$m->nama_menu.'</span>
                                            </a>
                                        ';
                                    echo '<div class="collapse menu-dropdown '.$show.'" id="sidebar'.$m->id.'">
                                    <ul class="nav nav-sm flex-column">';

                                 foreach ($subs->result() as $s) {
                                    if ($page_id == $s->id){
                                        $aktif = 'active'; 
                                    }else{
                                        $aktif = '';
                                    }
                                    echo '<li class="nav-item">
                                            <a href="'.base_url($s->link).'" class="nav-link '.$aktif.' ">
                                                <span >'.$s->nama_menu.'</span>
                                            </a>
                                        </li>';
                                }
                                    echo "</ul>";
                                    echo "</div>";
                                    echo '</li>';
                                    //end menu collapse
                                } else {
                                    if ($page_id == $m->id) {
                                        $aktif = 'active';
                                    }else{
                                        $aktif = '';
                                    }

                                    //single menu
                                    echo '<li class="nav-item">
                                            <a href="'.base_url($m->link).'" class="nav-link menu-link '.$aktif.' ">
                                                <i class="'.$m->icon.'"></i>
                                                <span>'.$m->nama_menu.'</span>
                                            </a>
                                        </li>';
                                }
                            }
                        ?>

                        

                    </ul>
                </div>
                <!-- Sidebar -->
            </div>

            <div class="sidebar-background"></div>
        </div>
        <!-- Left Sidebar End -->
        <!-- Vertical Overlay-->
        <div class="vertical-overlay"></div>

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            <div class="page-content">
                <?=$contents?>
                <!-- container-fluids -->
            </div>
            <!-- End Page-content -->

            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-6">
                            <script>document.write(new Date().getFullYear())</script> © PJM.
                        </div>
                        <div class="col-sm-6">
                            <div class="text-sm-end d-none d-sm-block">
                                Design & Develop by Proteksi Jaya Mandiri
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->



    <!--start back-to-top-->
    <button onclick="topFunction()" class="btn btn-danger btn-icon" id="back-to-top">
        <i class="ri-arrow-up-line"></i>
    </button>
    <!--end back-to-top-->

    <div class="modal fade" id="modalubahpassword" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah Password</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-5">

                <form method="post" id="formubahpassword">
                    <div class="row mb-3">
                        <div class="col-lg-4">
                            <label for="nameInput" class="form-label">Nama</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control pe-5" name="nama" value="<?=$this->session->userdata('nama')?>">
                            <input type="hidden" class="form-control pe-5" name="id_user" value="<?=$this->session->userdata('id')?>">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-lg-4">
                            <label for="nameInput" class="form-label">Username</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="text" class="form-control pe-5" name="username" id="username"  value="<?=$this->session->userdata('username')?>">
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-lg-4">
                            <label for="nameInput" class="form-label">Password Baru</label>
                        </div>
                        <div class="col-lg-8">
                            <input type="password" class="form-control pe-5" placeholder="Masukan password" id="password-input" name="password">
                        </div>
                    </div>
                    
                
            </div>
             <div class="modal-footer">
                <button type="button" onclick="updatepassword()" class="btn btn-primary waves-effect" >Simpan</button>
                <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Tutup</button>
            </div>
            </form>
        </div>
    </div>
</div>

    <!-- JAVASCRIPT -->
    <script src="<?=base_url()?>assets/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/simplebar/simplebar.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/node-waves/waves.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/feather-icons/feather.min.js"></script>
    <script src="<?=base_url()?>assets/assets/js/pages/plugins/lord-icon-2.1.0.js"></script>
    <script src="<?=base_url()?>assets/assets/js/plugins.js"></script>

    <script src="<?=base_url()?>assets/assets/js/pages/password-addon.init.js"></script>

    <!-- apexcharts -->
    <script src="<?=base_url()?>assets/assets/libs/apexcharts/apexcharts.min.js"></script>

    <!-- Vector map-->
    <script src="<?=base_url()?>assets/assets/libs/jsvectormap/js/jsvectormap.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/jsvectormap/maps/world-merc.js"></script>

    <!--Swiper slider js-->
    <script src="<?=base_url()?>assets/assets/libs/swiper/swiper-bundle.min.js"></script>

    <!-- Dashboard init -->
    <script src="<?=base_url()?>assets/assets/js/pages/dashboard-ecommerce.init.js"></script>

    <!-- App js -->
    <script src="<?=base_url()?>assets/assets/js/app.js"></script>

    <script src="<?=base_url()?>assets/assets/js/pages/form-wizard.init.js"></script>
    <script src="<?=base_url()?>assets/assets/js/cus.js"></script>

    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>

    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script src="<?=base_url()?>assets/assets/js/pages/select2.init.js"></script>
   <!--  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script> -->


    <script type="text/javascript">

        $(document).ready(function() {
            var txt = $("#username");
              var func = function() {
                txt.val(txt.val().replace(/\s/g, ''));
              }
              txt.keyup(func).blur(func);
        });

        function logout()
            {
                Swal.fire({
                title: 'Anda Yakin?',
                text: "Akan Keluar Dari Aplikasi?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Keluar!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url         : "<?php echo base_url('login/logout'); ?>",
                            type        : "post",
                            data        : $(this).serialize(),
                            success:function()
                            {
                               window.location.replace("<?=base_url('login')?>");
                            },
                            error:function()
                            {
                                Swal.fire(
                                  'Gagal!',
                                  'Gagal untuk Logout',
                                  'error'
                                )
                            }
                        })
                    }
                })
            }

        function updatepassword()
        {
            $.ajax({
                url         : "<?php echo base_url('login/update'); ?>",
                type        : "post",
                data        : $('#formubahpassword').serialize(),
                success:function()
                {
                   message('success','Berhasil Ubah Password, anda akan di alihkan ke halaman login ...');
                   location.reload();
                },
                error:function()
                {
                    warning('warning','Oopss... Gagal ubah password');
                }
            });
        }

    </script>

</body>


</html>