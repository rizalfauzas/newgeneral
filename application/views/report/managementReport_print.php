<?php
 header("Content-type: application/vnd-ms-excel");
 
 header("Content-Disposition: attachment; filename=Proyeksi.xls");
 
 header("Pragma: no-cache");
 
 header("Expires: 0");

 $proyeksitahun = (int)$aktualtahun + 1;
 ?>
 <div style="width:100%;">
  
 <div style="width: 100%; text-align:center;">
    <h3>MANAGEMENT REPORT</h3>
</div>
 </div>

</br>

<table>
    <tbody>
        <tr style="font-size: 10px;font-weight:bold;">
            <td>Periode </td>
            <td>: <?=$aktualtahun?></td>
        </tr>
        <tr style="font-size: 7px;font-weight:bold;">
            <td>generate at </td>
            <td>: <?= date('Ymd H:i:s') ?></td>
        </tr>
    </tbody>
</table>

</br>

<div style="width: 100%; text-align:left;">
    <h4>AKTUAL PERIODE : <?=$aktualtahun?></h4>
</div>

<div>
    <h5>MITRAGUNA, PRAPEN, PENSIUN</h5>
</div>

 <table border="1" width="100%" style="align-content: center;">
    <thead>
         <tr style="background-color:#B0B0B0;font-size:14px;">
            <th rowspan="2">Jenis Lini Usaha</th>
            <?php

            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }
                $periode_proyeksi = $aktualtahun."-".$bulan."-01";
                ?>
                <th class="text-center" colspan="2">Aktual <?=date('F Y',strtotime($periode_proyeksi));?></th>
            <?php } ?>
        </tr>
        <tr>
            <?php 
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }
            ?>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Premi</th>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Pendapatan Jasa Keperantaan</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td style="text-align: center;">MITRAGUNA</td>
        <?php
             for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $bulantahun = date($aktualtahun.'-'.$bulan.'-01');
                $dbMitraguna = $this->load->database('db4', TRUE);
                $query = "SELECT SUM(premi) as total_gross, sum(premi_before_admin) as total_premi, 
                id_asuransi, produk FROM tm_penutupan 
                WHERE status = 1 AND status_data = 1 AND produk = 3
                AND id_im is not null AND MONTH(createdon) = '$bulan' AND YEAR(createdon) = '$aktualtahun'
                GROUP BY id_asuransi, produk
                ORDER BY produk ASC";
                $qData = $dbMitraguna->query($query)->result_array();
                $arrData = array();
                $premi = 0;
                $fee_broker = 0;

                if(!empty($qData)){
                    foreach($qData as $vals){
                        $premi = $premi + $vals['total_gross'];
                        if($vals['id_asuransi'] == 2){
                            $fee_broker = $fee_broker + (($vals['total_premi'] * 10) / 100);
                        }else{
                            $fee_broker = $fee_broker + (($vals['total_premi'] * 5) / 100);
                        }
                    }
                }
            ?>
            
            <td  style="text-align: center;"><?=number_format($premi)?></td>
            <td  style="text-align: center;"><?=number_format($fee_broker)?></td>
            <?php } ?>
        </tr>
        
        <tr>
            <td style="text-align: center;">PRAPENSIUN</td>
        <?php
             for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $bulantahun = date($aktualtahun.'-'.$bulan.'-01');
                $dbMitraguna = $this->load->database('db4', TRUE);
                $query = "SELECT SUM(premi) as total_gross, sum(premi_before_admin) as total_premi, 
                id_asuransi, produk FROM tm_penutupan 
                WHERE status = 1 AND status_data = 1 AND produk = 4
                AND id_im is not null AND MONTH(createdon) = '$bulan' AND YEAR(createdon) = '$aktualtahun'
                GROUP BY id_asuransi, produk
                ORDER BY produk ASC";
                $qData = $dbMitraguna->query($query)->result_array();
                $arrData = array();
                $premi = 0;
                $fee_broker = 0;

                if(!empty($qData)){
                    foreach($qData as $vals){
                        $premi = $premi + $vals['total_gross'];
                            if($vals['id_asuransi'] == 2){
                                $fee_broker = $fee_broker + (($vals['total_premi'] * 5) / 100);
                            }else{
                                $fee_broker = $fee_broker + (($vals['total_premi'] * 1) / 100);
                            }
                      }
                }
            ?>
            <td  style="text-align: center;"><?=number_format($premi)?></td>
            <td  style="text-align: center;"><?=number_format($fee_broker)?></td>
            <?php } ?>
        </tr>

        <tr>
            <td style="text-align: center;">PENSIUN</td>
        <?php
             for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $bulantahun = date($aktualtahun.'-'.$bulan.'-01');
                $dbMitraguna = $this->load->database('db4', TRUE);
                $query = "SELECT SUM(premi) as total_gross, sum(premi_before_admin) as total_premi, 
                id_asuransi, produk FROM tm_penutupan 
                WHERE status = 1 AND status_data = 1 AND produk = 2
                AND id_im is not null AND MONTH(createdon) = '$bulan' AND YEAR(createdon) = '$aktualtahun'
                GROUP BY id_asuransi, produk
                ORDER BY produk ASC";
                $qData = $dbMitraguna->query($query)->result_array();
                $arrData = array();
                $premi = 0;
                $fee_broker = 0;

                if(!empty($qData)){
                    foreach($qData as $vals){
                        $premi = $premi + $vals['total_gross'];
                            if($vals['id_asuransi'] == 2){
                                $fee_broker = $fee_broker + (($vals['total_premi'] * 5) / 100);
                            }else{
                                $fee_broker = $fee_broker + (($vals['total_premi'] * 1) / 100);
                            }
                      }
                }
            ?>
            <td  style="text-align: center;"><?=number_format($premi)?></td>
            <td  style="text-align: center;"><?=number_format($fee_broker)?></td>
            <?php } ?>
        </tr>
    </tbody>
</table>

<div>
    <h5>GRIYA, TAPERA, FLPP</h5>
</div>

 <table border="1" width="100%" style="align-content: left;">
    <thead>
         <tr style="background-color:#B0B0B0;font-size:14px;">
            <th rowspan="2">Jenis Lini Usaha</th>
            <?php

            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }
                $periode_proyeksi = $aktualtahun."-".$bulan."-01";
                ?>
                <th class="text-center" colspan="2">Aktual <?=date('F Y',strtotime($periode_proyeksi));?></th>
            <?php } ?>
        </tr>
        <tr>
            <?php 
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }
            ?>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Premi</th>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Pendapatan Jasa Keperantaan</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td style="text-align: center;">GRIYA</td>
        <?php
             for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $bulantahun = date($aktualtahun.'-'.$bulan.'-01');
                $dbBsiGriya = $this->load->database('db2', TRUE);
                $query = "SELECT SUM(total_gross) as total_gross, sum(gross) as total_premi FROM penutupan 
                WHERE statusAktif = 1 AND status_akad = 1 AND MONTH(create_date) = '$bulan' AND YEAR(create_date) = '$aktualtahun'
                GROUP BY no_aplikasi";

                $qData = $dbBsiGriya->query($query)->result_array();
                $arrData = array();
                $premi = 0;
                $fee_broker = 0;

                if(!empty($qData)){
                    foreach($qData as $vals){
                        $premi = $vals['total_gross'];
                        $fee_broker = (($vals['total_gross'] * 0.5) / 100);
                    }
                }
            ?>
            
            <td  style="text-align: center;"><?=number_format($premi)?></td>
            <td  style="text-align: center;"><?=number_format($fee_broker)?></td>
            <?php } ?>
        </tr>
        
        <tr>
            <td style="text-align: center;">FLPP</td>
        <?php
             for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $bulantahun = date($aktualtahun.'-'.$bulan.'-01');
                $dbBsiFlpp = $this->load->database('db3', TRUE);
                $query = "SELECT SUM(premi) as total_gross FROM tm_penutupan 
                WHERE status = 1 AND MONTH(periode) = '$bulan' AND YEAR(periode) = '$aktualtahun' and fasilitas_kredit = 'FLPP'
                GROUP BY fasilitas_kredit";
                $qData = $dbBsiFlpp->query($query)->result_array();
                $arrData = array();
                $premi = 0;
                $fee_broker = 0;

                if(!empty($qData)){
                    foreach($qData as $vals){
                        $premi = $vals['total_gross'];
                            $fee_broker = (($vals['total_gross'] * 5) / 100);
                      }
                }
            ?>
            <td  style="text-align: center;"><?=number_format($premi)?></td>
            <td  style="text-align: center;"><?=number_format($fee_broker)?></td>
            <?php } ?>
        </tr>

        <tr>
            <td style="text-align: center;">TAPERA</td>
        <?php
             for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $bulantahun = date($aktualtahun.'-'.$bulan.'-01');
                $dbBsiFlpp = $this->load->database('db3', TRUE);
                $query = "SELECT SUM(premi) as total_gross FROM tm_penutupan 
                WHERE status = 1 AND MONTH(periode) = '$bulan' AND YEAR(periode) = '$aktualtahun' and fasilitas_kredit = 'TAPERA'
                GROUP BY fasilitas_kredit";
                $qData = $dbBsiFlpp->query($query)->result_array();
                $arrData = array();

                if(!empty($qData)){
                    foreach($qData as $vals){
                        $premi = $vals['total_gross'];
                            $fee_broker = (($vals['total_gross'] * 5) / 100);
                      }
                }
            ?>
            <td  style="text-align: center;"><?=number_format($premi)?></td>
            <td  style="text-align: center;"><?=number_format($fee_broker)?></td>
            <?php } ?>
        </tr>
    </tbody>
</table>

<!-- <div>
    <h5>KLAIM BWS</h5>
</div>

 <table border="1" width="100%" style="align-content: center;">
    <thead>
         <tr style="background-color:#B0B0B0;font-size:14px;">
            <th rowspan="2">Jenis Lini Usaha</th>
            <?php

            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }
                $periode_proyeksi = $aktualtahun."-".$bulan."-01";
                ?>
                <th class="text-center" colspan="2">Aktual <?=date('F Y',strtotime($periode_proyeksi));?></th>
            <?php } ?>
        </tr>
        <tr>
            <?php 
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }
            ?>
            <th style="background-color:#B0B0B0;" style="text-align: center;">OS Klaim</th>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Klaim Dibayar</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td style="text-align: center;">BWS Klaim</td>
        <?php
             for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $bulantahun = date($aktualtahun.'-'.$bulan.'-01');
                $dbMitraguna = $this->load->database('dbMitraguna', TRUE);
                $query = "SELECT SUM(premi) as total_gross, sum(premi_before_admin) as total_premi, 
                id_asuransi, produk FROM tm_penutupan 
                WHERE status = 1 AND status_data = 1 AND produk = 3
                AND id_im is not null AND MONTH(createdon) = '$bulan' AND YEAR(createdon) = '$aktualtahun'
                GROUP BY id_asuransi, produk
                ORDER BY produk ASC";
                $qData = $dbMitraguna->query($query)->result_array();
                $arrData = array();
                $premi = 0;
                $fee_broker = 0;

                if(!empty($qData)){
                    foreach($qData as $vals){
                        $premi = $premi + $vals['total_gross'];
                        if($vals['id_asuransi'] == 2){
                            $fee_broker = $fee_broker + (($vals['total_premi'] * 10) / 100);
                        }else{
                            $fee_broker = $fee_broker + (($vals['total_premi'] * 5) / 100);
                        }
                    }
                }
            ?>
            
            <td  style="text-align: center;"><?=number_format($premi)?></td>
            <td  style="text-align: center;"><?=number_format($fee_broker)?></td>
            <?php } ?>
        </tr>
    </tbody>
</table> -->

<div style="width: 100%; text-align:center;">
    <h4>PROYEKSI PERIODE : 2024</h4>
</div>

<div>
    <h5>MITRAGUNA, PRAPEN, PENSIUN</h5>
</div>

<table border="1" width="100%" style="align-content: center;">
    <thead>
         <tr style="background-color:#B0B0B0;font-size:14px;">
            <th rowspan="2">Jenis Lini Usaha</th>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                } 
                $periode_proyeksi = $proyeksitahun."-".$bulan."-01";
                ?>
                
                <th class="text-center" colspan="2">Proyeksi <?=date('F Y',strtotime($periode_proyeksi));?></th>
                <?php } ?>
        </tr>
        <tr>
            <?php 
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                } 
            ?>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Premi</th>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Pendapatan Jasa Keperantaan</th>
            <?php } ?>
        </tr>
       
    </thead>
    <tbody>
        <tr>
            <td style="text-align: center;">MITRAGUNA</td>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $qData = "SELECT premi, pendapatan FROM proyeksi WHERE 
                  YEAR(periode) = '".$proyeksitahun."' AND MONTH(periode) = '".$bulan."' AND produk = 'MITRAGUNA' ";
                $produk = $this->db->query($qData)->row();

                if(!empty($produk)){
            ?>
                <td  style="text-align: center;"><?=number_format($produk->premi)?></td>
                <td  style="text-align: center;"><?=number_format($produk->pendapatan)?></td>
            <?php }else{ ?>
                <td  style="text-align: center;">0</td>
                <td  style="text-align: center;">0</td>
            <?php } } ?>
        </tr>

        <tr>
            <td style="text-align: center;">PRAPENSIUN</td>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $qData = "SELECT premi, pendapatan FROM proyeksi WHERE 
                  YEAR(periode) = '".$proyeksitahun."' AND MONTH(periode) = '".$bulan."' AND produk = 'PRAPENSIUN' ";
                $produk = $this->db->query($qData)->row();

                if(!empty($produk)){
            ?>
                <td  style="text-align: center;"><?=number_format($produk->premi)?></td>
                <td  style="text-align: center;"><?=number_format($produk->pendapatan)?></td>
            <?php }else{ ?>
                <td  style="text-align: center;">0</td>
                <td  style="text-align: center;">0</td>
            <?php } } ?>
        </tr>

        <tr>
            <td style="text-align: center;">PENSIUN</td>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $qData = "SELECT premi, pendapatan FROM proyeksi WHERE 
                  YEAR(periode) = '".$proyeksitahun."' AND MONTH(periode) = '".$bulan."' AND produk = 'PENSIUN' ";
                $produk = $this->db->query($qData)->row();

                if(!empty($produk)){
            ?>
                <td  style="text-align: center;"><?=number_format($produk->premi)?></td>
                <td  style="text-align: center;"><?=number_format($produk->pendapatan)?></td>
            <?php }else{ ?>
                <td  style="text-align: center;">0</td>
                <td  style="text-align: center;">0</td>
            <?php } } ?>
        </tr>

    </tbody>
</table>

<div>
    <h5>GRIYA, FLPP, TAPERA</h5>
</div>

<table border="1" width="100%" style="align-content: center;">
    <thead>
         <tr style="background-color:#B0B0B0;font-size:14px;">
            <th rowspan="2">Jenis Lini Usaha</th>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                } 
                $periode_proyeksi = $proyeksitahun."-".$bulan."-01";
                ?>
                
                <th class="text-center" colspan="2">Proyeksi <?=date('F Y',strtotime($periode_proyeksi));?></th>
                <?php } ?>
        </tr>
        <tr>
            <?php 
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                } 
            ?>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Premi</th>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Pendapatan Jasa Keperantaan</th>
            <?php } ?>
        </tr>
       
    </thead>
    <tbody>
        <tr>
            <td style="text-align: center;">GRIYA</td>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $qData = "SELECT premi, pendapatan FROM proyeksi WHERE 
                  YEAR(periode) = '".$proyeksitahun."' AND MONTH(periode) = '".$bulan."' AND produk = 'GRIYA' ";
                $produk = $this->db->query($qData)->row();

                if(!empty($produk)){
            ?>
                <td  style="text-align: center;"><?=number_format($produk->premi)?></td>
                <td  style="text-align: center;"><?=number_format($produk->pendapatan)?></td>
            <?php }else{ ?>
                <td  style="text-align: center;">0</td>
                <td  style="text-align: center;">0</td>
            <?php } } ?>
        </tr>

        <tr>
            <td style="text-align: center;">FLPP</td>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $qData = "SELECT premi, pendapatan FROM proyeksi WHERE 
                  YEAR(periode) = '".$proyeksitahun."' AND MONTH(periode) = '".$bulan."' AND produk = 'FLPP' ";
                $produk = $this->db->query($qData)->row();

                if(!empty($produk)){
            ?>
                <td  style="text-align: center;"><?=number_format($produk->premi)?></td>
                <td  style="text-align: center;"><?=number_format($produk->pendapatan)?></td>
            <?php }else{ ?>
                <td  style="text-align: center;">0</td>
                <td  style="text-align: center;">0</td>
            <?php } } ?>
        </tr>

        <tr>
            <td style="text-align: center;">FLPP</td>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $qData = "SELECT premi, pendapatan FROM proyeksi WHERE 
                  YEAR(periode) = '".$proyeksitahun."' AND MONTH(periode) = '".$bulan."' AND produk = 'TAPERA' ";
                $produk = $this->db->query($qData)->row();

                if(!empty($produk)){
            ?>
                <td  style="text-align: center;"><?=number_format($produk->premi)?></td>
                <td  style="text-align: center;"><?=number_format($produk->pendapatan)?></td>
            <?php }else{ ?>
                <td  style="text-align: center;">0</td>
                <td  style="text-align: center;">0</td>
            <?php } } ?>
        </tr>
    


    </tbody>
</table>

<div>
    <h5>KLAIM BWS</h5>
</div>

<table border="1" width="100%" style="align-content: center;">
    <thead>
         <tr style="background-color:#B0B0B0;font-size:14px;">
            <th rowspan="2">Jenis Lini Usaha</th>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                } 
                $periode_proyeksi = $proyeksitahun."-".$bulan."-01";
                ?>
                
                <th class="text-center" colspan="2">Proyeksi <?=date('F Y',strtotime($periode_proyeksi));?></th>
                <?php } ?>
        </tr>
        <tr>
            <?php 
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                } 
            ?>
            <th style="background-color:#B0B0B0;" style="text-align: center;">OS Klaim</th>
            <th style="background-color:#B0B0B0;" style="text-align: center;">Klaim Dibayar</th>
            <?php } ?>
        </tr>
       
    </thead>
    <tbody>
        <tr>
            <td style="text-align: center;">KLAIM BWS</td>
            <?php
            for($i = 1; $i < 13; $i++) {
                if ($i < 10) {
                    $bulan = '0'.$i;
                } else {
                    $bulan = $i;
                }

                $qData = "SELECT premi, pendapatan FROM proyeksi WHERE 
                  YEAR(periode) = '".$proyeksitahun."' AND MONTH(periode) = '".$bulan."' AND produk = 'BWS' ";
                $produk = $this->db->query($qData)->row();

                if(!empty($produk)){
            ?>
                <td  style="text-align: center;"><?=number_format($produk->premi)?></td>
                <td  style="text-align: center;"><?=number_format($produk->pendapatan)?></td>
            <?php }else{ ?>
                <td  style="text-align: center;">0</td>
                <td  style="text-align: center;">0</td>
            <?php } } ?>
        </tr>
    </tbody>
</table>