<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title;?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=$title?></a></li>
                        <li class="breadcrumb-item active">index</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <ul class="nav nav-tabs nav-border-top nav-border-top-primary mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#data" role="tab" aria-selected="true">
                                <i class="ri-file-list-3-line"></i> Data Produksi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-bs-toggle="tab" href="#tambah" role="tab" aria-selected="true">
                                <i class="ri-file-list-3-line"></i> Tambah Proyeksi
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane p-3 active" id="data" role="tabpanel">
                            
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item border rounded">
                                  <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button fw-semibold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        FILTER DATA
                                    </button>
                                  </h2>
                                  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample" style="">
                                    <div class="accordion-body">
                                        <form id="form-filter" method="post" action="<?=base_url('managementReport/excel')?>" target="_blank">

                                            <div class="row">
                                                
                                               <!--  <div class="col-6">
                                                    <div class="mb-3">
                                                        <label for="firstNameinput" class="form-label">Aktual Bulan</label>
                                                            <select class="form-select" name="aktual_bulan" id="aktual_bulan" required>
                                                                <option value=""></option>
                                                                <option value="01">Januari</option>
                                                                <option value="02">Februari</option>
                                                                <option value="03">Maret</option>
                                                                <option value="04">April</option>
                                                                <option value="05">Mei</option>
                                                                <option value="06">Juni</option>
                                                                <option value="07">Juli</option>
                                                                <option value="08">Agustus</option>
                                                                <option value="09">September</option>
                                                                <option value="10">Oktober</option>
                                                                <option value="11">November</option>
                                                                <option value="12">Desember</option>
                                                               
                                                            </select>
                                                    </div>
                                                </div> -->

                                                <div class="col-6">
                                                    <div class="mb-3">
                                                        <label for="lastNameinput" class="form-label">Aktual Tahun</label>
                                                            <select class="form-select" name="aktual_tahun" id="aktual_tahun" required>
                                                                <option value=""></option>
                                                                <option value="2022">2022</option>
                                                                <option value="2023">2023</option>
                                                                <option value="2024">2024</option>
                                                                <option value="2025">2025</option>
                                                                <option value="2026">2026</option>
                                                                <option value="2027">2027</option>
                                                                <option value="2028">2028</option>
                                                                <option value="2029">2029</option>
                                                                <option value="2030">2030</option>
                                                            </select>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="hstack gap-3  justify-content-center">
                                            <button type="submit" class="btn btn-success" id="btn-filter" ><i class="ri-filter-line" ></i> Generate Excel</button>
                                            <!-- <div class="vr"></div> -->
                                            <!-- <button type="submit" id="btn-generate" class="btn btn-success" style="display: none" ><i class="ri-file-excel-line"></i>  Generate Report</button> -->
                                            <div class="vr" id="vr-generate" style="display: none"></div>
                                        </div>

                                        </form>
                                    </div>
                                  </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane p-3" id="tambah" role="tabpanel">
                            <div class="row">
                                <form id="form-proyeksi" method="post">

                                    <div class="row mb-3">
                                        <div class="col-lg-3">
                                            <label for="nameInput" class="form-label">Produk</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <select class="form-select" name="produk" id="Produksi">
                                                <option></option>
                                                <option value="GRIYA">GRIYA</option>
                                                <option value="FLPP">FLPP</option>
                                                <option value="TAPERA">TAPERA</option>
                                                <option value="MITRAGUNA">MITRAGUNA</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-lg-3">
                                            <label for="nameInput" class="form-label">Periode</label>
                                        </div>
                                        <div class="col-lg-3">
                                            <select class="form-select" name="bulan" id="bulan" required>
                                                <option value=""></option>
                                                <option value="01">Januari</option>
                                                <option value="02">Februari</option>
                                                <option value="03">Maret</option>
                                                <option value="04">April</option>
                                                <option value="05">Mei</option>
                                                <option value="06">Juni</option>
                                                <option value="07">Juli</option>
                                                <option value="08">Agustus</option>
                                                <option value="09">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                               
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <select class="form-select" name="tahun" id="tahun" required>
                                                <option value=""></option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                                <option value="2027">2027</option>
                                                <option value="2028">2028</option>
                                                <option value="2029">2029</option>
                                                <option value="2030">2030</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-lg-3">
                                            <label for="nameInput" class="form-label">Premi</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="premi" placeholder="Masukan Nilai Premi" required>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-lg-3">
                                            <label for="nameInput" class="form-label">Pendapatan Jasa Keperantaan</label>
                                        </div>
                                        <div class="col-lg-6">
                                            <input type="text" class="form-control" name="pendapatan" placeholder="Masukan Nilai Pendapatan" required>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-lg-3">
                                            <label for="nameInput" class="form-label"></label>
                                        </div>
                                        <div class="col-lg-6">
                                            <button type="button" onclick="save()" class="btn btn-primary btn-label waves-effect waves-light"><i class=" ri-save-line label-icon align-middle fs-16 me-2"></i> Simpan</button>
                                        </div>
                                    </div>
                                </form>
                                
                            </div>
                            
                        </div>


                    </div>

                </div>
            </div>
        </div>

    </div>



</div> <!-- container-fluid -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script> -->


<script type="text/javascript">

    $(document).ready( function(){
        // statusKtp();
        $("#tablena").DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": '<?=base_url('produksi/getData')?>',
                "type": "POST",
                "data": function ( data ) {
                    data.bulan                = $('#bulan').val();
                    data.tahun                 = $('#tahun').val();
                }
            },
            "columnDefs": [{
                "target": [-1],
                "orderable": false
            }],
            "scrollX": true
        });

        $('#btn-filter').click(function(){ 
            $('#btn-generate').show();
            $('#vr-generate').show();
            reloadTable();
        });
        $('#btn-reset').click(function(){ 
            $('#form-filter')[0].reset();
            reloadTable();
        });

    });

    function save()
    {

        let err = 0;

        var bulan = $("#bulan").val();
        if (bulan == '') {
            $(".err-bulan").show();
            err = 1;
        } else{
            $('.err-bulan').hide();
        }

        var tahun = $("#tahun").val();
        if (tahun == '') {
            $(".err-tahun").show();
            err = 1;
        } else{
            $('.err-tahun').hide();
        }

        var premi = $("#premi").val();
        if (premi == '') {
            $(".err-premi").show();
            err = 1;
        } else{
            $('.err-premi').hide();
        }

        var pendapatan = $("#pendapatan").val();
        if (pendapatan == '') {
            $(".err-pendapatan").show();
            err = 1;
        } else{
            $('.err-pendapatan').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        $.ajax({
            type: "POST",
            url: '<?=base_url('managementReport/save')?>',
            data: $('#form-proyeksi').serialize(),
            dataType: "JSON",
            success: function(message){
                if (message.status == 'success') {
                    message('success','Berhasil simpan Data');
                    window.setTimeout(function(){ 
                        window.location = "<?=base_url('managementReport')?>";
                        } ,2000);
                } else {
                    swal.close();
                    message('warning', 'Oopss.. Gagal Simpan data!');
                }


                
            },
            error: function(){
                message('warning', 'Oopss.. Gagal Simpan data!');
            }
        });
    }

</script>









