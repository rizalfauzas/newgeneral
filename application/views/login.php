<!doctype html>
<html lang="en" data-layout="vertical" data-topbar="light" data-sidebar="dark" data-sidebar-size="lg" data-sidebar-image="none">


<head>

    <meta charset="utf-8" />
    <title>Sign In</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="<?=base_url()?>assets/assets/images/logo.png">

    <!-- Layout config Js -->
    <script src="<?=base_url()?>assets/assets/js/layout.js"></script>
    <!-- Bootstrap Css -->
    <link href="<?=base_url()?>assets/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="<?=base_url()?>assets/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="<?=base_url()?>assets/assets/css/app.min.css" rel="stylesheet" type="text/css" />
    <!-- custom Css-->
    <link href="<?=base_url()?>assets/assets/css/custom.min.css" rel="stylesheet" type="text/css" />

</head>

<body style="background-image: url('<?=base_url()?>assets/assets/images/p-1.png'); background-size: cover; background-position: center center;">


    <!-- auth-page wrapper -->
    <div class="auth-page-wrapper auth-bg-cover py-5 d-flex justify-content-center align-items-center min-vh-100">
        <div class="bg-overlay"></div>
        <!-- auth-page content -->
        <div class="auth-page-content overflow-hidden pt-lg-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card overflow-hidden">
                            <div class="row g-0">
                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4 auth-one-bg h-100">
                                        <div class="bg-overlay"></div>
                                        <div class="position-relative h-100 d-flex flex-column">
                                            <div class="mb-4">
                                                <a class="d-block text-center">
                                                    <img src="<?=base_url()?>assets/assets/images/logo-pjm-putih.png" alt="" height="60">
                                                </a>
                                            </div>
                                            <div class="mt-auto">
                                                <div class="mb-3">
                                                    <i class="ri-double-quotes-l display-4 text-success"></i>
                                                </div>

                                                <div id="qoutescarouselIndicators" class="carousel slide" data-bs-ride="carousel">
                                                    <div class="carousel-indicators">
                                                        <button type="button" data-bs-target="#qoutescarouselIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                                                        <button type="button" data-bs-target="#qoutescarouselIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                                                        <button type="button" data-bs-target="#qoutescarouselIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                                                    </div>
                                                    <div class="carousel-inner text-center text-white-50 pb-5">
                                                        <div class="carousel-item active">
                                                            <p class="fs-15 fst-italic">" Masa depan tergantung dengan apa yang kita lakukan hari ini "</p>
                                                        </div>
                                                        <div class="carousel-item">
                                                            <p class="fs-15 fst-italic">" Sometimes you have to fall before you fly "</p>
                                                        </div>
                                                        <div class="carousel-item">
                                                            <p class="fs-15 fst-italic">" Work hard stay humble "</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end carousel -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end col -->

                                <div class="col-lg-6">
                                    <div class="p-lg-5 p-4">
                                        <div>
                                            <h5 class="text-primary">Selamat <?php if (date('H:i:s') > '00:01:00' && date('H:i:s') < '10:00:00') {
                                      $selamatnaon = 'Pagi';
                                    } else if (date('H:i:s') > '10:00:00' && date('H:i:s') < '15:00:00') {
                                      $selamatnaon = 'Siang';
                                    } else if (date('H:i:s') > '15:00:00' && date('H:i:s') < '18:00:00') {
                                      $selamatnaon = 'Sore';
                                    } else if (date('H:i:s') > '18:00:00' && date('H:i:s') < '23:58:59') {
                                      $selamatnaon = 'Malam';
                                    } echo $selamatnaon;?>!</h5>
                                    <p class="text-muted">Selamat Datang di Aplikasi GENERAL</p>
                                        </div>

                                        <div class="mt-4">
                                            <?= $this->session->flashdata('message');?>
                                            <form method="post" id="formLogin" action="<?=base_url('login')?>">

                                                <div class="mb-3">
                                                    <label for="username" class="form-label">Username</label>
                                                    <input type="text" class="form-control" id="username" name="username" placeholder="Masukan username" autofocus>
                                                    <div class="username-err" style="display: none"><code><i>* Wajib Diisi</i></code></div>
                                                </div>

                                                <div class="mb-3">
                                                    <div class="float-end">
                                                        <a onclick="warning('warning','Hubungi Administrator untuk mereset Password');" class="text-muted">Lupa password?</a>
                                                    </div>
                                                    <label class="form-label" for="password-input">Password</label>
                                                    <div class="position-relative auth-pass-inputgroup mb-3">
                                                        <input type="password" class="form-control pe-5" placeholder="Masukan password" id="password" name="password">
                                                        <div class="password-err" style="display: none"><code><i>* Wajib Diisi</i></code></div>
                                                    </div>
                                                </div>

                                                <div class="mt-4">
                                                    <button type="submit" class="btn btn-primary w-100" type="button" >Sign In</button>
                                                </div>

                                            </form>
                                        </div>

                                        <div class="mt-5 text-center">
                                            <p class="mb-0">Tidak Punya Akun ? <a onclick="warning('warning','Hubungi Administrator untuk membuat Akun!')" class="fw-semibold text-primary text-decoration-underline"> Daftar</a> </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- end col -->
                            </div>
                            <!-- end row -->
                        </div>
                        <!-- end card -->
                    </div>
                    <!-- end col -->

                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->

        <!-- footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <p class="mb-0">&copy;
                                <script>2023</script> General Apps. Crafted with <i class="mdi mdi-heart text-danger"></i> by PT. Proteksi Jaya Mandiri
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
    <!-- end auth-page-wrapper -->

    <!-- JAVASCRIPT -->
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script type="text/javascript" src="<?=base_url()?>assets/assets/js/cus.js"></script>


    <script src="<?=base_url()?>assets/assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/simplebar/simplebar.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/node-waves/waves.min.js"></script>
    <script src="<?=base_url()?>assets/assets/libs/feather-icons/feather.min.js"></script>
    <script src="<?=base_url()?>assets/assets/js/pages/plugins/lord-icon-2.1.0.js"></script>
    <script src="<?=base_url()?>assets/assets/js/plugins.js"></script>

    <!-- password-addon init -->
    <script src="<?=base_url()?>assets/assets/js/pages/password-addon.init.js"></script>

    <script type="text/javascript">
        function login()
        {
            let err = 0;

            var username = $("#username").val();
            if (username == '') {
                $(".username-err").show();
                err = 1;
            } else{
                $('.username-err').hide();
            }

            var password = $("#password").val();
            if (password == '') {
                $(".password-err").show();
                err = 1;
            } else{
                $('.password-err').hide();
            }

            if(err == 1){
                return false;
            }

            $.ajax({
                url:'<?php echo base_url('login');?>',
                data: $("#formLogin").serialize(),
                method:"post",
                    success: function(data){
                        $('#btnSave').prop("disabled", true);
                        $('#btnSave').html(
                            '<i class=" ri-loader-2-line ri-spin" style="color:white"></i> Loading ...'
                        );
                        // message('success','Anda akah dialihkan ke Halaman Beranda');

                        window.setTimeout(function() {
                            window.location.replace("<?=base_url('beranda')?>");
                        }, 2000);
                    }, error: function(){
                         warning('error', 'Username atau Password Anda salah!');
                    }
            });
        }
    </script>

</body>


</html>