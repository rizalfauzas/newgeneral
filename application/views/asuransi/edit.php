<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Penutupan Asuransi</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row" >
        <div class="col-xxl-12">
            <!-- <h5 class="mb-3">Border Top Nav</h5> -->
            <div class="card">
                <div class="card-body" >
                   
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                    <ul class="nav nav-tabs nav-border-top nav-border-top-primary mb-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#nav-border-top-profile" role="tab" aria-selected="true">
                                                Edit Data
                                            </a>
                                        </li>
                                        
                                    </ul>

                                    <div class="tab-content text-muted">

                                        <div class="tab-pane active" id="nav-border-top-profile" role="tabpanel">
                                            <div class="d-flex">
                                                
                                                <div class="flex-grow-1 ms-2">
                                                    <br>
                                                    <form action="<?=base_url('client/save')?>" id="formna" method="post">
                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="nameInput" class="form-label">Nama</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukan Nama Client" required value="<?php if (!empty($q->nama)) {
                                                                    echo $q->nama;
                                                                }?>">
                                                                <div id="nama-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="dateInput" class="form-label">No. Telp</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Masukan No. Telp" value="<?php if (!empty($q->no_hp)) {
                                                                    echo $q->no_hp;
                                                                }?>">
                                                                <div id="no_hp-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="timeInput" class="form-label">Email</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" id="email" name="email" placeholder="Masukan Email" value="<?php if (!empty($q->email)) {
                                                                    echo $q->email;
                                                                }?>">
                                                                <div id="email-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>

                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="timeInput" class="form-label">Alamat</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                               <input class="form-control" type="text" name="alamat" id="alamat" placeholder="Nama Jalan" value="<?php if (!empty($q->alamat)) {
                                                                   echo $q->alamat;
                                                               }?>"><br>
                                                               <input class="form-control" type="text" name="alamat_kelurahan" id="alamat_kelurahan" placeholder="Kelurahan/Kecamatan" value="<?php if (!empty($q->alamat_kelurahan)) {
                                                                   echo $q->alamat_kelurahan;
                                                               }?>"><br>
                                                               <input class="form-control" type="text" name="alamat_kota" id="alamat_kota" placeholder="Kota/Kabupaten" value="<?php if (!empty($q->alamat_kota)) {
                                                                   echo $q->alamat_kota;
                                                               }?>">
                                                                <div id="email-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>

                                                        <!-- <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="timeInput" class="form-label">No. Rekening</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="table-responsive">
                                                                    <table id="tableRek" class="table table-nowrap">
                                                                        <tr>
                                                                            <td>#</td>
                                                                            <td>Currency</td>
                                                                            <td>Bank</td>
                                                                            <td>No. Rekening</td>
                                                                            <td>Atas Nama</td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td>
                                                                                <button type="button" class="btn btn-primary btn-icon waves-effect waves-light" onclick="addRowRek()"><i class=" bx bx-plus-medical"></i></button>
                                                                            </td>
                                                                            <td>
                                                                                <select class="form-select" name="currency[]" id="currency">
                                                                                    <option value=""></option>
                                                                                    <option value="IDR">IDR</option>
                                                                                    <option value="IDR">USD</option>
                                                                                    <option value="IDR">GBP</option>
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control" name="bank[]" id="bank">
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control" name="no_rekening[]" id="no_rekening">
                                                                            </td>
                                                                            <td>
                                                                                <input type="text" class="form-control" name="atas_nama[]" id="atas_nama">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div> -->

                                                        
                                                       

                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="leaveemails" class="form-label"></label>
                                                            </div>
                                                            <div class="col-lg-9">
                                                                <button type="reset" class="btn btn-warning btn-label waves-effect waves-light" >
                                                                    <i class=" ri-refresh-line label-icon align-middle fs-16 me-2">
                                                                    </i> Refresh
                                                                </button>
                                                                <button type="button" onclick="save();" class="btn btn-primary btn-label waves-effect waves-light" >
                                                                    <i class=" bx bx-save label-icon align-middle fs-16 me-2">
                                                                    </i> Simpan
                                                                </button>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div><!-- end card-body -->
                            </div>
                            
                        </div><!--end col-->
                    </div>
                </div><!-- end card-body -->
            </div>
        </div>
        <!--end col-->

    </div>


</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>


<script type="text/javascript">

    function save()
    {
        let err = 0;

        var nama = $("#nama").val();
        if (nama == '') {
            $("#nama-err").show();
            err = 1;
        } else{
            $('#nama-err').hide();
        }

        var no_hp = $("#no_hp").val();
        if (no_hp == '') {
            $("#no_hp-err").show();
            err = 1;
        } else{
            $('#no_hp-err').hide();
        }

        var alamat = $("#alamat").val();
        if (alamat == '') {
            $("#alamat-err").show();
            err = 1;
        } else{
            $('#alamat-err').hide();
        }

        var email = $("#email").val();
        if (email == '') {
            $("#email-err").show();
            err = 1;
        } else{
            $('#email-err').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        $.ajax({
            type: "POST",
            url: '<?=base_url('client/update/'.$q->id)?>',
            data: $("#formna").serialize(),
            dataType: "JSON",
            success: function(response) {
                if (response.status == 'success') {
                    reloadTable();
                    message('success','Berhasil Disimpan');
                    window.setTimeout(function(){ 
                        window.location.href = '<?=base_url('client/index');?>';
                    } ,2000);
                } else {
                    warning('warning','Gagal Disimpan, periksa kembali inputan Anda!');
                }
                
            }, error: function(response){
                warning('warning',reponse.message);
            },
        })
    }

    function addRowRek()
    {
        markup = '<tr><td><button type="button" class="btn btn-danger btn-icon waves-effect waves-light" onclick="removeRowRek()"><i class=" bx bx-minus-circle"></i></button></td><td><select class="form-select" name="currency[]" id="currency"><option value=""></option><option value="IDR">IDR</option><option value="IDR">USD</option><option value="IDR">GBP</option></td><td><input type="text" class="form-control" name="bank[]" id="bank"></td><td><input type="text" class="form-control" name="no_rekening[]" id="no_rekening"></td><td><input type="text" class="form-control" name="atas_nama[]" id="atas_nama"></td></tr>'; 
                tableBody = $("#tableRek"); 
                tableBody.append(markup); 
    }

    function removeRowRek()
    {
        $('#tableRek tbody tr:last-child').remove(); 
    }

</script>
