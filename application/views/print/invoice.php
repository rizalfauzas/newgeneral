<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Invoice</title>
	<link href="<?=base_url()?>assets/assets//css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="row">

		<div class="col-md-12">
			<p>&nbsp;</p>
			<p>&nbsp;</p>

			<p><?=$q->namana;?><br>Jl. Senopati<br>Kec. Jakarta<br>Jakarta-Selatan</p>

			<div class="text-center">
				INVOICE<br><b>No. DN/<?=$q->no_nota;?><br>Premi Asuransi</b>
			</div>

			<div class="table-responsive" style="overflow-x:auto;">
				<table class="">
					<tr>
						<td><b>I</b></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><b>NOTA</b></td>
					</tr>

					<tr>
						<td ></td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td>Tipe Asuransi</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php if (isset($q->jenis_asuransina)) {
							echo $q->jenis_asuransina;
						}?></td>
					</tr>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Penanggung</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php if (isset($q->asuransi_leader)) {
							echo $q->asuransi_leader;
						}?></td>
					</tr>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Nomor Polis Sertifikat</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php if (isset($q->no_polis)) {
							echo $q->no_polis;
						}?></td>
					</tr>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Periode Pertanggungan</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php if (isset($q->jangka_waktu_dari)) {
							echo date('d F Y',strtotime($q->jangka_waktu_dari));
						}?> - <?php if (isset($q->jangka_waktu_sampai)) {
							echo date('d F Y',strtotime($q->jangka_waktu_sampai));
						}?></td>
					</tr>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Nilai Pertanggungan</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>IDR <?php if (isset($q->tsi)) {
							echo number_format($q->tsi);
						}?></td>
					</tr>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Tertanggung</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?php if (isset($q->namana)) {
							echo $q->namana;
						}?></td>
					</tr>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Objek Pertanggungan</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<?php 
								$j = $this->db->query("select count(id) jumlahna from produksi_tsi where id_produksi = $q->id")->row();

								if (!empty($j)) {
									if ($j->jumlahna > 1 ) {
										echo "As attached";
									} else {
										echo $j->nama;
									}
								}
							?>
						</td>
					</tr>

					
				</table>


				<table>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;Jumlah Premi</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;</td>
						<td></td>
						<td>&nbsp;:</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;Total Premi</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;IDR</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;<?=number_format($q->gross_premi);?></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;</td>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;Diskon</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;IDR</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;<?php if (isset($q->diskon_asd)) {
							echo number_format($q->diskon_asd);
						}?></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;</td>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;Biaya Polis & Materai</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;IDR</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;<?php if (isset($q->biaya_polis)) {
							$by_polis = $q->biaya_polis;
						} else {
							$by_polis = 0;
						}
						if (isset($q->materai)) {
							$mater = $q->materai;
						} else {
							$mater = 0;
						}

						echo number_format($by_polis+$mater);


						?></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;</td>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;Jumlah dibayarkan ke PJM</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;IDR</td>
						<td>&nbsp;</td>
						<td>&nbsp;&nbsp;<?php if (isset($q->jumlah_setelah_admin_broker)) {
							echo number_format($q->jumlah_setelah_admin_broker);
						}?></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
					</tr>


				</table>

				<table>
					<tr>
						<td ></td>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td ></td>
						<td>Terbilang</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;<?php if (isset($q->jumlah_setelah_admin_broker)) {
							echo strtoupper(ucwords(number_to_words($q->jumlah_setelah_admin_broker)));
						}?></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
					</tr>

				</table>

				<table>
					<tr>
						<td >&nbsp;</td>
						<td >&nbsp;</td>
						<td ></td>
						<td>&nbsp;Installment</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;Keterangan</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;Due date on</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;Jumlah</td>
					</tr>

					<?php 

					$a = $this->db->query("select a.no_nota, c.nama clientna, d.jenis_asuransi jinisna, b.* from produksi a join produksi_installment b on a.id = b.id_produksi join client c on a.tertanggung = c.id join jenis_asuransi d on a.jenis_asuransi = d.id where a.id = $q->id ")->result_array();

					$no=1;

					foreach ($a as $key => $value) { ?>

						<tr>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td >&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;Installment ke <?=$no++;?> dari <?= count($a);?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;<?=date('d F Y',strtotime($value['tanggal_jatuh_tempo']));?></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;IDR</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;<?=number_format($value['jumlah_setelah_admin_materai']);?></td>
						</tr>

					<?php } ?>
					

				</table>


				

				

				<table>

					<tr>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td><b>II</b></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><b>Pembayaran mohon transfer ke rekening di bawah ini</b></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Nama Bank</td>
						<td>No. Rekening</td>
						<td>Atas Nama</td>
					</tr>

					<?php 

					$jh = $this->db->query("select a.* from rekening a join produksi_tujuanTransper b on a.id = b.id_bank where b.id_produksi = $q->id")->result_array();

                    foreach ($jh as $key => $value) { ?>

					<tr>
						<td></td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><?=$value['namaBank'];?></td>
						<td><?=$value['noRekening'];?></td>
						<td><?=$value['atasNama'];?></td>
					</tr>

				<?php } ?>

					<tr>
						<td>&nbsp;</td>
					</tr>

				</table>

				<p>
					Bukti transfer mohon di e-mail ke marketing pjm@pjmbroker.com dengan cc : boby.azhar@pjmbroker.com.<br>Atas perhatian dan kerjasamanya kami ucapkan terimakasih.<br><br> 
					Jakarta, <?=date('d F Y');?><br><b>PT. PROTEKSI JAYA MANDIRI<br><br><br><br><u style="font-style: bold;">Haposan Bakara, S.Sos, AAAI-K</u><br>Direktur
				</p>

				<div>
					
				</div>


			</div>

		</div>

		

	</div>

</body>
</html>