

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Kwitansi</title>
	<link href="<?=base_url()?>assets/assets//css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>

<?php 

$a = $this->db->query("select a.no_nota, c.nama clientna, d.jenis_asuransi jinisna, b.* from produksi a join produksi_installment b on a.id = b.id_produksi join client c on a.tertanggung = c.id join jenis_asuransi d on a.jenis_asuransi = d.id where a.id = $q->id ")->result_array();

$no=1;

foreach ($a as $key => $value) { ?>
<body>


	<div class="row">

		<div style="display: block;">
			<p>&nbsp;</p>
			<p>&nbsp;</p>
		</div>

		<div class="text-center">
			KWITANSI<br><b><?=$value['no_nota'];?>, INSTALLMENT KE-<?=$no++;?> DARI <?= count($a);?></b>
		</div>

		<div class="table-responsive">
			<table>
				<tr>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>Telah terima dari</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>:</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><?=$value['clientna'];?></td>
				</tr>

				<tr>
					<td>Uang sejmulah</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>:</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><b>IDR <?=number_format($value['jumlah_setelah_admin_materai']);?></b></td>
				</tr>

				<tr>
					<td>Terbilang</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>:</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td><?php echo strtoupper(ucwords(number_to_words($value['jumlah_setelah_admin_materai'])));?></td>
				</tr>

				<tr>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>Untuk Pembayaran</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>:</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Premi Polis Asuransi <?=$value['jinisna'];?></td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>No. Polis <?=$q->no_polis;?>, Periode <?=date('d F Y',strtotime($q->jangka_waktu_dari)).' - '.date('d F Y',strtotime($q->jangka_waktu_sampai));?></td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>Due date on <?=date('d F Y',strtotime($value['tanggal_jatuh_tempo']));?></td>
				</tr>

				
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>

			<p>
				Jakarta, <?=date('d F Y',strtotime($value['tanggal_jatuh_tempo']));?><br><b>PT. PROTEKSI JAYA MANDIRI</b><br><i>Insurance Broker & Consultants</i><br><br><br><br><b>Haposan Bakara, S.Sos, AAAI-K</b><br>Direktur
			</p>

			<div>
				
			</div>


		</div>

		

	</div>

	

</body>


<?php } ?>




</html>

