<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Daftar Objek Pertanggungan</title>
	<style>
	table {
	  font-family: arial, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}

	td, th {
	  border: 1px solid #dddddd;
	  border-style: groove;
	  text-align: left;
	  font-size: 12px;
	  padding: 8px;
	}

	/*tr:nth-child(even) {
	  background-color: #dddddd;
	}*/
	</style>
  
</head>
<body>
<h4>PT. Proteksi Jaya Mandiri</h4>
<strong>Gandaria 8 Office Lantai 12 Unit H, Jl. Sultan Iskandar Muda - Jakarta Selatan 12240</strong><br>
<strong>Telp : 021 - 2903557</strong>
<p></p>
<p></p>
<strong>Daftar Objek Pertanggungan</strong><br>
<p style="font-size: 10;"><i><?=date('d-m-Y H:i:s')?></i></p>
<p></p>

<table class="table">
    <thead class="thead-light">
		<tr>
			<th>No.</th>
			<th>No. Polis</th>
			<th>Object</th>
			<th>Start</th>
			<th>End</th>
			<th>TSI</th>
			<th>Rate</th>
			<th>Premi</th>
			<th>Diskon</th>
			<th>Admin Cost</th>
			<th>Total Premi</th>
		</tr>
	</thead>
	<tbody>

		<?php 
		$no=1; 
		$tsi = 0;
		$premi = 0;
		$asd_diskon = 0;
		$adm_broker = 0;
		$jumlah = 0;

		foreach ($q as $key => $value) { ?>

			<tr>
				<td><?=$no++;?></td>
				<td><?=$value['no_polis'];?></td>
				<td><?=$value['nama'];?></td>
				<td><?=date('d/m/Y',strtotime($value['tanggal_awal']));?></td>
				<td><?=date('d/m/Y',strtotime($value['tanggal_akhir']));?></td>
				<td><?=number_format($value['tsi']);?></td>
				<td><?=number_format($value['premi_rate']);?> %</td>
				<td><?=number_format($value['premi']);?></td>
				<td><?=number_format($value['asd_diskon']);?></td>
				<td><?=number_format($value['adm_broker']);?></td>
				<td><?=number_format($value['jumlah']);?></td>
			</tr>

		<?php 

		$tsi += $value['tsi'];
		$premi += $value['premi'];
		$asd_diskon += $value['asd_diskon'];
		$adm_broker += $value['adm_broker'];
		$jumlah += $value['jumlah'];

		} ?>

		<tr>
			<td style="text-align: center;" colspan="5"><strong>TOTAL</strong></td>
			<td><?=number_format($tsi);?></td>
			<td>&nbsp;</td>
			<td><?=number_format($premi);?></td>
			<td><?=number_format($asd_diskon);?></td>
			<td><?=number_format($adm_broker);?></td>
			<td><?=number_format($jumlah);?></td>
		</tr>
		
	</tbody>

</table>
</body>
</html>