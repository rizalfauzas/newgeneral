<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Installment</title>
	<style>
	table {
	  font-family: arial, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}

	td, th {
	  border: 1px solid #dddddd;
	  border-style: groove;
	  text-align: left;
	  font-size: 12px;
	  padding: 8px;
	}

	</style>
  
</head>
<body>
<h4>PT. Proteksi Jaya Mandiri</h4>
<strong>Gandaria 8 Office Lantai 12 Unit H, Jl. Sultan Iskandar Muda - Jakarta Selatan 12240</strong><br>
<strong>Telp : 021 - 2903557</strong>
<p></p>
<strong>Daftar Installment Ke Tertanggung</strong><br>
<p style="font-size: 10;"><i><?=date('d-m-Y H:i:s')?><br><?=$this->session->userdata('nama');?></i></p>
<p></p>

<table class="table">
    <thead class="thead-light">
		<tr>
			<th>Installment Ke-</th>
			<th>Tanggal Tempo</th>
			<th>Premikot</th>
			<th>Diskonrb</th>
			<th>Premi</th>
			<th>Diskon</th>
			<th>Brokerage</th>
			<th>Biaya Polis</th>
			<th>Biaya Materai</th>
			<th>Biaya Admin Broker</th>
			<th>Jumlah</th>
		</tr>
	</thead>
	<tbody>

		<?php 
		$no=1; 
		$gross_premi = 0;
		$diskon_asd = 0;
		$premi = 0;
		$brokerage = 0;
		$biaya_polis = 0;
		$biaya_materai = 0;
		$biaya_admin_broker = 0;
		$jumlah = 0;
		foreach ($q as $key => $value) { ?>
		<tr>
			<td><?=$no++;?></td>
			<td><?=date('d/m/Y',strtotime($value['tanggal_jatuh_tempo']));?></td>
			<td><?=number_format($value['gross_premi']);?></td>
			<td><?=number_format($value['diskon_asd']);?></td>
			<td><?=number_format($value['gross_premi'] - $value['diskon_asd']);?></td>
			<td><?=number_format($value['diskon_asd']);?></td>
			<td><?=number_format($value['brokerage']);?></td>
			<td><?=number_format($value['biaya_polis']);?></td>
			<td><?=number_format($value['biaya_materai']);?></td>
			<td><?=number_format($value['biaya_admin_broker']);?></td>
			<td><?=number_format($value['jumlah_setelah_admin_materai']);?></td>

		</tr>	

		<?php 

		$gross_premi += $value['gross_premi'];
		$diskon_asd += $value['diskon_asd'];
		$premi += $value['gross_premi'] - $value['diskon_asd'];
		$brokerage+= $value['brokerage'];
		$biaya_polis += $value['biaya_polis'];
		$biaya_materai += $value['biaya_materai'];
		$biaya_admin_broker += $value['biaya_admin_broker'];
		$jumlah += $value['jumlah_setelah_admin_materai'];

		} 

		?>	

		<tr>
			<td style="text-align: center;" colspan="2"><strong>TOTAL</strong></td>
			<td><?=number_format($gross_premi);?></td>
			<td><?=number_format($diskon_asd);?></td>
			<td><?=number_format($premi);?></td>
			<td><?=number_format($diskon_asd);?></td>
			<td><?=number_format($brokerage);?></td>
			<td><?=number_format($biaya_polis);?></td>
			<td><?=number_format($biaya_materai);?></td>
			<td><?=number_format($biaya_admin_broker);?></td>
			<td><?=number_format($jumlah);?></td>
		</tr>

	</tbody>

</table>
</body>
</html>