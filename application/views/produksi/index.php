<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title;?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=$title?></a></li>
                        <li class="breadcrumb-item active">index</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <ul class="nav nav-tabs nav-border-top nav-border-top-primary mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#data" role="tab" aria-selected="true">
                                <i class="ri-file-list-3-line"></i> Data Produksi
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-bs-toggle="tab" href="#add" role="tab" aria-selected="false">
                                <i class="ri-edit-line"></i> Tambah Produksi
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane p-3 active" id="data" role="tabpanel">
                            
                            <div class="accordion" id="accordionExample">
                                <div class="accordion-item border rounded">
                                  <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button fw-semibold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        FILTER DATA
                                    </button>
                                  </h2>
                                  <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample" style="">
                                    <div class="accordion-body">
                                        <form id="form-filter" method="post" action="<?=base_url('akad/excel')?>" target="_blank">

                                        <div class="row">
                                            <div class="col-6">
                                                <div class="mb-3">
                                                    <label for="firstNameinput" class="form-label"> Produksi (dari)</label>
                                                    <input type="date" class="form-control" name="dari" id="dari">
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="mb-3">
                                                    <label for="lastNameinput" class="form-label"> Produksi (sampai)</label>
                                                     <input type="date" class="form-control" name="sampai" id="sampai">
                                                </div>
                                            </div>

                                            
                                            <div class="col-6">
                                                <div class="mb-3">
                                                    <label for="firstNameinput" class="form-label">Jenis Asuransi</label>
                                                    <select class="form-select" name="jenis_asuransi_filter" id="jenis_asuransi_filter" >
                                                        <option></option>
                                                        <?php
                                                            $jenis_asuransi = $this->db->query("select * from jenis_asuransi")->result_array();
                                                            foreach ($jenis_asuransi as $value) {
                                                                  echo"<option value='".$value['id']."'>".$value['jenis_asuransi']."</option>"; 
                                                            }
                                                        ?> 
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-6">
                                                <div class="mb-3">
                                                    <label for="firstNameinput" class="form-label">Tertanggung</label>
                                                    <select class="form-select" name="tertanggung_filter" id="tertanggung_filter"  >
                                                        <option></option>
                                                        <?php
                                                            $client = $this->db->query("select * from client")->result_array();
                                                            foreach ($client as $value) {
                                                                  echo"<option value='".$value['id']."'>".$value['nama']."</option>"; 
                                                            }
                                                        ?>                                                                          
                                                       </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="hstack gap-3  justify-content-center">
                                            <button type="button" class="btn btn-primary" id="btn-filter" ><i class="ri-filter-line" ></i> Filter</button>
                                            <div class="vr"></div>
                                            
                                            <div class="vr" id="vr-generate" style="display: none"></div>
                                            <button type="reset" class="btn btn-warning" id="btn-reset"><i class="ri-refresh-line"></i> Refresh</button>
                                        </div>

                                        </form>
                                    </div>
                                  </div>
                                </div>
                            </div>

                            <!-- <p>&nbsp;</p> -->

                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-body">
                                        
                                        <div class="table-responsive">
                                            <table id="tablena" class="table table-striped mb-0" style="width:100%">

                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Aksi</th>
                                                        <th>No. Nota</th>
                                                        <th>Tanggal Nota</th>
                                                        <th>Jenis Asuransi</th>
                                                        <th>Tertanggung</th>
                                                        <th>TSI</th>
                                                        <th>Create By</th>
                                                        <th>Create Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>


                        <div class="tab-pane p-3" id="add" role="tabpanel">

                            <h5>Form Tambah Produksi</h5><br>

                             <form method="post" id="form_produksi" enctype="multipart/form-data">

                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Status <code>*</code></label>
                                            <select class="form-select" name="statusna" id="statusna">
                                                <option value="1">New</option>
                                                <option value="2">Renewal</option>
                                                <option value="3">Endorsment</option>
                                            </select>
                                            <div id="statusna-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Tanggal Nota <code>*</code></label>
                                            <input type="date" class="form-control" name="tgl_nota" id="tgl_nota"  >
                                            <div id="tgl_nota-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jenis Asuransi <code>*</code></label>
                                            <select class="form-select" name="jenis_asuransi" id="jenis_asuransi" >
                                                <option></option>
                                                <?php
                                                    $jenis_asuransi = $this->db->query("select * from jenis_asuransi")->result_array();
                                                    foreach ($jenis_asuransi as $value) {
                                                          echo"<option value='".$value['id']."'>".$value['jenis_asuransi']."</option>"; 
                                                    }
                                                ?> 
                                            </select>
                                            <div id="jenis_asuransi-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Tertanggung <code>*</code></label>
                                            <select class="form-select" name="tertanggung" id="tertanggung" >
                                                <option></option>
                                                <?php
                                                    $client = $this->db->query("select * from client")->result_array();
                                                    foreach ($client as $value) {
                                                          echo"<option value='".$value['id']."'>".$value['nama']."</option>"; 
                                                    }
                                                ?> 
                                            </select>
                                            <div id="tertanggung-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">TSI <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <select class="form-select" name="currency" id="currency">
                                                    <?php
                                                        $currency = $this->db->query("select * from currency")->result_array();
                                                        foreach ($currency as $value) {
                                                              echo"<option value='".$value['id']."'>".$value['currency']."</option>"; 
                                                        }
                                                    ?> 
                                                </select>

                                                <input type="text" class="form-control" id="tsi" name="tsi" value="" placeholder="TSI" readonly="">
                                                
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalTsi"><i class="  bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input TSI</button>
                                                </div>
                                            </div>
                                            <div id="currency-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                            <div id="tsi-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">No. Polis <code>*</code></label>
                                            <input type="text" class="form-control" name="no_polis" id="no_polis" placeholder="No. Polis" readonly>
                                            <div id="no_polis-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jangka Waktu Dari <code>*</code></label>
                                            <input type="date" class="form-control" name="jangka_waktu_dari" id="jangka_waktu_dari"  readonly>
                                            <div id="jangka_waktu_dari-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jangka Waktu Sampai <code>*</code></label>
                                            <input type="date" class="form-control" name="jangka_waktu_sampai" id="jangka_waktu_sampai"  readonly>
                                            <div id="jangka_waktu_sampai-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Gross Premi <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="gross_premi_rate" name="gross_premi_rate" value="" readonly="" placeholder="Rate">
                                                <input type="text" class="form-control" id="gross_premi" name="gross_premi" placeholder="Gross premi" readonly="">
                                            </div>
                                            <div id="gross_premi-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Diskon ASD <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="diskon_asd_persen" name="diskon_asd_persen" value="" readonly="" placeholder="Diskon %">
                                                <input type="text" class="form-control" id="diskon_asd" name="diskon_asd" placeholder="Diskon ASD" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Materai <code>*</code></label>
                                            <input type="text" class="form-control" name="materai" id="materai" placeholder="Materai" readonly>
                                            <div id="materai-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Biaya Polis <code>*</code></label>
                                            <input type="text" class="form-control" name="biaya_polis" id="biaya_polis" placeholder="Biaya Polis" readonly >
                                            <div id="biaya_polis-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Biaya Admin Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="biaya_admin_broker" id="biaya_admin_broker" placeholder="Biaya Admin Broker" readonly>
                                            <div id="biaya_admin_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jumlah Sebelum Admin Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="jumlah_sebelum_admin_broker" placeholder="Jumlah Sebelum Admin Broker" id="jumlah_sebelum_admin_broker" readonly >
                                            <div id="jumlah_sebelum_admin_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jumlah Setelah Admin Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="jumlah_setelah_admin_broker" id="jumlah_setelah_admin_broker" placeholder="Jumlah Setelah Admin Broker" readonly>
                                            <div id="jumlah_setelah_admin_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Premi After Diskon <code>*</code></label>
                                            <input type="text" class="form-control" name="premi_after_diskon" id="premi_after_diskon" placeholder="Premi After Diskon" readonly >
                                            <div id="premi_after_diskon-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Rincian Asuransi <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="asuransi_leader" name="asuransi_leader" value="" placeholder="Rincian Asuransi" readonly="">
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalRincianAsuransi"><i class="  bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input Asuransi</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Asuransi Leader 100% <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="asuransi_leader_id" name="asuransi_leader_id" value="" readonly="" placeholder="" style="display: none;">
                                                <input type="text" class="form-control" id="share_leader" name="share_leader" value="" readonly="" placeholder="Share %">
                                                <!-- <input type="text" class="form-control" name="asuransi_leader" id="asuransi_leader" placeholder="Asuransi Leader" readonly> -->
                                            </div>
                                            <div id="asuransi_leader-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Gross Broker <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="gross_broker_rate" name="gross_broker_rate" placeholder="Gross Broker Rate %" value="" readonly="" placeholder="Diskon %">
                                                <input type="text" class="form-control" id="gross_broker" name="gross_broker" placeholder="Gross Broker" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Diskon Broker <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="diskon_broker_persen" name="diskon_broker_persen" value="" readonly="" placeholder="Diskon Broker %">
                                                <input type="text" class="form-control" id="diskon_broker" name="diskon_broker" placeholder="Diskon Broker" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Premi After Diskon Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="premi_after_diskon_broker" id="premi_after_diskon_broker" placeholder="Premi After Diskon" readonly>
                                            <div id="premi_after_diskon_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Net Brokerage <code>*</code></label>
                                            <input type="text" class="form-control" name="net_brokerage" id="net_brokerage" placeholder="Net Brokerage" readonly >
                                            <div id="net_brokerage-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Premi To Ceding <code>*</code></label>
                                            <input type="text" class="form-control" name="premi_to_ceding" id="premi_to_ceding" placeholder="Premi After Ceding" readonly>
                                            <div id="premi_to_ceding-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Installment <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="installment" name="installment" value="" placeholder="Installmment" readonly="">
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalInstallment"><i class=" bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input Installment</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Rate PPN <code>*</code></label>
                                            <input type="text" class="form-control" name="rate_ppn" id="rate_ppn" placeholder="Rate PPN" readonly>
                                            <div id="rate_ppn-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Tujuan Penerimaan Transper <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="tujuan_transper" name="tujuan_transper" value="" placeholder="Tujuan Transper" readonly="">
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalTransper"><i class=" bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input Tujuan Transper</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Unit Bisnis <code>*</code></label>
                                            <select class="form-select" name="unit_bisnis" id="unit_bisnis">
                                                <option></option>
                                                <?php
                                                    $unitbisnis = $this->db->query("select * from unit_bisnis")->result_array();
                                                    foreach ($unitbisnis as $value) {
                                                          echo"<option value='".$value['id']."'>".$value['unit_bisnis']."</option>"; 
                                                    }
                                                ?> 
                                            </select>
                                            <div id="unit_bisnis-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="json_data" id="json_data" placeholder="" value="" style="display: none;">
                                        <input type="text" class="form-control" name="json_data_asd" id="json_data_asd" placeholder="" value="" style="display: none;">
                                     
                                        <div class="text-center">
                                            <button type="button" onclick="save()" class="btn btn-primary btn-label waves-effect waves-light"><i class="bx bx-save label-icon align-middle fs-16 me-2"></i>Simpan</button>
                                        </div>
                                        
                                    </div>

                                   
                                </div>

                                <div class="modal fade" id="modalTsi" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Input TSI
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="tableTsi" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>Aksi</th>
                                                        <th>No. Polis <code>*</code></th>
                                                        <th>Keterangan <code>*</code></th>
                                                        <th>Awal <code>*</code></th>
                                                        <th>Akhir <code>*</code></th>
                                                        <th>TSI <code>*</code></th>
                                                        <th>Rate Premi <code>*</code></th>
                                                        <th>Premi Gross <code>*</code></th>
                                                        <th>Diskon ASD %</th>
                                                        <th>Diskon ASD</th>
                                                        <th>Premi <code>*</code></th>
                                                        <th>Diskon Broker %</th>
                                                        <th>Diskon Broker</th>
                                                        <th>Biaya Polis <code>*</code></th>
                                                        <th>Biaya Materai <code>*</code></th>
                                                        <th>Biaya Adm Bro <code>*</code></th>
                                                        <th>Jumlah <code>*</code></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <button type="button" class="btn btn-primary btn-sm" onclick="addRowTsi()"><i class="las la-plus"></i></button>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="no_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="no_polis_modal_tsi">
                                                                <br>
                                                                <div id="no_polis_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="keterangan_modal_tsi[]" class="form-control" style="width: 300px;" id="keterangan_modal_tsi">
                                                                <br>
                                                                <div id="keterangan_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="date" name="tanggal_awal_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_awal_modal_tsi" >
                                                                <br>
                                                                <div id="tanggal_awal_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="date" name="tanggal_akhir_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_akhir_modal_tsi">
                                                                <br>
                                                                <div id="tanggal_akhir_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="tsi_modal_tsi[]" class="form-control" style="width: 300px;" id="tsi_modal_tsi">
                                                                <br>
                                                                <div id="tsi_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="rate_premi_modal_tsi[]" class="form-control" style="width: 300px;" id="rate_premi_modal_tsi">
                                                                <br>
                                                                <div id="rate_premi_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>

                                                            <td>
                                                                <input type="text" name="premi_gross_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_gross_modal_tsi">
                                                                <br>
                                                                <div id="premi_gross_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_asd_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_persen_modal_tsi">
                                                                <br>
                                                                <div id="diskon_asd_persen_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_asd_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_modal_tsi">
                                                                <br>
                                                                <div id="diskon_asd_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="premi_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_modal_tsi">
                                                                <br>
                                                                <div id="premi_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_broker_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_persen_modal_tsi">
                                                                
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_broker_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_modal_tsi">
                                                                
                                                            </td>
                                                            <td>
                                                                <input type="text" name="biaya_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_polis_modal_tsi">
                                                                <br>
                                                                <div id="biaya_polis_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="biaya_materai_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_materai_modal_tsi">
                                                                <br>
                                                                <div id="biaya_materai_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="biaya_adm_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_adm_modal_tsi" value="50.000">
                                                                <br>
                                                                <div id="biaya_adm_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            <td>
                                                                <input type="text" name="jumlah_modal_tsi[]" class="form-control" style="width: 300px;" id="jumlah_modal_tsi">
                                                                <br>
                                                                <div id="jumlah_modal_tsi-err" style="display:none"><i style="color: red;">* Wajib Disii</i></div>
                                                            </td>
                                                            
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                <button type="button" id="buttonTsi" class="btn btn-primary" onclick="saveTsi()">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                                <div class="modal fade" id="modalRincianAsuransi" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Rincian Asuransi
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="tabelRincianAsuransi" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>Aksi</th>
                                                        <th>Nama Asuradur</th>
                                                        <th>JN+Pajak</th>
                                                        <th>Share</th>
                                                        <th>TSI</th>
                                                        <th>Gross Premi</th>
                                                        <th>Diskon</th>
                                                        <th>Premi</th>
                                                        <th>Broker %</th>
                                                        <th>Gross Brokerage</th>
                                                        <th>Hut Premi</th>
                                                        <th>Biaya Polis</th>
                                                        <th>Biaya Materai</th>
                                                        <th>Jumlah Sebelum Pajak</th>
                                                        <th>PPN Asli</th>
                                                        <th>PPN</th>
                                                        <th>PPH23</th>
                                                        <th>Net Premi</th>
                                                        <th>DPP</th>
                                                        <th>Nilai Klaim</th>
                                                        <th>Keterangan Klaim</th>
                                                        
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <button type="button" class="btn btn-primary btn-sm" onclick="addRowRincianAsuransi()"><i class="las la-plus"></i></button>
                                                            </td>
                                                            <td>
                                                                <select class="form-select" name="nama_asuransi_modal_asuransi[]" id="nama_asuransi_modal_asuransi"  style="width: 300px;">
                                                                    <option></option>
                                                                    <option value="1">TAKAFUL UMUM</option>
                                                                    <option value="2">BRINS</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select class="form-select" name="jn_pajak_modal_asuransi[]" id="jn_pajak_modal_asuransi"  style="width: 300px;">
                                                                    <option></option>
                                                                    <option value="1">INCLUDE</option>
                                                                    <option value="2">EXCLUDE</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="share_modal_asuransi" name="share_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="tsi_modal_asuransi" name="tsi_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="gross_premi_modal_asuransi" name="gross_premi_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="diskon_modal_asuransi" name="diskon_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="premi_modal_asuransi" name="premi_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="fee_broker_modal_asuransi" name="fee_broker_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="gross_brokerage_modal_asuransi" name="gross_brokerage_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="hut_premi_modal_asuransi" name="hut_premi_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="biaya_polis_modal_asuransi" name="biaya_polis_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="biaya_materai_modal_asuransi" name="biaya_materai_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="jumlah_sebelum_pajak_modal_asuransi" name="jumlah_sebelum_pajak_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="ppn_asli_modal_asuransi" name="ppn_asli_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="ppn_modal_asuransi" name="ppn_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="pph_23_modal_asuransi" name="pph_23_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="net_premi_modal_asuransi" name="net_premi_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="dpp_modal_asuransi" name="dpp_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="nilai_klaim_modal_asuransi" name="nilai_klaim_modal_asuransi[]">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="keterangan_klaim_modal_asuransi" name="keterangan_klaim_modal_asuransi[]">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                <button type="button" class="btn btn-primary" onclick="saveAsuransi()">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                                <div class="modal fade" id="modalInstallment" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Installment
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                            <!-- <button type="button" class="btn btn-primary btn-sm" onclick="addRowInstalment()">Tambah Installment <i class="las la-plus"></i></button> -->

                                            <button type="button" class="btn btn-primary btn-label waves-effect waves-light" onclick="addRowInstalment()"><i class=" las la-plus-circle label-icon align-middle fs-16 me-2"></i> Tambah Installment</button>

                                                <table id="tableInstallment" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>Aksi</th>
                                                        <th>Installment</th>
                                                        <th>Tgl. Kwitansi</th>
                                                        <th>Tgl. Jatuh Tempo</th>
                                                        <th>Gross Premi</th>
                                                        <th>Diskon Asd</th>
                                                        <th>Diskon Broker</th>
                                                        <th>Biaya Polis</th>
                                                        <th>Biaya Materai</th>
                                                        <th>Biaya Admin Broker</th>
                                                        <th>Jumlah Setelah Biaya Polis dan Materai + Admin</th>
                                                        <th>Brokerage</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td colspan='12'>belum ada instalment</td>
                                                        </tr>
                                                    </tbody>
                                                </table>

                                                <table id="tableInstallment2" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>Installment</th>
                                                        <th>ASD</th>
                                                        <th>ASD Nama</th>
                                                        <th>Tanggal Tempo</th>
                                                        <th>Gross Premi</th>
                                                        <th>Diskon ASD</th>
                                                        <th>Premi</th>
                                                        <th>Brokerage</th>
                                                        <th>Biaya Polis</th>
                                                        <th>Biaya Materai</th>
                                                        <th>Tax Index</th>
                                                        <th>Dpp</th>
                                                        <th>PPN</th>
                                                        <th>PPH 23</th>
                                                        <th>Iuran OJK</th>
                                                        <th>Jumlah</th>
                                                        
                                                        
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                       
                                                    </tbody>
                                                </table>


                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                <button type="button" class="btn btn-primary" onclick="saveInstallment()">Hitung Instalment</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                                <div class="modal fade" id="modalTransper" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Input Tujuan Transfer
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="tableBank" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap; width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Aksi</th>
                                                        <th>Bank</th>
                                                        <th>No. Rekening</th>
                                                        <th>Atas Nama</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 

                                                        $jh = $this->db->query("select * from rekening")->result_array();

                                                        foreach ($jh as $key => $value) { ?>
                                                        <tr>

                                                            <td>
                                                                <input type="checkbox" name="id_bank[]" id="id_bank" value="<?=$value['id'];?>">
                                                            </td>

                                                            <td>
                                                                <input class="form-control" type="text" name="modal_namaBank" id="modal_namaBank" value="<?=$value['namaBank'];?>" style="width: 300px;" readonly>
                                                            </td>

                                                            <td>
                                                                <input class="form-control" type="text" name="modal_namaBank" id="modal_namaBank" value="<?=$value['noRekening'];?>" style="width: 300px;" readonly>
                                                            </td>

                                                            <td>
                                                                <input class="form-control" type="text" name="modal_namaBank" id="modal_namaBank" value="<?=$value['atasNama'];?>" style="width: 300px;" readonly>
                                                            </td>

                                                        </tr>

                                                    <?php } ?>
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                                <button type="button" id="" class="btn btn-primary" data-bs-dismiss="modal">Simpan</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>



</div> <!-- container-fluid -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script> -->


<script type="text/javascript">

    $(document).ready( function(){
        // statusKtp();
        $("#tablena").DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": '<?=base_url('produksi/getData')?>',
                "type": "POST",
                "data": function ( data ) {
                    data.dari                = $('#dari').val();
                    data.sampai                 = $('#sampai').val();
                    data.jenis_asuransi_filter                 = $('#jenis_asuransi_filter').val();
                    data.tertanggung_filter                 = $('#tertanggung_filter').val();
                }
            },
            "columnDefs": [{
                "target": [-1],
                "orderable": false
            }],
            "scrollX": true
        });

        $('#btn-filter').click(function(){ 
            $('#btn-generate').show();
            $('#vr-generate').show();
            reloadTable();
        });
        $('#btn-reset').click(function(){ 
            $('#form-filter')[0].reset();
            reloadTable();
        });

    });

    // function formatRupiah(angka, prefix)
    // {
    //     var number_string = angka.replace(/[^,\d]/g, '').toString(),
    //         split    = number_string.split('.'),
    //         sisa     = split[0].length % 3,
    //         rupiah     = split[0].substr(0, sisa),
    //         ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
    //     if (ribuan) {
    //         separator = sisa ? '.' : '';
    //         rupiah += separator + ribuan.join('.');
    //     }
    //     console.log(rupiah);
    //     console.log(split[1]);

    //     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    //     return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    // }

    // Modal TSI Function //
    $('body').on('keyup', '#tableTsi #rate_premi_modal_tsi', function () {
        var cIndex = $(this).index();
        var tr = $(this).closest('tr');
        rowIndex = tr.index();
        var tsi = tr.find('input#tsi_modal_tsi').val().replace(/\./g,"");
        var rate = $(this).val();
        var premi = 0;
        if(rate){
            premi = (parseFloat(rate) * parseFloat(tsi)) / 100;
        }
        tr.find('input#premi_gross_modal_tsi').val(formatRupiah(premi.toFixed(2).toString().replace('.',','),'Rp. '));
    });

    $('body').on('keyup', '#tableTsi #diskon_asd_persen_modal_tsi', function () {
        var cIndex = $(this).index();
        var tr = $(this).closest('tr');
        rowIndex = tr.index();
        var premi = tr.find('input#premi_gross_modal_tsi').val().replace(/\./g,"");
        var rateasd = $(this).val();
        var discAsd = 0;
        var preminet = parseFloat(premi.replace(',','.'));
        // console.log(rateasd);
        // console.log(premi.replace(',','.'));
        if(rateasd > 0){
            // console.log('masuk');
            discAsd = (parseFloat(rateasd) * parseFloat(premi)) / 100;
            preminet = parseFloat(premi) - parseFloat(discAsd);
        }
        //  console.log(preminet.toFixed(2));
        tr.find('input#diskon_asd_modal_tsi').val(formatRupiah(discAsd.toString().replace('.',','),'Rp. '));
        tr.find('input#premi_modal_tsi').val(formatRupiah(preminet.toFixed(2).toString().replace('.',','),'Rp. '));
    });

    $('body').on('keyup', '#tableTsi #biaya_materai_modal_tsi', function () {
        $(this).val(formatRupiah($(this).val(),'Rp. '));

        var cIndex = $(this).index();
        var tr = $(this).closest('tr');
        rowIndex = tr.index();

        var premi = tr.find('input#premi_modal_tsi').val().replace(/\./g,"");
        var materai = $(this).val().replace(/\./g,"");
        var polis = tr.find('input#biaya_polis_modal_tsi').val().replace(/\./g,"");
        var admbro = tr.find('input#biaya_adm_modal_tsi').val().replace(/\./g,"");
        var totalpremi = 0;
        if(materai){
            premi = premi.replace(',','.');
            totalpremi = parseFloat(premi) + parseFloat(materai) + parseFloat(polis) + parseFloat(admbro);
        }
        // console.log(totalpremi);
        tr.find('input#jumlah_modal_tsi').val(formatRupiah(totalpremi.toFixed(2).toString().replace('.',','),'Rp. '));
    });


    $('body').on('keyup', '#tableTsi #tsi_modal_tsi', function () {
        $(this).val(formatRupiah($(this).val(),'Rp. '));
    });

    $('body').on('keyup', '#tableTsi #biaya_polis_modal_tsi', function () {
        $(this).val(formatRupiah($(this).val(),'Rp. '));
    });

    function saveTsi() 
    {

        let err = 0;

        var no_polis_modal_tsi = $("#no_polis_modal_tsi").val();
        if (no_polis_modal_tsi == '') {
            $("#no_polis_modal_tsi-err").show();
            err = 1;
        } else{
            $('#no_polis_modal_tsi-err').hide();
        }

        var keterangan_modal_tsi = $("#keterangan_modal_tsi").val();
        if (keterangan_modal_tsi == '') {
            $("#keterangan_modal_tsi-err").show();
            err = 1;
        } else{
            $('#keterangan_modal_tsi-err').hide();
        }

         var tanggal_awal_modal_tsi = $("#tanggal_awal_modal_tsi").val();
        if (tanggal_awal_modal_tsi == '') {
            $("#tanggal_awal_modal_tsi-err").show();
            err = 1;
        } else{
            $('#tanggal_awal_modal_tsi-err').hide();
        }

         var tanggal_akhir_modal_tsi = $("#v").val();
        if (tanggal_akhir_modal_tsi == '') {
            $("#tanggal_akhir_modal_tsi-err").show();
            err = 1;
        } else{
            $('#tanggal_akhir_modal_tsi-err').hide();
        }

         var tsi_modal_tsi = $("#tsi_modal_tsi").val();
        if (tsi_modal_tsi == '') {
            $("#tsi_modal_tsi-err").show();
            err = 1;
        } else{
            $('#tsi_modal_tsi-err').hide();
        }

         var rate_premi_modal_tsi = $("#rate_premi_modal_tsi").val();
        if (rate_premi_modal_tsi == '') {
            $("#rate_premi_modal_tsi-err").show();
            err = 1;
        } else{
            $('#rate_premi_modal_tsi-err').hide();
        }

         var premi_gross_modal_tsi = $("#premi_gross_modal_tsi").val();
        if (premi_gross_modal_tsi == '') {
            $("#premi_gross_modal_tsi-err").show();
            err = 1;
        } else{
            $('#premi_gross_modal_tsi-err').hide();
        }

         var diskon_asd_persen_modal_tsi = $("#diskon_asd_persen_modal_tsi").val();
        if (diskon_asd_persen_modal_tsi == '') {
            $("#diskon_asd_persen_modal_tsi-err").show();
            err = 1;
        } else{
            $('#diskon_asd_persen_modal_tsi-err').hide();
        }

         var diskon_asd_modal_tsi = $("#diskon_asd_modal_tsi").val();
        if (diskon_asd_modal_tsi == '') {
            $("#diskon_asd_modal_tsi-err").show();
            err = 1;
        } else{
            $('#diskon_asd_modal_tsi-err').hide();
        }

         var premi_modal_tsi = $("#premi_modal_tsi").val();
        if (premi_modal_tsi == '') {
            $("#premi_modal_tsi-err").show();
            err = 1;
        } else{
            $('#premi_modal_tsi-err').hide();
        }

         var biaya_polis_modal_tsi = $("#biaya_polis_modal_tsi").val();
        if (biaya_polis_modal_tsi == '') {
            $("#biaya_polis_modal_tsi-err").show();
            err = 1;
        } else{
            $('#biaya_polis_modal_tsi-err').hide();
        }

         var biaya_materai_modal_tsi = $("#biaya_materai_modal_tsi").val();
        if (biaya_materai_modal_tsi == '') {
            $("#biaya_materai_modal_tsi-err").show();
            err = 1;
        } else{
            $('#v-err').hide();
        }

         var biaya_adm_modal_tsi = $("#biaya_adm_modal_tsi").val();
        if (biaya_adm_modal_tsi == '') {
            $("#biaya_adm_modal_tsi-err").show();
            err = 1;
        } else{
            $('#biaya_adm_modal_tsi-err').hide();
        }

         var jumlah_modal_tsi = $("#jumlah_modal_tsi").val();
        if (jumlah_modal_tsi == '') {
            $("#jumlah_modal_tsi-err").show();
            err = 1;
        } else{
            $('#jumlah_modal_tsi-err').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        let arrTsi = {
            data : [],
            totalData : {
                sum_tsi : 0,
                sum_premi_gross : 0,
                sum_diskon_asd : 0,
                sum_premi : 0,
                sum_diskon_broker : 0,
                sum_biaya_polis : 0,
                sum_biaya_materai : 0,
                sum_biaya_adm : 0,
                sum_total_tsi : 0,
                sum_rate_premi : 0,
                sum_disc_bro : 0,
                sum_disc_asd : 0,
                count_tsi : 0
            }
        };
        let sum_tsi = 0;
        let sum_premi_gross = 0;
        let sum_diskon_asd = 0;
        let sum_premi = 0;
        let sum_diskon_broker = 0;
        let sum_biaya_polis = 0;
        let sum_biaya_materai = 0;
        let sum_biaya_adm = 0;
        let sum_total_tsi = 0;
        let sum_rate_premi = 0;
        let sum_disc_bro = 0;
        let sum_disc_asd = 0;
        let no_polis = '';
        let tanggal_awal = '';
        let tanggal_akhir = '';
        let total_count = 0;

        $('#tableTsi > tbody  > tr').each(function(index, tr) { 
            // console.log(index);
            // console.log(tr);
            var item = $(this, tr);
            var arrItem = {
                tsi : 0,
                premi_gross : 0,
                diskon_asd : 0,
                premi : 0,
                diskon_broker : 0,
                biaya_polis : 0,
                biaya_materai : 0,
                biaya_adm : 0,
                rate_premi : 0,
                disc_bro : 0,
                disc_asd : 0,
                total_tsi : 0
            };

            if(index == 0){
                no_polis = item.find("input#no_polis_modal_tsi", tr).val();
                tanggal_awal = item.find("input#tanggal_awal_modal_tsi", tr).val();
                tanggal_akhir = item.find("input#tanggal_akhir_modal_tsi", tr).val();
            }

            var tsi = item.find("input#tsi_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(tsi);
            var premi_gross = item.find("input#premi_gross_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(premi_gross);
            var diskon_asd = item.find("input#diskon_asd_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(diskon_asd);
            var premi = item.find("input#premi_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(premi);
            var diskon_broker = item.find("input#diskon_broker_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(diskon_broker);
            var biaya_polis = item.find("input#biaya_polis_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(biaya_polis);
            var biaya_materai = item.find("input#biaya_materai_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(biaya_materai);
            var biaya_adm = item.find("input#biaya_adm_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(biaya_adm);
            var total_tsi = item.find("input#jumlah_modal_tsi", tr).val().replace(/\./g,"").replace(',','.');
            console.log(total_tsi);
            var rate_premi = item.find("input#rate_premi_modal_tsi", tr).val();
            var disc_bro = item.find("input#diskon_broker_persen_modal_tsi", tr).val();
            var disc_asd = item.find("input#diskon_asd_persen_modal_tsi", tr).val();
            // console.log(total_tsi);
            // console.log(rate_premi);

            sum_tsi = parseFloat(tsi) + sum_tsi;
            sum_premi_gross = parseFloat(premi_gross) + sum_premi_gross;
            sum_diskon_asd = parseFloat(diskon_asd) + sum_diskon_asd;
            sum_premi = parseFloat(premi) + sum_premi;
            sum_diskon_broker = parseFloat(diskon_broker) + sum_diskon_broker;
            sum_biaya_polis = parseFloat(biaya_polis) + sum_biaya_polis;
            sum_biaya_materai = parseFloat(biaya_materai) + sum_biaya_materai;
            sum_biaya_adm = parseFloat(biaya_adm) + sum_biaya_adm;
            sum_total_tsi = parseFloat(total_tsi) + sum_total_tsi;
            sum_rate_premi = parseFloat(rate_premi) + sum_rate_premi;
            sum_disc_bro = parseFloat(disc_bro) + sum_disc_bro;
            sum_disc_asd = parseFloat(disc_asd) + sum_disc_asd;

            arrItem.tsi = parseFloat(tsi);
            arrItem.premi_gross = parseFloat(premi_gross);
            arrItem.diskon_asd = parseFloat(diskon_asd);
            arrItem.premi = parseFloat(premi);
            arrItem.diskon_broker = parseFloat(diskon_broker);
            arrItem.biaya_polis = parseFloat(biaya_polis);
            arrItem.biaya_materai = parseFloat(biaya_materai);
            arrItem.biaya_adm = parseFloat(biaya_adm);
            arrItem.total_tsi = parseFloat(total_tsi);
            arrItem.rate_premi = parseFloat(rate_premi);
            arrItem.disc_bro = parseFloat(disc_bro);
            arrItem.disc_asd = parseFloat(disc_asd);

            arrTsi.data.push(arrItem);
            total_count = total_count + 1;
        });

        arrTsi.totalData.sum_tsi = sum_tsi;
        arrTsi.totalData.sum_premi_gross = sum_premi_gross;
        arrTsi.totalData.sum_diskon_asd = sum_diskon_asd;
        arrTsi.totalData.sum_premi = sum_premi;
        arrTsi.totalData.sum_diskon_broker = sum_diskon_broker;
        arrTsi.totalData.sum_biaya_polis = sum_biaya_polis;
        arrTsi.totalData.sum_biaya_materai = sum_biaya_materai;
        arrTsi.totalData.sum_biaya_adm = sum_biaya_adm;
        arrTsi.totalData.sum_total_tsi = sum_total_tsi;
        arrTsi.totalData.sum_rate_premi = sum_rate_premi;
        arrTsi.totalData.sum_disc_bro = sum_disc_bro;
        arrTsi.totalData.sum_disc_asd = sum_disc_asd;
        arrTsi.totalData.count_tsi = total_count;

        // console.log(arrTsi);

        var premi_before_adm = arrTsi.totalData.sum_premi + arrTsi.totalData.sum_biaya_polis + arrTsi.totalData.sum_biaya_materai;
        var premi_adm = arrTsi.totalData.sum_premi + arrTsi.totalData.sum_biaya_polis + arrTsi.totalData.sum_biaya_materai + arrTsi.totalData.sum_biaya_adm;
        var premi_disc_bro = arrTsi.totalData.sum_premi + arrTsi.totalData.sum_biaya_polis + arrTsi.totalData.sum_biaya_materai + arrTsi.totalData.sum_disc_bro;

        var json = JSON.stringify(arrTsi);
        // console.log(json);
        $('#no_polis').val(no_polis);
        $('#json_data').val(json);
        $('#jangka_waktu_dari').val(tanggal_awal);
        $('#jangka_waktu_sampai').val(tanggal_akhir);
        $('#gross_premi_rate').val(arrTsi.data[0].rate_premi + ' %');
        $('#diskon_asd_persen').val(arrTsi.data[0].disc_asd + ' %');
        $('#diskon_broker_persen').val(arrTsi.data[0].disc_bro + ' %');
        $('#diskon_broker').val(formatRupiah(arrTsi.totalData.sum_disc_bro.toString().replace('.',','),'Rp. '));
        $('#gross_premi').val(formatRupiah(arrTsi.totalData.sum_premi_gross.toString().replace('.',','),'Rp. '));
        $('#tsi').val(formatRupiah(arrTsi.totalData.sum_tsi.toString().replace('.',','),'Rp. '));
        $('#diskon_asd').val(formatRupiah(arrTsi.totalData.sum_diskon_asd.toString().replace('.',','),'Rp. '));
        $('#materai').val(formatRupiah(arrTsi.totalData.sum_biaya_materai.toString().replace('.',','),'Rp. '));
        $('#biaya_polis').val(formatRupiah(arrTsi.totalData.sum_biaya_polis.toString().replace('.',','),'Rp. '));
        $('#biaya_admin_broker').val(formatRupiah('50000','Rp. '));
        $('#premi_after_diskon').val(formatRupiah(arrTsi.totalData.sum_total_tsi.toString().replace('.',','),'Rp. '));
        $('#jumlah_sebelum_admin_broker').val(formatRupiah(premi_before_adm.toString().replace('.',','),'Rp. '));
        $('#jumlah_setelah_admin_broker').val(formatRupiah(premi_adm.toString().replace('.',','),'Rp. '));
        $('#premi_after_diskon_broker').val(formatRupiah(premi_disc_bro.toString().replace('.',','),'Rp. '));

        $('#modalTsi').modal('hide');
    }
    // END Modal TSI Function //

    //Modal Asuransi Function //

    $('body').on('keyup', '#tabelRincianAsuransi #share_modal_asuransi', function () {
        var cIndex = $(this).index();
        var tr = $(this).closest('tr');
        rowIndex = tr.index();
        var tsi = $('#tsi').val().replace(/\./g,"").replace(',','.');
        var premi = $('#gross_premi').val().replace(/\./g,"").replace(',','.');
        var discBro = $('#diskon_broker').val().replace(/\./g,"").replace(',','.');
        var discIns = $('#diskon_asd').val().replace(/\./g,"").replace(',','.');
        var rate = $(this).val();
        var tsiAsuransi = 0;
        var premiAsuransi = 0;
        var discount = 0;
        var discountShare = 0;
        var premiNett = 0;
        if(rate){
            tsiAsuransi = (parseFloat(rate) * parseFloat(tsi)) / 100;
            premiAsuransi = (parseFloat(rate) * parseFloat(premi)) / 100;
            discount = (parseFloat(discBro) + parseFloat(discIns));
            premiNett = premiAsuransi - discount;
            if(discount > 0){
                discountShare = (parseFloat(rate) * discount) / 100;
            }

        }

        if(rowIndex == 0){
          var by_materai =  $('#materai').val();
          var by_polis = $('#biaya_polis').val();
          var by_adm_broker = $('#biaya_admin_broker').val();

          tr.find('input#biaya_polis_modal_asuransi').val(by_polis);
          tr.find('input#biaya_materai_modal_asuransi').val(by_materai);
        }else{
          tr.find('input#biaya_polis_modal_asuransi').val('0');
          tr.find('input#biaya_materai_modal_asuransi').val('0');
        }

        tr.find('input#tsi_modal_asuransi').val(formatRupiah(tsiAsuransi.toFixed(2).toString().replace('.',','),'Rp. '));
        tr.find('input#gross_premi_modal_asuransi').val(formatRupiah(premiAsuransi.toFixed(2).toString().replace('.',','),'Rp. '));
        tr.find('input#diskon_modal_asuransi').val(formatRupiah(discountShare.toFixed(2).toString().replace('.',','),'Rp. '));
        tr.find('input#premi_modal_asuransi').val(formatRupiah(premiNett.toFixed(2).toString().replace('.',','),'Rp. '));
    });

    $('body').on('keyup', '#tabelRincianAsuransi #fee_broker_modal_asuransi', function () {
        var cIndex = $(this).index();
        var tr = $(this).closest('tr');
        rowIndex = tr.index();
        var premi = tr.find('input#premi_modal_asuransi').val().replace(/\./g,"").replace(',','.');
        var polis = tr.find('input#biaya_polis_modal_asuransi').val().replace(/\./g,"").replace(',','.');
        var materai = tr.find('input#biaya_materai_modal_asuransi').val().replace(/\./g,"").replace(',','.');
        var rate = $(this).val();
        var brokerage = 0;
        var hutpremi = 0;
        var premi_sebelum_pajak = 0;
        var ppn = 0;
        var pph = 0;
        var dpp = 0;
        var netPremi = 0;
        var jn = tr.find("#jn_pajak_modal_asuransi", tr).val();
        if(rate){
            brokerage = (parseFloat(rate) * parseFloat(premi)) / 100;
            hutpremi = parseFloat(premi) - parseFloat(brokerage);
            premi_sebelum_pajak = hutpremi + parseFloat(polis) + parseFloat(materai);
            ppn = Math.round((premi_sebelum_pajak * 2.2) / 100);
            pph = Math.round((premi_sebelum_pajak * 2) / 100);
            dpp = Math.round((premi_sebelum_pajak * 2) / 100);
            netPremi = premi_sebelum_pajak + ppn + pph;

            // console.log(ppn);
            // console.log(pph);
        }
        if(jn == 1){
            tr.find('input#ppn_modal_asuransi').val(formatRupiah(ppn.toString(),'Rp. '));
        }

        tr.find('input#dpp_modal_asuransi').val(formatRupiah(dpp.toString(),'Rp. '));
        tr.find('input#net_premi_modal_asuransi').val(formatRupiah(netPremi.toFixed(2).toString().replace('.',','),'Rp. '));
        tr.find('input#ppn_asli_modal_asuransi').val(formatRupiah(ppn.toString(),'Rp. '));
        tr.find('input#pph_23_modal_asuransi').val(formatRupiah(pph.toString(),'Rp. '));
        tr.find('input#gross_brokerage_modal_asuransi').val(formatRupiah(brokerage.toFixed(2).toString().replace('.',','),'Rp. '));
        tr.find('input#hut_premi_modal_asuransi').val(formatRupiah(hutpremi.toFixed(2).toString().replace('.',','),'Rp. '));
        tr.find('input#jumlah_sebelum_pajak_modal_asuransi').val(formatRupiah(premi_sebelum_pajak.toFixed(2).toString().replace('.',','),'Rp. '));
    });

    function saveAsuransi()
    {
        let arrAsd = {
            data : [],
            totalData : {
                sum_tsi : 0,
                sum_share : 0,
                sum_premi_gross : 0,
                sum_diskon : 0,
                sum_premi : 0,
                sum_rate_broker : 0,
                sum_gross_brokerage : 0,
                sum_hut_premi : 0,
                sum_biaya_materai : 0,
                sum_biaya_polis : 0,
                sum_sebelum_pajak : 0,
                sum_ppn_asli : 0,
                sum_ppn : 0,
                sum_pph : 0,
                sum_net_premi : 0,
                sum_pajak : 0,
                sum_dpp : 0,
                sum_nilai_klaim : 0,
                count_asd : 0
            }
        };
        let sum_tsi = 0;
        let sum_share = 0;
        let sum_premi_gross = 0;
        let sum_diskon = 0;
        let sum_premi = 0;
        let sum_rate_broker = 0;
        let sum_gross_brokerage = 0;
        let sum_hut_premi = 0;
        let sum_biaya_materai = 0;
        let sum_biaya_polis = 0;
        let sum_sebelum_pajak = 0;
        let sum_ppn_asli = 0;
        let sum_ppn = 0;
        let sum_pph = 0;
        let sum_net_premi = 0;
        let sum_pajak = 0;
        let sum_dpp = 0;
        let sum_nilai_klaim = 0;
        let tanggal_awal = '';
        let tanggal_akhir = '';
        let total_count = 0;
        let rate_ppn = 0;
        let asuransi_leader = '';

        $('#tabelRincianAsuransi > tbody  > tr').each(function(index, tr) { 
            // console.log(index);
            // console.log(tr);
            var item = $(this, tr);
            var arrItem = {
                asuransi : '',
                id_asuransi : '',
                tsi : 0,
                share : 0,
                premi_gross : 0,
                diskon : 0,
                premi : 0,
                rate_broker : 0,
                gross_broker : 0,
                biaya_materai : 0,
                biaya_polis : 0,
                sebelum_pajak : 0,
                ppn_asli : 0,
                ppn : 0,
                pph : 0,
                net_premi : 0,
                hut_premi : 0,
                pajak : 0,
                dpp : 0,
                nilai_klaim : 0,
                keterangan : ''
            };

            if(index == 0){
                asuransi_leader = item.find("#nama_asuransi_modal_asuransi :selected", tr).text();
                asuransi_id = item.find("#nama_asuransi_modal_asuransi", tr).val();
                rate_ppn = item.find("#ppn_asli_modal_asuransi", tr).val();
            }



            var asuransi = item.find("#nama_asuransi_modal_asuransi :selected", tr).text();
            var id_asuransi = item.find("#nama_asuransi_modal_asuransi", tr).val();
            var share = item.find("input#share_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var tsi = item.find("input#tsi_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(tsi);
            var premi_gross = item.find("input#gross_premi_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(premi_gross);
            var diskon = item.find("input#diskon_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(diskon_asd);
            var premi = item.find("input#premi_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(premi);
            var rate_broker = item.find("input#fee_broker_modal_asuransi", tr).val();
            // console.log(diskon_broker);
            var gross_broker = item.find("input#gross_brokerage_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(biaya_polis);
            var hut_premi = item.find("input#hut_premi_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(biaya_materai);
            var biaya_polis = item.find("input#biaya_polis_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(biaya_adm);
            var biaya_materai = item.find("input#biaya_materai_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            // console.log(total_tsi);
            var sebelum_pajak = item.find("input#jumlah_sebelum_pajak_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var ppn_asli = item.find("input#ppn_asli_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var ppn = item.find("input#ppn_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var net_premi = item.find("input#net_premi_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var pph = item.find("input#pph_23_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var pajak = item.find("#jn_pajak_modal_asuransi", tr).val();
            var dpp = item.find("input#dpp_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var nilai_klaim = item.find("input#nilai_klaim_modal_asuransi", tr).val().replace(/\./g,"").replace(',','.');
            var keterangan = item.find("input#keterangan_klaim_modal_asuransi", tr).val().replace(/\./g,"");
            // console.log(total_tsi);

            sum_tsi = parseFloat(tsi) + sum_tsi;
            sum_share = parseFloat(share) + sum_share;
            sum_premi_gross = parseFloat(premi_gross) + sum_premi_gross;
            sum_diskon = parseFloat(diskon) + sum_diskon;
            sum_premi = parseFloat(premi) + sum_premi;
            sum_rate_broker = parseFloat(rate_broker) + sum_rate_broker;
            sum_gross_brokerage = parseFloat(gross_broker) + sum_gross_brokerage;
            sum_hut_premi = parseFloat(hut_premi) + sum_hut_premi;
            sum_biaya_materai = parseFloat(biaya_materai) + sum_biaya_materai;
            sum_biaya_polis = parseFloat(biaya_polis) + sum_biaya_polis;
            sum_sebelum_pajak = parseFloat(sebelum_pajak) + sum_sebelum_pajak;
            sum_ppn_asli = parseFloat(ppn_asli) + sum_ppn_asli;
            sum_ppn = parseFloat(ppn) + sum_ppn;
            sum_pph = parseFloat(pph) + sum_pph;
            sum_net_premi = parseFloat(net_premi) + sum_net_premi;
            sum_pajak = parseFloat(pajak) + sum_pajak;
            sum_dpp = parseFloat(dpp) + sum_dpp;
            sum_nilai_klaim = parseFloat(nilai_klaim) + sum_nilai_klaim;

            arrItem.asuransi = asuransi;
            arrItem.id_asuransi = id_asuransi;
            arrItem.tsi = parseFloat(tsi);
            arrItem.share = parseFloat(share);
            arrItem.premi_gross = parseFloat(premi_gross);
            arrItem.diskon = parseFloat(diskon);
            arrItem.premi = parseFloat(premi);
            arrItem.rate_broker = parseFloat(rate_broker);
            arrItem.gross_broker = parseFloat(gross_broker);
            arrItem.hut_premi = parseFloat(hut_premi);
            arrItem.biaya_polis = parseFloat(biaya_polis);
            arrItem.biaya_materai = parseFloat(biaya_materai);
            arrItem.sebelum_pajak = parseFloat(sebelum_pajak);
            arrItem.ppn_asli = parseFloat(ppn_asli);
            arrItem.ppn = parseFloat(ppn);
            arrItem.net_premi = parseFloat(net_premi);
            arrItem.pph = parseFloat(pph);
            arrItem.pajak = parseFloat(pajak);
            arrItem.dpp = parseFloat(dpp);
            arrItem.nilai_klaim = parseFloat(nilai_klaim);
            arrItem.keterangan = parseFloat(keterangan);

            arrAsd.data.push(arrItem);
            total_count = total_count + 1;
        });

        arrAsd.totalData.sum_tsi = sum_tsi;
        arrAsd.totalData.sum_share = sum_share;
        arrAsd.totalData.sum_premi_gross = sum_premi_gross;
        arrAsd.totalData.sum_premi = sum_diskon;
        arrAsd.totalData.sum_rate_broker = sum_rate_broker;
        arrAsd.totalData.sum_gross_brokerage = sum_gross_brokerage;
        arrAsd.totalData.sum_hut_premi = sum_hut_premi;
        arrAsd.totalData.sum_biaya_materai = sum_biaya_materai;
        arrAsd.totalData.sum_biaya_polis = sum_biaya_polis;
        arrAsd.totalData.sum_sebelum_pajak = sum_sebelum_pajak;
        arrAsd.totalData.sum_ppn_asli = sum_ppn_asli;
        arrAsd.totalData.sum_ppn = sum_ppn;
        arrAsd.totalData.sum_pph = sum_pph;
        arrAsd.totalData.sum_net_premi = sum_net_premi;
        arrAsd.totalData.sum_pajak = sum_pajak;
        arrAsd.totalData.sum_dpp = sum_dpp;
        arrAsd.totalData.sum_nilai_klaim = sum_nilai_klaim;
        arrAsd.totalData.count_asd = total_count;
        arrAsd.totalData.sum_diskon = sum_diskon;

        // console.log(arrAsd);

        arrAsd.data.sort((a, b) => parseFloat(b.share) - parseFloat(a.share));

        var json = JSON.stringify(arrAsd);
        // console.log(json);
        $('#asuransi').val(arrAsd.data[0].asuransi);
        $('#json_data_asd').val(json);
        $('#asuransi_leader').val(arrAsd.data[0].asuransi);
        $('#share_leader').val(arrAsd.data[0].share + ' %');
        $('#asuransi_leader_id').val(arrAsd.data[0].id_asuransi);
        $('#gross_broker_rate').val(arrAsd.data[0].rate_broker + ' %');
        $('#rate_ppn').val(formatRupiah(arrAsd.totalData.sum_ppn.toFixed(2).toString().replace('.',','),'Rp. '));
        $('#gross_broker').val(formatRupiah(arrAsd.totalData.sum_gross_brokerage.toFixed(2).toString().replace('.',','),'Rp. '));
        $('#net_brokerage').val(formatRupiah(arrAsd.totalData.sum_gross_brokerage.toFixed(2).toString().replace('.',','),'Rp. '));
        $('#premi_to_ceding').val(formatRupiah(arrAsd.totalData.sum_net_premi.toFixed(2).toString().replace('.',','),'Rp. '));

        $('#modalRincianAsuransi').modal('hide');
    }

    $('body').on('keyup', '#tableInstallment #installment_modalInstallment_gross_premi', function () {
        let jsons = JSON.parse($('#json_data_asd').val());
        // console.log(jsons);
        var cIndex = $(this).index();
        var tr = $(this).closest('tr');
        rowIndex = tr.index();
        var premi = tr.find('input#installment_modalInstallment_gross_premi').val().replace(/\./g,"").replace(',','.');
        var discBro = tr.find('input#installment_modalInstallment_diskon_broker').val().replace(/\./g,"").replace(',','.');
        var discIns = tr.find('input#installment_modalInstallment_diskon_asd').val().replace(/\./g,"").replace(',','.');
        var polis = tr.find('input#installment_modalInstallment_biaya_polis').val().replace(/\./g,"").replace(',','.');
        var materai = tr.find('input#installment_modalInstallment_biaya_materai').val().replace(/\./g,"").replace(',','.');
        var adm_broker = tr.find('input#installment_modalInstallment_biaya_adm_broker').val().replace(/\./g,"").replace(',','.');
        var rate = jsons.totalData.sum_rate_broker;
        var ratedisc = jsons.totalData.sum_diskon;
        var premiAsuransi = 0;
        var discount = 0;
        var jumlahBayar = 0;
        var brokerageNet = 0;
        if(rate){
           jumlahBayar = parseFloat(premi) - parseFloat(discBro) - parseFloat(discIns) + parseFloat(polis) + parseFloat(materai) + parseFloat(adm_broker);
           brokerageNet = ( parseFloat(premi) * parseFloat(rate) ) / 100;
        }

        tr.find('input#installment_modalInstallment_biaya_net').val(formatRupiah(jumlahBayar.toFixed(2).toString().replace('.',','),'Rp. '));
        tr.find('input#installment_modalInstallment_brokerage').val(formatRupiah(brokerageNet.toFixed(2).toString().replace('.',','),'Rp. '));
    });

    function saveInstallment()
    {
        $("#tableInstallment2 > tbody").html("");

        let jsons = JSON.parse($('#json_data_asd').val());
        // let jsons = JSON.parse('{"data":[{"asuransi":"BRINS","id_asuransi":"2","tsi":1000000000,"share":70,"premi_gross":700000000,"diskon":0,"premi":700000000,"rate_broker":10,"gross_broker":70000000,"biaya_materai":0,"biaya_polis":0,"sebelum_pajak":0,"ppn_asli":0,"ppn":0,"pph":0,"net_premi":0,"hut_premi":630000000,"pajak":0,"dpp":0,"nilai_klaim":0,"keterangan":0},{"asuransi":"TAKAFUL UMUM","id_asuransi":"1","tsi":1000000000,"share":30,"premi_gross":300000000,"diskon":0,"premi":300000000,"rate_broker":10,"gross_broker":30000000,"biaya_materai":0,"biaya_polis":0,"sebelum_pajak":0,"ppn_asli":0,"ppn":0,"pph":0,"net_premi":0,"hut_premi":270000000,"pajak":0,"dpp":0,"nilai_klaim":0,"keterangan":0}],"totalData":{"sum_tsi":2000000000,"sum_share":100,"sum_premi_gross":1000000000,"sum_diskon":0,"sum_premi":0,"sum_rate_broker":20,"sum_gross_brokerage":100000000,"sum_hut_premi":900000000,"sum_biaya_materai":0,"sum_biaya_polis":0,"sum_sebelum_pajak":0,"sum_ppn_asli":0,"sum_ppn":0,"sum_pph":0,"sum_net_premi":0,"sum_pajak":0,"sum_dpp":0,"sum_nilai_klaim":0,"count_asd":2}}');   

        // console.log(countTable); return false;
        var countData = 1;

         let instalment = 1;
         var countInstalment = $('#tableInstallment tr').length;
         if(countInstalment > 0){
            $('#tableInstallment > tbody  > tr').each(function(index, tr) {
                var item = $(this, tr);
                var premis = item.find('input#installment_modalInstallment_gross_premi').val().replace(/\./g,"").replace(',','.');
                var preminets = item.find('input#installment_modalInstallment_biaya_net').val().replace(/\./g,"").replace(',','.');
                var brokerages = item.find('input#installment_modalInstallment_brokerage').val().replace(/\./g,"").replace(',','.');
                var discBros = item.find('input#installment_modalInstallment_diskon_broker').val().replace(/\./g,"").replace(',','.');
                var discInss = item.find('input#installment_modalInstallment_diskon_asd').val().replace(/\./g,"").replace(',','.');
                var poliss = item.find('input#installment_modalInstallment_biaya_polis').val().replace(/\./g,"").replace(',','.');
                var materais = item.find('input#installment_modalInstallment_biaya_materai').val().replace(/\./g,"").replace(',','.');
                var adm_brokers = item.find('input#installment_modalInstallment_biaya_adm_broker').val().replace(/\./g,"").replace(',','.');
                var tempo = item.find('input#installment_modalInstallment_jatuh_tempo').val();

                for (let index2 = 0; index2 < jsons.data.length; index2++) {
                    var countInst = 1;
                    var arrItem = {
                        instalment : '',
                        id_asuransi : '',
                        asuransi : '',
                        tgl_tempo : '',
                        premi_gross : 0,
                        diskon_asd : 0,
                        premi : 0,
                        brokerage : 0,
                        biaya_materai : 0,
                        biaya_polis : 0,
                        sebelum_pajak : 0,
                        tax_index : 'E',
                        ppn : 0,
                        pph : 0,
                        iuran_ojk : 0,
                        total : 0,
                        dpp : 0
                    };

                    const element = jsons.data[index2];
                    const share = element.share;

                    // const element_inst = arrInstalment.data[index];
                    // let tempo = arrInstalment.data[index].tgl_tempo;

                    let premi_gross = (parseFloat(premis) * share) / 100 ;
                    let premi = (parseFloat(preminets) * share) / 100;
                    let brokerage = (parseFloat(brokerages) * share) / 100;
                    let diskon_asd = (parseFloat(discInss) * share) / 100;
                    let biaya_polis = 0;
                    let biaya_materai = 0;
                    let dpp = 0;
                    let ppn = 0;
                    let pph = 0;

                    if(countInst == '1'){
                        biaya_polis = parseFloat(poliss);
                        biaya_materai = parseFloat(materais);
                    }
                        dpp = parseFloat(element.dpp) / countInstalment;
                        ppn = parseFloat(element.ppn_asli) / countInstalment;
                        pph = parseFloat(element.pph) / countInstalment;

                    let jumlah =  premi - dpp + pph;
                    let iuran_ojk = (dpp * 1.2) / 100;

                    markup =
                    `<tr>
                        <td>
                        <input type="text" value="`+instalment+`" class="form-control" style="width: 70px;" id="installment_modalInstallment_dua_instalment" name="installment_modalInstallment_dua_instalment[]">
                        </td>
                        <td>
                        <input type="text" value="`+element.id_asuransi+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_asd" name="installment_modalInstallment_dua_asd[]">
                        </td>
                        <td>
                        <input type="text" value="`+element.asuransi+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_asd_nama" name="installment_modalInstallment_dua_asd_nama[]">
                        </td>
                        <td>
                        <input type="text" value="`+tempo+`"  class="form-control" style="width: 150px;" id="installment_modalInstallment_dua_tgl_tempo" name="installment_modalInstallment_dua_tgl_tempo[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(premi_gross.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_gross_premi" name="installment_modalInstallment_dua_gross_premi[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(diskon_asd.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_diskon_asd" name="installment_modalInstallment_dua_diskon_asd[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(premi.toFixed(2).toString().replace('.',','),'Rp. ')+`" class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_premi" name="installment_modalInstallment_dua_premi[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(brokerage.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_brokerage" name="installment_modalInstallment_dua_brokerage[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(biaya_polis.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_biaya_polis" name="installment_modalInstallment_dua_biaya_polis[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(biaya_materai.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_biaya_materai" name="installment_modalInstallment_dua_biaya_materai[]">
                        </td>
                        <td>
                        <input type="text" value="E" class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_tax_index" name="installment_modalInstallment_dua_tax_index[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(dpp.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_dpp" name="installment_modalInstallment_dua_dpp[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(ppn.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_ppn" name="installment_modalInstallment_dua_ppn[]">
                        </td>
                        <td>
                        <input type="text"  value="`+formatRupiah(pph.toFixed(2).toString().replace('.',','),'Rp. ')+`" class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_pph" name="installment_modalInstallment_dua_pph[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(iuran_ojk.toFixed(2).toString().replace('.',','),'Rp. ')+`" class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_iuran_ojk" name="installment_modalInstallment_dua_iuran_ojk[]">
                        </td>
                        <td>
                        <input type="text" value="`+formatRupiah(jumlah.toFixed(2).toString().replace('.',','),'Rp. ')+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_dua_jumlah" name="installment_modalInstallment_dua_jumlah[]">
                        </td>
                    </tr>`;
                    tableBody = $("#tableInstallment2");
                    tableBody.append(markup);

                    countInst++;
                }
                instalment++;
            });
         }

        // $('#tableInstallment > tbody  > tr').each(function(index, tr) {
        //     var item = $(this, tr);
        //     var polis = 0;
        //     var materai = 0;
        //     var adm = 0;
        //     // console.log(index);
        //     // if(index == 0){
        //         polis = parseFloat(arrInstalment.data[index].biaya_polis);
        //         materai = parseFloat(arrInstalment.data[index].biaya_materai);
        //         adm = parseFloat(arrInstalment.data[index].biaya_adm_broker);
        //     // }
        //     item.find("#installment_modalInstallment_instalment", tr).val(arrInstalment.data[index].instalment);
        //     item.find("#installment_modalInstallment_jatuh_tempo", tr).val(arrInstalment.data[index].tgl_tempo);
        //     item.find("#installment_modalInstallment_tgl_kwitansi", tr).val(arrInstalment.data[index].tgl_kwitansi);
        //     item.find("#installment_modalInstallment_gross_premi", tr).val(formatRupiah(arrInstalment.data[index].gross_premi.toString(),'Rp. '));
        //     item.find("#installment_modalInstallment_diskon_asd", tr).val(formatRupiah(arrInstalment.data[index].diskon_asd.toString(),'Rp. '));
        //     item.find("#installment_modalInstallment_diskon_broker", tr).val(formatRupiah(arrInstalment.data[index].diskon_bro.toString(),'Rp. '));
        //     item.find("#installment_modalInstallment_biaya_polis", tr).val(formatRupiah(polis.toString(),'Rp. '));
        //     item.find("#installment_modalInstallment_biaya_materai", tr).val(formatRupiah(materai.toString(),'Rp. '));
        //     item.find("#installment_modalInstallment_biaya_adm_broker", tr).val(formatRupiah(adm.toString(),'Rp. '));
        //     item.find("#installment_modalInstallment_biaya_net", tr).val(formatRupiah(arrInstalment.data[index].jumlah.toString(),'Rp. '));
        //     item.find("#installment_modalInstallment_brokerage", tr).val(formatRupiah(arrInstalment.data[index].brokerage.toString(),'Rp. '));
        // });

    }

    function saveToform()
    {
        var checklistData = [];
        var checklistId = [];

        var sumTsi = 0;
        $("#buttonTsi").each(function (){
            var tr = $(this).parent().parent()[0];
            var rowIndex = $("#tableTsi").dataTable().fnGetPosition(tr);
            var tsina = data[5].replace(/\,/g,'');

            // var datas = {}
            // var datasId = {}

            console.log(tsina);

            datas.id = data[1];
            // datasId.id = data[1];
            datas.tsi = tsina;

            checklistData.push(datas);
            // checklistId.push(datasId);
        });
        // console.log(checklistData);

        for (var i = 0; i < checklistData.length; i++) {
             const needle = checklistData[i];

             sumTsi = sumTsi + parseFloat(needle.gross_kontribusi);
         }
         var sumTsi = parseFloat(sumTsi) * 0.85;

        $("#tsi").val(parseFloat(sumUtang.toFixed(0)).toLocaleString());

        // $("#tampung").val("");
        // $("#tampung").val(JSON.stringify(checklistData));
        // $("#tampung_id").val(JSON.stringify(checklistId));

    }

    function saveInputTsi()
    {
        $('#no_polis').val($('#no_polis_modal_tsi').val());
        $('#tsi').val($('#tsi_modal_tsi').val());
        $('#jangka_waktu_dari').val($('#tanggal_awal_modal_tsi').val());
        $('#jangka_waktu_sampai').val($('#tanggal_akhir_modal_tsi').val());
        $('#keterangan').val($('#keterangan_modal_tsi').val());
        $('#biaya_polis').val($('#biaya_polis_modal_tsi').val());
        $('#materai').val($('#materai_modal_tsi').val());
        $('#biaya_polis').val($('#biaya_polis_modal_tsi').val());

        $('#modalTsi').modal('hide');
    }

    function addRowTsi()
    {
        markup = '<tr> <td><button type="button" class="btn btn-danger btn-sm" onclick="removeRowTsi()"><i class=" las la-minus"></i></button></td><td><input type="text" name="no_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="no_polis_modal_tsi"></td><td><input type="text" name="keterangan_modal_tsi[]" class="form-control" style="width: 300px;" id="keterangan_modal_tsi"></td> <td><input type="date" name="tanggal_awal_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_awal_modal_tsi"></td><td><input type="date" name="tanggal_akhir_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_akhir_modal_tsi"></td><td><input type="text" name="tsi_modal_tsi[]" class="form-control" style="width: 300px;" id="tsi_modal_tsi"></td><td><input type="text" name="rate_premi_modal_tsi[]" class="form-control" style="width: 300px;" id="rate_premi_modal_tsi"></td><td><input type="text" name="premi_gross_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_gross_modal_tsi"> </td> <td><input type="text" name="diskon_asd_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_persen_modal_tsi"> </td><td><input type="text" name="diskon_asd_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_modal_tsi"> </td><td><input type="text" name="premi_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_modal_tsi"> </td><td><input type="text" name="diskon_broker_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_persen_modal_tsi"></td> <td> <input type="text" name="diskon_broker_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_modal_tsi">  </td> <td><input type="text" name="biaya_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_polis_modal_tsi"></td> <td><input type="text" name="biaya_materai_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_materai_modal_tsi"></td> <td>  <input type="text" name="biaya_adm_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_adm_modal_tsi" value="50.000"> </td>  <td><input type="text" name="jumlah_modal_tsi[]" class="form-control" style="width: 300px;" id="jumlah_modal_tsi"> </td></tr>'; 
                tableBody = $("#tableTsi"); 
                tableBody.append(markup); 
    }

    function calculateInstalment(){

        var arrInstalment = {
            count_instalment : 0,
            data : []
        };
        var countInstalment = $('#tableInstallment tbody tr').length;
        var grosspremi = $('#gross_premi').val().replace(/\./g,"").replace(',','.');
        var grossbrokerage = $('#gross_broker').val().replace(/\./g,"").replace(',','.');
        var biaya_materai = $('#materai').val().replace(/\./g,"").replace(',','.');
        var biaya_polis = $('#biaya_polis').val().replace(/\./g,"").replace(',','.');
        var biaya_adm_bro = $('#biaya_admin_broker').val().replace(/\./g,"").replace(',','.');
        var discIns = $('#diskon_asd').val().replace(/\./g,"").replace(',','.');
        var discBro = $('#diskon_broker').val().replace(/\./g,"").replace(',','.');

        $('#tableInstallment > tbody  > tr').each(function(index, tr) {
            var item = $(this, tr);
            var dataInst = {
                    instalment : 0,
                    tgl_tempo : '',
                    tgl_kwitansi : '',
                    gross_premi : 0,
                    diskon_asd : 0,
                    diskon_bro : 0,
                    biaya_polis : 0,
                    biaya_materai : 0,
                    biaya_adm_broker : 0,
                    jumlah : 0,
                    brokerage : 0
                };

            var instalment = item.find("#installment_modalInstallment_instalment", tr).val();
            var tgl_tempo = item.find("#installment_modalInstallment_jatuh_tempo", tr).val();
            var tgl_kwitansi = item.find("#installment_modalInstallment_tgl_kwitansi", tr).val();
            var gross_premi = parseFloat(grosspremi) / parseFloat(countInstalment);
            var diskon_asd = 0;
            if(discIns > 0){
                diskon_asd = parseFloat(discIns) / parseFloat(countInstalment);
            }

            var diskon_bro = 0;
            if(discBro > 0){
                diskon_bro = parseFloat(discBro) / parseFloat(countInstalment);
            }

            var polis = 0;
            var materai = 0;
            var adm_broker = 0;

            if(countData == 1){
                polis = parseFloat(biaya_polis);
                materai = parseFloat(biaya_materai);
                adm_broker = parseFloat(biaya_adm_bro);
            }

            var jumlahPremiDiskon = gross_premi - diskon_asd - diskon_bro;
            var jumlahBayar = jumlahPremiDiskon + adm_broker + polis + materai;
            var grossbrokerages  = parseFloat(grossbrokerage)  / countInstalment
            var jumlahBrokerage = parseFloat(grossbrokerage) + parseFloat(polis) + parseFloat(materai) + parseFloat(adm_broker);

            // console.log(grossbrokerages);
            // console.log(jumlahBrokerage);

            dataInst.instalment = parseFloat(instalment);
            dataInst.tgl_tempo = tgl_tempo;
            dataInst.tgl_kwitansi = tgl_kwitansi;
            dataInst.gross_premi = parseFloat(gross_premi);
            dataInst.diskon_asd = parseFloat(diskon_asd);
            dataInst.diskon_bro = parseFloat(diskon_bro);
            dataInst.biaya_polis = parseFloat(polis);
            dataInst.biaya_materai = parseFloat(materai);
            dataInst.biaya_adm_broker = parseFloat(adm_broker);
            dataInst.jumlah = parseFloat(jumlahBayar);
            dataInst.brokerage = parseFloat(jumlahBrokerage);

            arrInstalment.data.push(dataInst);

            var polis = 0;
            var materai = 0;
            var adm = 0;

            item.find("#installment_modalInstallment_instalment", tr).val(instalment);
            item.find("#installment_modalInstallment_jatuh_tempo", tr).val(tgl_tempo);
            item.find("#installment_modalInstallment_tgl_kwitansi", tr).val(tgl_kwitansi);
            item.find("#installment_modalInstallment_gross_premi", tr).val(formatRupiah(gross_premi.toString(),'Rp. '));
            item.find("#installment_modalInstallment_diskon_asd", tr).val(formatRupiah(diskon_asd.toString(),'Rp. '));
            item.find("#installment_modalInstallment_diskon_broker", tr).val(formatRupiah(diskon_bro.toString(),'Rp. '));
            item.find("#installment_modalInstallment_biaya_polis", tr).val(formatRupiah(polis.toString(),'Rp. '));
            item.find("#installment_modalInstallment_biaya_materai", tr).val(formatRupiah(materai.toString(),'Rp. '));
            item.find("#installment_modalInstallment_biaya_adm_broker", tr).val(formatRupiah(adm_broker.toString(),'Rp. '));
            item.find("#installment_modalInstallment_biaya_net", tr).val(formatRupiah(jumlahBayar.toString(),'Rp. '));
            item.find("#installment_modalInstallment_brokerage", tr).val(formatRupiah(jumlahBrokerage.toString(),'Rp. '));

            countData++;
        });
        }

    function addRowInstalment()
    {
        var jsons = JSON.parse($('#json_data_asd').val());
        var countInstalment = 1;
        var gross_premi = $('#gross_premi').val().replace(/\./g,"").replace(',','.');
        var biaya_materai = $('#materai').val().replace(/\./g,"").replace(',','.');
        var biaya_polis = $('#biaya_polis').val().replace(/\./g,"").replace(',','.');
        var biaya_admin_broker = $('#biaya_admin_broker').val().replace(/\./g,"").replace(',','.');
        var jumlah_setelah_admin_broker = $('#jumlah_setelah_admin_broker').val().replace(/\./g,"").replace(',','.');
        var gross_broker = $('#gross_broker').val().replace(/\./g,"").replace(',','.');
        var disc_ins = $('#diskon_asd').val().replace(/\./g,"").replace(',','.');
        var disc_bro = $('#diskon_broker').val().replace(/\./g,"").replace(',','.');

        $('#tableInstallment > tbody  > tr').each(function(index, tr) { 
            var item = $(this, tr);
            // console.log(index);
            // console.log(tr);
            if (item.find("#installment_modalInstallment_jatuh_tempo", tr).length > 0) {
                countInstalment++;
                // console.log(index);
            }
          });

        gross_premi = parseFloat(gross_premi) / countInstalment;
        

        if(countInstalment == '1'){
            $("#tableInstallment > tbody").html("");

        markup = `<tr>
                <td>
                    <button type="button" class="btn btn-danger btn-sm" onclick="removeRowInstalment()"><i class=" las la-minus"></i></button>
                </td>
                <td>
                    <input type="text" value = "`+countInstalment+`"  class="form-control" style="width: 70px;" id="installment_modalInstallment_instalment" name="installment_modalInstallment_instalment[]">
                </td>
                <td>
                    <input type="date"  class="form-control" style="width: 150px;" id="installment_modalInstallment_tgl_kwitansi" name="installment_modalInstallment_tgl_kwitansi[]">
                </td>
                <td>
                    <input type="date"  class="form-control" style="width: 150px;" id="installment_modalInstallment_jatuh_tempo" name="installment_modalInstallment_jatuh_tempo[]">
                </td>
                <td>
                    <input type="text" value="`+formatRupiah(gross_premi.toFixed(2).toString().replace('.',','),'Rp. ')+`" class="form-control" style="width: 300px;" id="installment_modalInstallment_gross_premi" name="installment_modalInstallment_gross_premi[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_diskon_asd" name="installment_modalInstallment_diskon_asd[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_diskon_broker" name="installment_modalInstallment_diskon_broker[]">
                </td>
                <td>
                    <input type="text" value="`+0+`" class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_polis" name="installment_modalInstallment_biaya_polis[]">
                </td>
                <td>
                    <input type="text" value="`+0+`" class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_materai" name="installment_modalInstallment_biaya_materai[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_adm_broker" name="installment_modalInstallment_biaya_adm_broker[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"   class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_net" name="installment_modalInstallment_biaya_net[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"   class="form-control" style="width: 300px;" id="installment_modalInstallment_brokerage" name="installment_modalInstallment_brokerage[]">
                </td></tr>`; 
        }else{
            markup = `<tr> 
                <td>
                    <button type="button" class="btn btn-danger btn-sm" onclick="removeRowInstalment()"><i class=" las la-minus"></i></button>
                </td>
                <td>
                    <input type="text" value = "`+countInstalment+`"  class="form-control" style="width: 70px;" id="installment_modalInstallment_instalment" name="installment_modalInstallment_instalment[]">
                </td>
                <td>
                    <input type="date"  class="form-control" style="width: 150px;" id="installment_modalInstallment_tgl_kwitansi" name="installment_modalInstallment_tgl_kwitansi[]">
                </td>
                <td>
                    <input type="date"  class="form-control" style="width: 150px;" id="installment_modalInstallment_jatuh_tempo" name="installment_modalInstallment_jatuh_tempo[]">
                </td>
                <td>
                    <input type="text" value="`+formatRupiah(gross_premi.toFixed(2).toString().replace('.',','),'Rp. ')+`" class="form-control" style="width: 300px;" id="installment_modalInstallment_gross_premi" name="installment_modalInstallment_gross_premi[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_diskon_asd" name="installment_modalInstallment_diskon_asd[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_diskon_broker" name="installment_modalInstallment_diskon_broker[]">
                </td>
                <td>
                    <input type="text" value="0" class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_polis" name="installment_modalInstallment_biaya_polis[]">
                </td>
                <td>
                    <input type="text" value="0" class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_materai" name="installment_modalInstallment_biaya_materai[]">
                </td>
                <td>
                    <input type="text" value="0"  class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_adm_broker" name="installment_modalInstallment_biaya_adm_broker[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_biaya_net" name="installment_modalInstallment_biaya_net[]">
                </td>
                <td>
                    <input type="text" value="`+0+`"  class="form-control" style="width: 300px;" id="installment_modalInstallment_brokerage" name="installment_modalInstallment_brokerage[]">
                </td></tr>`;
        }
                tableBody = $("#tableInstallment");
                tableBody.append(markup);
                var rate = jsons.totalData.sum_rate_broker;

                $('#tableInstallment > tbody  > tr').each(function(index, tr) {
                    var item = $(this, tr);
                    // console.log(index);
                    if(index == 0){
                        biaya_polis = parseFloat(biaya_polis);
                        biaya_materai = parseFloat(biaya_materai);
                        biaya_admin_broker = parseFloat(biaya_admin_broker);
                        var disc_bros = parseFloat(disc_bro);
                        var disc_inss = parseFloat(disc_ins);
                        if(parseFloat(disc_ins) > 0){
                            disc_inss = parseFloat(disc_inss) / countInstalment;
                        }

                        if(parseFloat(disc_bro) > 0){
                            disc_bros = parseFloat(disc_bros) / countInstalment;
                        }


                        gross_broker = (gross_premi * rate) / 100;
                        jumlah_setelah_admin_broker = (gross_premi - disc_inss - disc_bros) + (biaya_polis + biaya_materai + biaya_admin_broker);

                        item.find("#installment_modalInstallment_gross_premi", tr).val(formatRupiah(gross_premi.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_asd", tr).val(formatRupiah(disc_inss.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_broker", tr).val(formatRupiah(disc_bros.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_polis", tr).val(formatRupiah(biaya_polis.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_materai", tr).val(formatRupiah(biaya_materai.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_adm_broker", tr).val(formatRupiah(biaya_admin_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_net", tr).val(formatRupiah(jumlah_setelah_admin_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_brokerage", tr).val(formatRupiah(gross_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                    }else{
                        var disc_bros = parseFloat(disc_bro);
                        var disc_inss = parseFloat(disc_ins);
                        if(parseFloat(disc_ins) > 0){
                            disc_inss = parseFloat(disc_inss) / countInstalment;
                        }

                        if(parseFloat(disc_bro) > 0){
                            disc_bros = parseFloat(disc_bros) / countInstalment;
                        }

                        gross_broker = (gross_premi * rate) / 100;
                        jumlah_setelah_admin_broker = gross_premi - disc_inss - disc_bros;

                        item.find("#installment_modalInstallment_gross_premi", tr).val(formatRupiah(gross_premi.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_asd", tr).val(formatRupiah(disc_inss.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_broker", tr).val(formatRupiah(disc_bros.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_polis", tr).val('0');
                        item.find("#installment_modalInstallment_biaya_materai", tr).val('0');
                        item.find("#installment_modalInstallment_biaya_adm_broker", tr).val('0');
                        item.find("#installment_modalInstallment_biaya_net", tr).val(formatRupiah(jumlah_setelah_admin_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_brokerage", tr).val(formatRupiah(gross_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                    }
                });
    }

    function addRowRincianAsuransi()
    {
        rincians = `
        <tr>
            <td>
                <button type="button" class="btn btn-danger btn-sm" onclick="removeRowRincianAsuransi()"><i class=" las la-minus"></i></button>
            </td>
            <td>
                <select class="form-select" name="nama_asuransi_modal_asuransi[]" id="nama_asuransi_modal_asuransi"  style="width: 300px;">
                    <option></option>
                    <option value="1">TAKAFUL UMUM</option>
                    <option value="2">BRINS</option>
                </select>
            </td>
            <td>
                <select class="form-select" name="jn_pajak_modal_asuransi[]" id="jn_pajak_modal_asuransi"  style="width: 300px;">
                    <option></option>
                    <option value="1">INCLUDE</option>
                    <option value="2">EXCLUDE</option>
                </select>
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="share_modal_asuransi" name="share_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="tsi_modal_asuransi" name="tsi_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="gross_premi_modal_asuransi" name="gross_premi_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="diskon_modal_asuransi" name="diskon_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="premi_modal_asuransi" name="premi_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="fee_broker_modal_asuransi" name="fee_broker_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="gross_brokerage_modal_asuransi" name="gross_brokerage_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="hut_premi_modal_asuransi" name="hut_premi_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="biaya_polis_modal_asuransi" name="biaya_polis_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="biaya_materai_modal_asuransi" name="biaya_materai_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="jumlah_sebelum_pajak_modal_asuransi" name="jumlah_sebelum_pajak_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="ppn_asli_modal_asuransi" name="ppn_asli_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="ppn_modal_asuransi" name="ppn_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="pph_23_modal_asuransi" name="pph_23_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="net_premi_modal_asuransi" name="net_premi_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="dpp_modal_asuransi" name="dpp_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="nilai_klaim_modal_asuransi" name="nilai_klaim_modal_asuransi[]">
            </td>
            <td>
                <input type="text"  class="form-control" style="width: 300px;" id="keterangan_klaim_modal_asuransi" name="keterangan_klaim_modal_asuransi[]">
            </td>
        </tr>
        `;
        tableBody = $("#tabelRincianAsuransi tbody"); 
                tableBody.append(rincians); 
    }

    function removeRowTsi()
    {
        $('#tableTsi tbody tr:last-child').remove(); 
    }

    function removeRowInstalment()
    {
        $('#tableInstallment tbody tr:last-child').remove(); 


        var countInstalment = $('#tableInstallment tbody tr').length;
        var jsons = JSON.parse($('#json_data_asd').val());
        var rate = jsons.totalData.sum_rate_broker;
        var gross_premi = $('#gross_premi').val().replace(/\./g,"").replace(',','.');
        var biaya_materai = $('#materai').val().replace(/\./g,"").replace(',','.');
        var biaya_polis = $('#biaya_polis').val().replace(/\./g,"").replace(',','.');
        var biaya_admin_broker = $('#biaya_admin_broker').val().replace(/\./g,"").replace(',','.');
        var jumlah_setelah_admin_broker = $('#jumlah_setelah_admin_broker').val().replace(/\./g,"").replace(',','.');
        var gross_broker = $('#gross_broker').val().replace(/\./g,"").replace(',','.');
        var disc_ins = $('#diskon_asd').val().replace(/\./g,"").replace(',','.');
        var disc_bro = $('#diskon_broker').val().replace(/\./g,"").replace(',','.');
        gross_premi = parseFloat(gross_premi) / countInstalment;

                $('#tableInstallment > tbody  > tr').each(function(index, tr) {
                    var item = $(this, tr);
                    // console.log(index);
                    if(index == 0){
                        biaya_polis = parseFloat(biaya_polis);
                        biaya_materai = parseFloat(biaya_materai);
                        biaya_admin_broker = parseFloat(biaya_admin_broker);
                        var disc_bros = parseFloat(disc_bro);
                        var disc_inss = parseFloat(disc_ins);
                        if(parseFloat(disc_ins) > 0){
                            disc_inss = parseFloat(disc_inss) / countInstalment;
                        }

                        if(parseFloat(disc_bro) > 0){
                            disc_bros = parseFloat(disc_bros) / countInstalment;
                        }


                        gross_broker = (gross_premi * rate) / 100;
                        jumlah_setelah_admin_broker = (gross_premi - disc_inss - disc_bros) + (biaya_polis + biaya_materai + biaya_admin_broker);

                        item.find("#installment_modalInstallment_gross_premi", tr).val(formatRupiah(gross_premi.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_asd", tr).val(formatRupiah(disc_inss.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_broker", tr).val(formatRupiah(disc_bros.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_polis", tr).val(formatRupiah(biaya_polis.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_materai", tr).val(formatRupiah(biaya_materai.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_adm_broker", tr).val(formatRupiah(biaya_admin_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_net", tr).val(formatRupiah(jumlah_setelah_admin_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_brokerage", tr).val(formatRupiah(gross_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                    }else{
                        var disc_bros = parseFloat(disc_bro);
                        var disc_inss = parseFloat(disc_ins);
                        if(parseFloat(disc_ins) > 0){
                            disc_inss = parseFloat(disc_inss) / countInstalment;
                        }

                        if(parseFloat(disc_bro) > 0){
                            disc_bros = parseFloat(disc_bros) / countInstalment;
                        }

                        gross_broker = (gross_premi * rate) / 100;
                        jumlah_setelah_admin_broker = gross_premi - disc_inss - disc_bros;

                        item.find("#installment_modalInstallment_gross_premi", tr).val(formatRupiah(gross_premi.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_asd", tr).val(formatRupiah(disc_inss.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_diskon_broker", tr).val(formatRupiah(disc_bros.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_biaya_polis", tr).val('0');
                        item.find("#installment_modalInstallment_biaya_materai", tr).val('0');
                        item.find("#installment_modalInstallment_biaya_adm_broker", tr).val('0');
                        item.find("#installment_modalInstallment_biaya_net", tr).val(formatRupiah(jumlah_setelah_admin_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                        item.find("#installment_modalInstallment_brokerage", tr).val(formatRupiah(gross_broker.toFixed(2).toString().replace('.',','),'Rp. '));
                    }
                });

    }

    function removeRowRincianAsuransi()
    {
        $('#tabelRincianAsuransi tbody tr:last-child').remove(); 
    }

    function save()
    {
        let err = 0;

        var statusna = $("#statusna").val();
        if (statusna == '') {
            $("#statusna-err").show();
            err = 1;
        } else{
            $('#statusna-err').hide();
        }

        var tgl_nota = $("#tgl_nota").val();
        if (tgl_nota == '') {
            $("#tgl_nota-err").show();
            err = 1;
        } else{
            $('#tgl_nota-err').hide();
        }

        var jenis_asuransi = $("#jenis_asuransi").val();
        if (jenis_asuransi == '') {
            $("#jenis_asuransi-err").show();
            err = 1;
        } else{
            $('#jenis_asuransi-err').hide();
        }

        var tertanggung = $("#tertanggung").val();
        if (tertanggung == '') {
            $("#tertanggung-err").show();
            err = 1;
        } else{
            $('#tertanggung-err').hide();
        }

        var currency = $("#currency").val();
        if (currency == '') {
            $("#currency-err").show();
            err = 1;
        } else{
            $('#currency-err').hide();
        }

        var tsi = $("#tsi").val();
        if (tsi == '') {
            $("#tsi-err").show();
            err = 1;
        } else{
            $('#tsi-err').hide();
        }

        var jangka_waktu_dari = $("#jangka_waktu_dari").val();
        if (jangka_waktu_dari == '') {
            $("#jangka_waktu_dari-err").show();
            err = 1;
        } else{
            $('#jangka_waktu_dari-err').hide();
        }

        var jangka_waktu_sampai = $("#jangka_waktu_sampai").val();
        if (jangka_waktu_sampai == '') {
            $("#jangka_waktu_sampai-err").show();
            err = 1;
        } else{
            $('#jangka_waktu_sampai-err').hide();
        }

        var rate_ppn = $("#rate_ppn").val();
        if (rate_ppn == '') {
            $("#rate_ppn-err").show();
            err = 1;
        } else{
            $('#rate_ppn-err').hide();
        }

        var gross_premi_rate = $("#gross_premi_rate").val();
        if (gross_premi_rate == '') {
            $("#gross_premi_rate-err").show();
            err = 1;
        } else{
            $('#gross_premi_rate-err').hide();
        }

        var gross_premi = $("#gross_premi").val();
        if (gross_premi == '') {
            $("#gross_premi-err").show();
            err = 1;
        } else{
            $('#gross_premi-err').hide();
        }

        var diskon_asd_persen = $("#diskon_asd_persen").val();
        if (diskon_asd_persen == '') {
            $("#diskon_asd_persen-err").show();
            err = 1;
        } else{
            $('#diskon_asd_persen-err').hide();
        }

        var diskon_asd = $("#diskon_asd").val();
        if (diskon_asd == '') {
            $("#diskon_asd-err").show();
            err = 1;
        } else{
            $('#diskon_asd-err').hide();
        }

        var materai = $("#materai").val();
        if (materai == '') {
            $("#materai-err").show();
            err = 1;
        } else{
            $('#materai-err').hide();
        }

        var biaya_polis = $("#biaya_polis").val();
        if (biaya_polis == '') {
            $("#biaya_polis-err").show();
            err = 1;
        } else{
            $('#biaya_polis-err').hide();
        }

        var biaya_admin_broker = $("#biaya_admin_broker").val();
        if (biaya_admin_broker == '') {
            $("#biaya_admin_broker-err").show();
            err = 1;
        } else{
            $('#biaya_admin_broker-err').hide();
        }

        var jumlah_sebelum_admin_broker = $("#jumlah_sebelum_admin_broker").val();
        if (jumlah_sebelum_admin_broker == '') {
            $("#jumlah_sebelum_admin_broker-err").show();
            err = 1;
        } else{
            $('#jumlah_sebelum_admin_broker-err').hide();
        }

        var jumlah_setelah_admin_broker = $("#jumlah_setelah_admin_broker").val();
        if (jumlah_setelah_admin_broker == '') {
            $("#jumlah_setelah_admin_broker-err").show();
            err = 1;
        } else{
            $('#jumlah_setelah_admin_broker-err').hide();
        }

        var premi_after_diskon = $("#premi_after_diskon").val();
        if (premi_after_diskon == '') {
            $("#premi_after_diskon-err").show();
            err = 1;
        } else{
            $('#premi_after_diskon-err').hide();
        }

        var asuransi_leader = $("#asuransi_leader").val();
        if (asuransi_leader == '') {
            $("#asuransi_leader-err").show();
            err = 1;
        } else{
            $('#asuransi_leader-err').hide();
        }

        var gross_broker_rate = $("#gross_broker_rate").val();
        if (gross_broker_rate == '') {
            $("#gross_broker_rate-err").show();
            err = 1;
        } else{
            $('#gross_broker_rate-err').hide();
        }

        var gross_broker = $("#gross_broker").val();
        if (gross_broker == '') {
            $("#gross_broker-err").show();
            err = 1;
        } else{
            $('#gross_broker-err').hide();
        }

        var net_brokerage = $("#net_brokerage").val();
        if (net_brokerage == '') {
            $("#net_brokerage-err").show();
            err = 1;
        } else{
            $('#net_brokerage-err').hide();
        }

        var premi_to_ceding = $("#premi_to_ceding").val();
        if (premi_to_ceding == '') {
            $("#premi_to_ceding-err").show();
            err = 1;
        } else{
            $('#premi_to_ceding-err').hide();
        }

        var premi_after_diskon_broker = $("#premi_after_diskon_broker").val();
        if (premi_after_diskon_broker == '') {
            $("#premi_after_diskon_broker-err").show();
            err = 1;
        } else{
            $('#premi_after_diskon_broker-err').hide();
        }

        

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        var formData = new FormData($("#form_produksi")[0]);
        $.ajax({
            url: '<?=base_url('produksi/save')?>',
            type:"POST",
            data:formData,
            processData:false,
            contentType:false,
            cache:false,
            async:true,
            beforeSend: function(){
                Swal.fire({
                    title: 'Mohon Tungu',
                    html: 'Proses Menyimpan Data ...',// add html attribute if you want or remove
                    allowOutsideClick : false,
                    showConfirmButton : false,
                    didOpen: () => {
                        Swal.showLoading()
                    }
                });
            },
            success: function(response) {
                swal.close();
                

                if (response.status == 'success') {
                    message('success', 'Berhasil disimpan');
                    window.setTimeout(function(){ 
                        window.location.href = '<?=base_url('produksi/index')?>';
                    } ,2000);
                } else {
                    warning('warning','Oopss... Data gagal disimpan, periksa kembali inputan anda!');
                }
                
            }, error: function(){
                 warning('error', 'Gagal disimpan');
            }
        });
    }

    function printInstallment()
    {
        var url = "<?php echo base_url('produksi/print_installment');?>";
        window.open(url);
    }

    function print_invoice()
    {
        var url = "<?php echo base_url('produksi/print_invoice');?>";
        window.open(url);
    }

    function print_lampiran()
    {
        var url = "<?php echo base_url('produksi/print_lampiran');?>";
        window.open(url);
    }

    function print_kwitansi()
    {
        var url = "<?php echo base_url('produksi/print_kwitansi');?>";
        window.open(url);
    }

    function deleteNota(id,type)
    {
        Swal.fire({
          title: 'Anda yakin akan menghapus Data Nota DN?',
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: 'Hapus!',
          denyButtonText: `Tidak`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            $.ajax({
                type: "POST", 
                url: "<?php echo base_url("produksi/delete/"); ?>" + id, 
                dataType: "json",
                beforeSend: function(){
                    Swal.fire({
                        title: 'Waits',
                        html: 'Deleting data in Progress',
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                success: function(response){ // Ketika proses pengiriman berhasil
                    swal.close();

                   if (response.status == 'success') {
                        message('success','Berhasil Dihapus!');
                         reloadTable();
                   } else {
                        warning('warning','Gagal Dihapus');
                   }

                    
                },
                error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                    swal.close();
                   warning('warning','Ooppss Gagal Disimpan!!')
                }
            });
          } else if (result.isDenied) {
           warning('warning','Gagal Dihapus');
          }
        })
    }

</script>









