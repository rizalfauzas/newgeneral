
<div class="container-fluid">

    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title;?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=$title?></a></li>
                        <li class="breadcrumb-item active">index</li>
                    </ol>
                </div>

            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                    <ul class="nav nav-tabs nav-border-top nav-border-top-primary mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#edit" role="tab" aria-selected="true">
                                <i class="ri-file-list-3-line"></i> Edit Produksi
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">

                        <div class="tab-pane p-3 active" id="edit" role="tabpanel">

                            <h5>Form Edit Produksi</h5><br>

                             <form method="post" id="form_produksi" enctype="multipart/form-data">

                                <div class="row">

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Status <code>*</code></label>
                                            <select class="form-select" name="statusna" id="statusna">
                                                <?php
                                                    $status = $this->db->query("select * from status_produksi")->result_array();
                                                    foreach ($status as $value) {
                                                        if ($q->status == $value['id']) {
                                                          echo"<option selected='selected' value='".$value['id']."'>".$value['status']."</option>"; 
                                                        } else {
                                                          echo"<option value='".$value['id']."'>".$value['status']."</option>"; 
                                                        }
                                                    }
                                                ?> 
                                            </select>
                                            <div id="statusna-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Tanggal Nota <code>*</code></label>
                                            <input type="date" class="form-control" name="tgl_nota" id="tgl_nota" value="<?php if (isset($q->tgl_nota)) {
                                                echo $q->tgl_nota;
                                            }?>" >
                                            <div id="tgl_nota-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jenis Asuransi <code>*</code></label>
                                            <select class="form-select" name="jenis_asuransi" id="jenis_asuransi" >
                                                <option></option>
                                                <?php
                                                    $jenis_asuransi = $this->db->query("select * from jenis_asuransi")->result_array();
                                                    foreach ($jenis_asuransi as $value) {
                                                        if ($q->jenis_asuransi == $value['id']) {
                                                          echo"<option selected='selected' value='".$value['id']."'>".$value['jenis_asuransi']."</option>"; 
                                                        } else {
                                                          echo"<option value='".$value['id']."'>".$value['jenis_asuransi']."</option>"; 
                                                        }
                                                    }
                                                ?> 
                                            </select>
                                            <div id="jenis_asuransi-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Tertanggung <code>*</code></label>
                                            <select class="form-select" name="tertanggung" id="tertanggung" >
                                                <option></option>
                                                <?php
                                                    $client = $this->db->query("select * from client")->result_array();
                                                    foreach ($client as $value) {
                                                        if ($q->tertanggung == $value['id']) {
                                                          echo"<option selected='selected' value='".$value['id']."'>".$value['nama']."</option>"; 
                                                        } else {
                                                          echo"<option value='".$value['id']."'>".$value['nama']."</option>"; 
                                                        }
                                                    }
                                                ?> 
                                            </select>
                                            <div id="tertanggung-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">TSI <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <select class="form-select" name="currency" id="currency">
                                                    <?php
                                                        $currency = $this->db->query("select * from currency")->result_array();
                                                        foreach ($currency as $value) {
                                                            if ($q->currency == $value['id']) {
                                                              echo"<option selected='selected' value='".$value['id']."'>".$value['currency']."</option>"; 
                                                            } else {
                                                              echo"<option value='".$value['id']."'>".$value['currency']."</option>"; 
                                                            }
                                                        }
                                                    ?> 
                                                </select>
                                                <input type="text" class="form-control" id="tsi" name="tsi" readonly="" value="<?php if (isset($q->tsi)) {
                                                    echo number_format($q->tsi);
                                                }else{echo 0;}?>" >
                                                <div id="tsi-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalTsi"><i class="  bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input TSI</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">No. Polis <code>*</code></label>
                                            <input type="text" class="form-control" name="no_polis" id="no_polis" placeholder="No. Polis" readonly value="<?php if (isset($q->no_polis)) {
                                                    echo $q->no_polis;
                                                }else{echo 0;}?>" >
                                            <div id="no_polis-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jangka Waktu Dari <code>*</code></label>
                                            <input type="date" class="form-control" name="jangka_waktu_dari" id="jangka_waktu_dari"  readonly value="<?php if (isset($q->jangka_waktu_dari)) {
                                                    echo $q->jangka_waktu_dari;
                                                }?>" >
                                            <div id="jangka_waktu_dari-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jangka Waktu Sampai <code>*</code></label>
                                            <input type="date" class="form-control" name="jangka_waktu_sampai" id="jangka_waktu_sampai"  readonly value="<?php if (isset($q->jangka_waktu_sampai)) {
                                                    echo $q->jangka_waktu_sampai;
                                                }?>" >
                                            <div id="jangka_waktu_sampai-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Gross Premi <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="gross_premi_rate" name="gross_premi_rate" value="<?php if (isset($q->gross_premi_rate)) {
                                                    echo $q->gross_premi_rate;
                                                }else{echo 0;}?>"  readonly="" placeholder="Rate">
                                                <input type="text" class="form-control" id="gross_premi" name="gross_premi" placeholder="Gross premi" readonly="" value="<?php if (isset($q->gross_premi)) {
                                                    echo number_format($q->gross_premi);
                                                }else{echo 0;}?>" >
                                                <div id="gross_premi-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Diskon ASD <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="diskon_asd_persen" name="diskon_asd_persen" readonly="" placeholder="Diskon %" value="<?php if (isset($q->diskon_asd_persen)) {
                                                    echo $q->diskon_asd_persen;
                                                }else{echo 0;}?>" >
                                                <input type="text" class="form-control" id="diskon_asd" name="diskon_asd" placeholder="Diskon ASD" readonly="" value="<?php if (!empty($q->diskon_asd)) {
                                                    echo number_format($q->diskon_asd);
                                                }else{echo 0;}?>" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Materai <code>*</code></label>
                                            <input type="text" class="form-control" name="materai" id="materai" placeholder="Materai" readonly value="<?php if (isset($q->materai)) {
                                                    echo number_format($q->materai);
                                                }else{echo 0;}?>" >
                                            <div id="materai-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Biaya Polis <code>*</code></label>
                                            <input type="text" class="form-control" name="biaya_polis" id="biaya_polis" placeholder="Biaya Polis" readonly value="<?php if (isset($q->biaya_polis)) {
                                                    echo number_format($q->biaya_polis);
                                                }else{echo 0;}?>" >
                                            <div id="biaya_polis-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Biaya Admin Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="biaya_admin_broker" id="biaya_admin_broker" placeholder="Biaya Admin Broker" readonly value="<?php if (isset($q->biaya_admin_broker)) {
                                                    echo number_format($q->biaya_admin_broker);
                                                }else{echo 0;}?>" >
                                            <div id="biaya_admin_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jumlah Sebelum Admin Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="jumlah_sebelum_admin_broker" placeholder="Jumlah Sebelum Admin Broker" id="jumlah_sebelum_admin_broker" readonly value="<?php if (isset($q->jumlah_sebelum_admin_broker)) {
                                                    echo number_format($q->jumlah_sebelum_admin_broker);
                                                }else{echo 0;}?>" >
                                            <div id="jumlah_sebelum_admin_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Jumlah Setelah Admin Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="jumlah_setelah_admin_broker" id="jumlah_setelah_admin_broker" placeholder="Jumlah Setelah Admin Broker" readonly value="<?php if (isset($q->jumlah_setelah_admin_broker)) {
                                                    echo number_format($q->jumlah_setelah_admin_broker);
                                                }else{echo 0;}?>" >
                                            <div id="jumlah_setelah_admin_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Premi After Diskon <code>*</code></label>
                                            <input type="text" class="form-control" name="premi_after_diskon" id="premi_after_diskon" placeholder="Premi After Diskon" readonly value="<?php if (isset($q->premi_after_diskon)) {
                                                    echo number_format($q->premi_after_diskon);
                                                }else{echo 0;}?>" >
                                            <div id="premi_after_diskon-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Rincian Asuransi <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="asuransi_leader" name="asuransi_leader" placeholder="Rincian Asuransi" readonly="" value="<?php if (isset($q->asuransi_leader)) {
                                                    echo $q->asuransi_leader;
                                                }else{echo 0;}?>" >
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalRincianAsuransi"><i class="  bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input Asuransi</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Asuransi Leader 100% <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="asuransi_leader_id" name="asuransi_leader_id" value="" readonly="" placeholder="" style="display: none;">
                                                <input type="text" class="form-control" id="share_leader" name="share_leader" value="<?php if (isset($q->share)) {
                                                    echo $q->share;
                                                }else{echo 0;}?>" readonly="" placeholder="Share %">
                                                <!-- <input type="text" class="form-control" name="asuransi_leader" id="asuransi_leader" placeholder="Asuransi Leader" readonly> -->
                                            </div>
                                            <div id="asuransi_leader-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Gross Broker <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="gross_broker_rate" name="gross_broker_rate" placeholder="Gross Broker Rate %" readonly="" placeholder="Diskon %" value="<?php if (isset($q->gross_broker_rate)) {
                                                    echo $q->gross_broker_rate;
                                                }else{echo 0;}?>" >
                                                <input type="text" class="form-control" id="gross_broker" name="gross_broker" placeholder="Gross Broker" readonly="" value="<?php if (isset($q->gross_broker)) {
                                                    echo number_format($q->gross_broker);
                                                }else{echo 0;}?>" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Diskon Broker <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="diskon_broker_persen" name="diskon_broker_persen" readonly="" placeholder="Diskon Broker %" value="<?php if (isset($q->diskon_broker_persen)) {
                                                    echo $q->diskon_broker_persen;
                                                }else{echo 0;}?>" >
                                                <input type="text" class="form-control" id="diskon_broker" name="diskon_broker" placeholder="Diskon Broker" readonly=""  value="<?php if (isset($q->diskon_broker)) {
                                                    echo number_format($q->diskon_broker);
                                                }else{echo 0;}?>" >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Premi After Diskon Broker <code>*</code></label>
                                            <input type="text" class="form-control" name="premi_after_diskon_broker" id="premi_after_diskon_broker" placeholder="Premi After Diskon" readonly value="<?php if (isset($q->premi_after_diskon_broker)) {
                                                    echo number_format($q->premi_after_diskon_broker);
                                                }else{echo 0;}?>" >
                                            <div id="premi_after_diskon_broker-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Net Brokerage <code>*</code></label>
                                            <input type="text" class="form-control" name="net_brokerage" id="net_brokerage" placeholder="Net Brokerage" readonly value="<?php if (isset($q->net_brokerage)) {
                                                    echo number_format($q->net_brokerage);
                                                }else{echo 0;}?>" >
                                            <div id="net_brokerage-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Premi To Ceding <code>*</code></label>
                                            <input type="text" class="form-control" name="premi_to_ceding" id="premi_to_ceding" placeholder="Premi After Ceding" readonly value="<?php if (isset($q->premi_to_ceding)) {
                                                    echo number_format($q->premi_to_ceding);
                                                }else{echo 0;}?>" >
                                            <div id="premi_to_ceding-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Installment <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="installment" name="installment" value="" placeholder="Installmment" readonly="">
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalInstallment"><i class=" bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input Installment</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Rate PPN <code>*</code></label>
                                            <input type="text" class="form-control" name="rate_ppn" id="rate_ppn" placeholder="Rate PPN" readonly value="<?php if (!empty($q->rate_ppn)) {
                                                    echo number_format($q->rate_ppn);
                                                }else{echo 0;}?>" >
                                            <div id="rate_ppn-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Tujuan Penerimaan Transper <code>*</code></label>
                                            <div class="input-group mb-3">
                                                <input type="text" class="form-control" id="tujuan_transper" name="tujuan_transper" value="" placeholder="Tujuan Transper" readonly="">
                                                <div class="clearfix">
                                                    <button type="button" class="btn btn-primary btn-label waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#modalTransper"><i class=" bx bx-list-plus label-icon align-middle fs-16 me-2"></i> Input Tujuan Transper</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label class="form-label">Unit Bisnis <code>*</code></label>
                                            <select class="form-select" name="unit_bisnis" id="unit_bisnis">
                                                <option></option>
                                                <?php
                                                    $unitbisnis = $this->db->query("select * from unit_bisnis")->result_array();
                                                    foreach ($unitbisnis as $value) {
                                                        if ($q->jenis_asuransi == $value['id']) {
                                                          echo"<option selected='selected' value='".$value['id']."'>".$value['unit_bisnis']."</option>"; 
                                                        } else {
                                                          echo"<option value='".$value['id']."'>".$value['unit_bisnis']."</option>"; 
                                                        }
                                                    }
                                                ?> 
                                            </select>
                                            <div id="unit_bisnis-err" style="display: none;"><code>* Wajib Diisi</code></div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <input type="text" class="form-control" name="json_data" id="json_data" placeholder="" value="" style="display: none;">
                                        <input type="text" class="form-control" name="json_data_asd" id="json_data_asd" placeholder="" value="" style="display: none;">
                                        <div class="text-left">
                                            <button type="button" onclick="print_invoice(<?=$q->id;?>)" class="btn btn-success btn-label waves-effect waves-light"><i class="bx bxs-file-pdf label-icon align-middle fs-16 me-2"></i>Cetak Invoice</button>
                                            <button type="button" onclick="print_objek_pertanggungan(<?=$q->id;?>)" class="btn btn-danger btn-label waves-effect waves-light"><i class="bx bxs-file-pdf label-icon align-middle fs-16 me-2"></i>Cetak Objek Pertanggungan</button>
                                            <button type="button" onclick="print_kwitansi(<?=$q->id;?>)" class="btn btn-warning btn-label waves-effect waves-light"><i class="bx bxs-file-pdf label-icon align-middle fs-16 me-2"></i>Cetak Kwitansi</button>

                                            <button type="button" onclick="print_installment(<?=$q->id;?>)" class="btn btn-info btn-label waves-effect waves-light"><i class="bx bxs-file-pdf label-icon align-middle fs-16 me-2"></i>Cetak Installment</button>
                                        </div>
                                       <!--  <div class="text-center">
                                            <button type="button" onclick="save()" class="btn btn-primary btn-label waves-effect waves-light"><i class="bx bx-save label-icon align-middle fs-16 me-2"></i>Simpan</button>
                                        </div> -->
                                        
                                    </div>

                                   
                                </div>

                                <div class="modal fade" id="modalTsi" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Input TSI
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="tableTsi" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>No. Polis</th>
                                                        <th>Keterangan</th>
                                                        <th>Awal</th>
                                                        <th>Akhir</th>
                                                        <th>TSI</th>
                                                        <th>Rate Premi</th>
                                                        <th>Premi Gross</th>
                                                        <th>Diskon ASD %</th>
                                                        <th>Diskon ASD</th>
                                                        <th>Premi</th>
                                                        <th>Diskon Broker %</th>
                                                        <th>Diskon Broker</th>
                                                        <th>Biaya Polis</th>
                                                        <th>Biaya Materai</th>
                                                        <th>Biaya Adm Bro</th>
                                                        <th>Jumlah</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php foreach ($tsi as $key => $val) { ?>

                                                        <tr>
                                                            <td>
                                                                <input type="text" name="no_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="no_polis_modal_tsi" value="<?=$val['no_polis'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="keterangan_modal_tsi[]" class="form-control" style="width: 300px;" id="keterangan_modal_tsi" value="<?=$val['nama'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="date" name="tanggal_awal_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_awal_modal_tsi" value="<?=$val['tanggal_awal'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="date" name="tanggal_akhir_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_akhir_modal_tsi" value="<?=$val['tanggal_akhir'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="tsi_modal_tsi[]" class="form-control" style="width: 300px;" id="tsi_modal_tsi" value="<?=number_format($val['tsi']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="rate_premi_modal_tsi[]" class="form-control" style="width: 300px;" id="rate_premi_modal_tsi" value="<?=$val['premi_rate'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text" name="premi_gross_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_gross_modal_tsi" value="<?=number_format($val['premi_gross']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_asd_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_persen_modal_tsi" value="<?=$val['asd_diskon_persen'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_asd_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_modal_tsi" value="<?=number_format($val['asd_diskon']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="premi_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_modal_tsi" value="<?=number_format($val['premi']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_broker_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_persen_modal_tsi" value="<?=$val['broker_diskon_persen'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="diskon_broker_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_modal_tsi" value="<?=number_format($val['broker_diskon']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="biaya_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_polis_modal_tsi" value="<?=number_format($val['by_polis']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="biaya_materai_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_materai_modal_tsi" value="<?=number_format($val['materai']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="biaya_adm_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_adm_modal_tsi" value="<?=number_format($val['adm_broker']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text" name="jumlah_modal_tsi[]" class="form-control" style="width: 300px;" id="jumlah_modal_tsi" value="<?=number_format($val['jumlah']);?>">
                                                            </td>
                                                            
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                                <div class="modal fade" id="modalRincianAsuransi" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Rincian Asuransi
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="tabelRincianAsuransi" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>Nama Asuradur</th>
                                                        <th>JN+Pajak</th>
                                                        <th>Share</th>
                                                        <th>TSI</th>
                                                        <th>Gross Premi</th>
                                                        <th>Diskon</th>
                                                        <th>Premi</th>
                                                        <th>Broker %</th>
                                                        <th>Gross Brokerage</th>
                                                        <th>Hut Premi</th>
                                                        <th>Biaya Polis</th>
                                                        <th>Biaya Materai</th>
                                                        <th>Jumlah Sebelum Pajak</th>
                                                        <th>PPN Asli</th>
                                                        <th>PPN</th>
                                                        <th>PPH23</th>
                                                        <th>Net Premi</th>
                                                        <th>DPP</th>
                                                        <th>Nilai Klaim</th>
                                                        <th>Keterangan Klaim</th>
                                                        
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($asuransi as $key => $asur) { ?>
                                                        <tr>
                                                            <td>
                                                                <select class="form-select" name="nama_asuransi_modal_asuransi[]" id="nama_asuransi_modal_asuransi"  style="width: 300px;">
                                                                    <?php
                                                                    $asu = $this->db->query("select * from asuransi")->result_array();
                                                                    foreach ($asu as $value) {
                                                                        if ($asur['asuransi'] == $value['id']) {
                                                                          echo"<option selected='selected' value='".$value['id']."'>".$value['asuransi']."</option>"; 
                                                                        } else {
                                                                          echo"<option value='".$value['id']."'>".$value['asuransi']."</option>"; 
                                                                        }
                                                                    }
                                                                ?> 
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="share_modal_asuransi" name="share_modal_asuransi[]" value="<?=$asur['share'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="tsi_modal_asuransi" name="tsi_modal_asuransi[]" value="<?=number_format($asur['tsi']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="gross_premi_modal_asuransi" name="gross_premi_modal_asuransi[]" value="<?=number_format($asur['gross_premi']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="diskon_modal_asuransi" name="diskon_modal_asuransi[]" value="<?=number_format($asur['diskon']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="premi_modal_asuransi" name="premi_modal_asuransi[]" value="<?=number_format($asur['premi']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="fee_broker_modal_asuransi" name="fee_broker_modal_asuransi[]" value="<?number_format(=$asur['fee_broker']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="gross_brokerage_modal_asuransi" name="gross_brokerage_modal_asuransi[]" value="<?=number_format($asur['gross_brokerage']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="hut_premi_modal_asuransi" name="hut_premi_modal_asuransi[]" value="<?=number_format($asur['hut_premi']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="biaya_polis_modal_asuransi" name="biaya_polis_modal_asuransi[]" value="<?=number_format($asur['biaya_polis']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="biaya_materai_modal_asuransi" name="biaya_materai_modal_asuransi[]" value="<?=number_format($asur['biaya_materai']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="jumlah_sebelum_pajak_modal_asuransi" name="jumlah_sebelum_pajak_modal_asuransi[]" value="<?=number_format($asur['jumlah_sebelum_pajak']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="ppn_asli_modal_asuransi" name="ppn_asli_modal_asuransi[]" value="<?=number_format($asur['ppn_asli']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="ppn_modal_asuransi" name="ppn_modal_asuransi[]" value="<?=number_format($asur['ppn']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="pph_23_modal_asuransi" name="pph_23_modal_asuransi[]" value="<?=number_format($asur['pph_23']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="net_premi_modal_asuransi" name="net_premi_modal_asuransi[]" value="<?=number_format($asur['net_premi']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="jn_pajak_modal_asuransi" name="jn_pajak_modal_asuransi[]" value="<?=number_format($asur['jn_pajak']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="dpp_modal_asuransi" name="dpp_modal_asuransi[]" value="<?=number_format($asur['dpp']);?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="nilai_klaim_modal_asuransi" name="nilai_klaim_modal_asuransi[]" value="<?=$asur['nilai_klaim'];?>">
                                                            </td>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="keterangan_klaim_modal_asuransi" name="keterangan_klaim_modal_asuransi[]" value="<?=$asur['keterangan_klaim'];?>">
                                                            </td>
                                                            
                                                        </tr>

                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->number_format(
                                </div>

                                <div class="modal fade" id="modalInstallment" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Installment
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="tableInstallment" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>Installment</th>
                                                        <th>Tgl. Kwitansi</th>
                                                        <th>Tgl. Jatuh Tempo</th>
                                                        <th>Gross Premi</th>
                                                        <th>Diskon Asd</th>
                                                        <th>Diskon Broker</th>
                                                        <th>Biaya Polis</th>
                                                        <th>Biaya Materai</th>
                                                        <th>Biaya Admin Broker</th>
                                                        <th>Jumlah Setelah Biaya Polis dan Materai + Admin</th>
                                                        <th>Brokerage</th>
                                                        
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php foreach ($installment as $key => $valueInstallment) { ?>

                                                        <tr>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=$valueInstallment['installment'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=$valueInstallment['tanggal_kwitansi'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=$valueInstallment['tanggal_jatuh_tempo'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['gross_premi']);?>">
                                                            </td>

                                                             <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['diskon_asd']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['diskon_broker']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['biaya_polis']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['biaya_materai']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['biaya_admin_broker']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['jumlah_setelah_admin_materai']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInstallment['brokerage']);?>">
                                                            </td>

                                                        </tr>

                                                    <?php } ?>
                                                    </tbody>
                                                </table>

                                                <table id="tableInstallment" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap;">
                                                    <thead>
                                                    <tr>
                                                        <th>Installment</th>
                                                        <th>ASD Nama</th>
                                                        <th>Tanggal Tempo</th>
                                                        <th>Gross Premi</th>
                                                        <th>Diskon ASD</th>
                                                        <th>Premi</th>
                                                        <th>Brokerage</th>
                                                        <th>Biaya Polis</th>
                                                        <th>Biaya Materai</th>
                                                        <th>Tax Index</th>
                                                        <th>Dpp</th>
                                                        <th>PPN</th>
                                                        <th>PPH 23</th>
                                                        <th>Iuran OJK</th>
                                                        <th>Jumlah</th>
                                                        
                                                        
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                        <?php foreach ($installment_detail as $key => $valueInsDetail) { ?>

                                                        <tr>
                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=$valueInsDetail['installment'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=$valueInsDetail['asuransina'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=$valueInsDetail['tanggal_tempo'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['gross_premi']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['diskon_asd']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['premi']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['brokerage']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['biaya_polis']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['biaya_materai']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=$valueInsDetail['tax_index'];?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['dpp']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['ppn']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['pph_23']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['iuran_ojk']);?>">
                                                            </td>

                                                            <td>
                                                                <input type="text"  class="form-control" style="width: 300px;" id="" name="" value="<?=number_format($valueInsDetail['jumlah']);?>">
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    </tbody>
                                                </table>


                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                                <div class="modal fade" id="modalTransper" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">Input Tujuan Transfer
                                                </h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                    aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <table id="tableTsi" class="table table-striped" style="display: block; overflow-x: auto; white-space: nowrap; width: 100%;">
                                                    <thead>
                                                    <tr>
                                                        <th>Bank</th>
                                                        <th>No. Rekening</th>
                                                        <th>Atas Nama</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 

                                                        $jh = $this->db->query("select a.* from rekening a join produksi_tujuanTransper b on a.id = b.id_bank where b.id_produksi = $q->id")->result_array();

                                                        foreach ($jh as $key => $value) { ?>
                                                        <tr>

                                                            <td>
                                                                <input class="form-control" type="text" name="modal_namaBank" id="modal_namaBank" value="<?=$value['namaBank'];?>" style="width: 300px;" readonly>
                                                            </td>

                                                            <td>
                                                                <input class="form-control" type="text" name="modal_namaBank" id="modal_namaBank" value="<?=$value['noRekening'];?>" style="width: 300px;" readonly>
                                                            </td>

                                                            <td>
                                                                <input class="form-control" type="text" name="modal_namaBank" id="modal_namaBank" value="<?=$value['atasNama'];?>" style="width: 300px;" readonly>
                                                            </td>

                                                        </tr>

                                                    <?php } ?>
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>



</div> <!-- container-fluid -->

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- <script type="text/javascript" src="//cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script> -->


<script type="text/javascript">

    function formatRupiah(angka, prefix)
    {
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split    = number_string.split(','),
            sisa     = split[0].length % 3,
            rupiah     = split[0].substr(0, sisa),
            ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
        
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? rupiah : '');
    }

    function saveTsi(){
        let arrTsi = {
            data : [],
            totalData : {
                sum_tsi : 0,
                sum_premi_gross : 0,
                sum_diskon_asd : 0,
                sum_premi : 0,
                sum_diskon_broker : 0,
                sum_biaya_polis : 0,
                sum_biaya_materai : 0,
                sum_biaya_adm : 0,
                sum_total_tsi : 0,
                sum_rate_premi : 0,
                sum_disc_bro : 0,
                sum_disc_asd : 0,
                count_tsi : 0
            }
        };
        let sum_tsi = 0;
        let sum_premi_gross = 0;
        let sum_diskon_asd = 0;
        let sum_premi = 0;
        let sum_diskon_broker = 0;
        let sum_biaya_polis = 0;
        let sum_biaya_materai = 0;
        let sum_biaya_adm = 0;
        let sum_total_tsi = 0;
        let sum_rate_premi = 0;
        let sum_disc_bro = 0;
        let sum_disc_asd = 0;
        let no_polis = '';
        let tanggal_awal = '';
        let tanggal_akhir = '';
        let total_count = 0;

        $('#tableTsi > tbody  > tr').each(function(index, tr) { 
            // console.log(index);
            // console.log(tr);
            var item = $(this, tr);
            var arrItem = {
                tsi : 0,
                premi_gross : 0,
                diskon_asd : 0,
                premi : 0,
                diskon_broker : 0,
                biaya_polis : 0,
                biaya_materai : 0,
                biaya_adm : 0,
                rate_premi : 0,
                disc_bro : 0,
                disc_asd : 0,
                total_tsi : 0
            };

            if(index == 0){
                no_polis = item.find("input#no_polis_modal_tsi", tr).val();
                tanggal_awal = item.find("input#tanggal_awal_modal_tsi", tr).val();
                tanggal_akhir = item.find("input#tanggal_akhir_modal_tsi", tr).val();
            }

            var tsi = item.find("input#tsi_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(tsi);
            var premi_gross = item.find("input#premi_gross_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(premi_gross);
            var diskon_asd = item.find("input#diskon_asd_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(diskon_asd);
            var premi = item.find("input#premi_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(premi);
            var diskon_broker = item.find("input#diskon_broker_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(diskon_broker);
            var biaya_polis = item.find("input#biaya_polis_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(biaya_polis);
            var biaya_materai = item.find("input#biaya_materai_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(biaya_materai);
            var biaya_adm = item.find("input#biaya_adm_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(biaya_adm);
            var total_tsi = item.find("input#jumlah_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(total_tsi);
            var rate_premi = item.find("input#rate_premi_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            var disc_bro = item.find("input#diskon_broker_persen_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            var disc_asd = item.find("input#diskon_asd_persen_modal_tsi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(total_tsi);

            sum_tsi = parseFloat(tsi) + sum_tsi;
            sum_premi_gross = parseFloat(premi_gross) + sum_premi_gross;
            sum_diskon_asd = parseFloat(diskon_asd) + sum_diskon_asd;
            sum_premi = parseFloat(premi) + sum_premi;
            sum_diskon_broker = parseFloat(diskon_broker) + sum_diskon_broker;
            sum_biaya_polis = parseFloat(biaya_polis) + sum_biaya_polis;
            sum_biaya_materai = parseFloat(biaya_materai) + sum_biaya_materai;
            sum_biaya_adm = parseFloat(biaya_adm) + sum_biaya_adm;
            sum_total_tsi = parseFloat(total_tsi) + sum_total_tsi;
            sum_rate_premi = parseFloat(total_tsi) + sum_total_tsi;
            sum_disc_bro = parseFloat(total_tsi) + sum_total_tsi;
            sum_disc_asd = parseFloat(total_tsi) + sum_total_tsi;

            arrItem.tsi = parseFloat(tsi);
            arrItem.premi_gross = parseFloat(premi_gross);
            arrItem.diskon_asd = parseFloat(diskon_asd);
            arrItem.premi = parseFloat(premi);
            arrItem.diskon_broker = parseFloat(diskon_broker);
            arrItem.biaya_polis = parseFloat(biaya_polis);
            arrItem.biaya_materai = parseFloat(biaya_materai);
            arrItem.biaya_adm = parseFloat(biaya_adm);
            arrItem.total_tsi = parseFloat(total_tsi);
            arrItem.rate_premi = parseFloat(rate_premi);
            arrItem.disc_bro = parseFloat(disc_bro);
            arrItem.disc_asd = parseFloat(disc_asd);

            arrTsi.data.push(arrItem);
            total_count = total_count + 1;
        });

        arrTsi.totalData.sum_tsi = sum_tsi;
        arrTsi.totalData.sum_premi_gross = sum_premi_gross;
        arrTsi.totalData.sum_diskon_asd = sum_diskon_asd;
        arrTsi.totalData.sum_premi = sum_premi;
        arrTsi.totalData.sum_diskon_broker = sum_diskon_broker;
        arrTsi.totalData.sum_biaya_polis = sum_biaya_polis;
        arrTsi.totalData.sum_biaya_materai = sum_biaya_materai;
        arrTsi.totalData.sum_biaya_adm = sum_biaya_adm;
        arrTsi.totalData.sum_total_tsi = sum_total_tsi;
        arrTsi.totalData.sum_rate_premi = sum_rate_premi;
        arrTsi.totalData.sum_disc_bro = sum_disc_bro;
        arrTsi.totalData.sum_disc_asd = sum_disc_asd;
        arrTsi.totalData.count_tsi = total_count;

        console.log(arrTsi);

        var premi_before_adm = arrTsi.totalData.sum_premi + arrTsi.totalData.sum_biaya_polis + arrTsi.totalData.sum_biaya_materai;
        var premi_adm = arrTsi.totalData.sum_premi + arrTsi.totalData.sum_biaya_polis + arrTsi.totalData.sum_biaya_materai + arrTsi.totalData.sum_biaya_adm;
        var premi_disc_bro = arrTsi.totalData.sum_premi + arrTsi.totalData.sum_biaya_polis + arrTsi.totalData.sum_biaya_materai + arrTsi.totalData.sum_diskon_broker;

        var json = JSON.stringify(arrTsi);
        console.log(json);
        $('#no_polis').val(no_polis);
        $('#json_data').val(json);
        $('#jangka_waktu_dari').val(tanggal_awal);
        $('#jangka_waktu_sampai').val(tanggal_akhir);
        $('#gross_premi_rate').val(arrTsi.data[0].rate_premi + ' %');
        $('#diskon_asd_persen').val(arrTsi.data[0].disc_asd + ' %');
        $('#diskon_broker_persen').val(arrTsi.data[0].disc_bro + ' %');
        $('#diskon_broker').val(formatRupiah(arrTsi.totalData.sum_diskon_broker.toString(),'Rp. '));
        $('#gross_premi').val(formatRupiah(arrTsi.totalData.sum_premi_gross.toString(),'Rp. '));
        $('#tsi').val(formatRupiah(arrTsi.totalData.sum_tsi.toString(),'Rp. '));
        $('#diskon_asd').val(formatRupiah(arrTsi.totalData.sum_diskon_asd.toString(),'Rp. '));
        $('#materai').val(formatRupiah(arrTsi.totalData.sum_biaya_materai.toString(),'Rp. '));
        $('#biaya_polis').val(formatRupiah(arrTsi.totalData.sum_biaya_polis.toString(),'Rp. '));
        $('#biaya_admin_broker').val(formatRupiah(arrTsi.totalData.sum_biaya_adm.toString(),'Rp. '));
        $('#premi_after_diskon').val(formatRupiah(arrTsi.totalData.sum_total_tsi.toString(),'Rp. '));
        $('#jumlah_sebelum_admin_broker').val(formatRupiah(premi_before_adm.toString(),'Rp. '));
        $('#jumlah_setelah_admin_broker').val(formatRupiah(premi_adm.toString(),'Rp. '));
        $('#premi_after_diskon_broker').val(formatRupiah(premi_disc_bro.toString(),'Rp. '));

        $('#modalTsi').modal('hide');
    }

    function saveAsuransi(){
        let arrAsd = {
            data : [],
            totalData : {
                sum_tsi : 0,
                sum_share : 0,
                sum_premi_gross : 0,
                sum_diskon : 0,
                sum_premi : 0,
                sum_rate_broker : 0,
                sum_gross_brokerage : 0,
                sum_hut_premi : 0,
                sum_biaya_materai : 0,
                sum_biaya_polis : 0,
                sum_sebelum_pajak : 0,
                sum_ppn_asli : 0,
                sum_ppn : 0,
                sum_pph : 0,
                sum_net_premi : 0,
                sum_pajak : 0,
                sum_dpp : 0,
                sum_nilai_klaim : 0,
                count_asd : 0
            }
        };
        let sum_tsi = 0;
        let sum_share = 0;
        let sum_premi_gross = 0;
        let sum_diskon = 0;
        let sum_premi = 0;
        let sum_rate_broker = 0;
        let sum_gross_brokerage = 0;
        let sum_hut_premi = 0;
        let sum_biaya_materai = 0;
        let sum_biaya_polis = 0;
        let sum_sebelum_pajak = 0;
        let sum_ppn_asli = 0;
        let sum_ppn = 0;
        let sum_pph = 0;
        let sum_net_premi = 0;
        let sum_pajak = 0;
        let sum_dpp = 0;
        let sum_nilai_klaim = 0;
        let tanggal_awal = '';
        let tanggal_akhir = '';
        let total_count = 0;
        let rate_ppn = 0;
        let asuransi_leader = '';

        $('#tabelRincianAsuransi > tbody  > tr').each(function(index, tr) { 
            // console.log(index);
            // console.log(tr);
            var item = $(this, tr);
            var arrItem = {
                asuransi : '',
                id_asuransi : '',
                tsi : 0,
                share : 0,
                premi_gross : 0,
                diskon : 0,
                premi : 0,
                rate_broker : 0,
                gross_broker : 0,
                biaya_materai : 0,
                biaya_polis : 0,
                sebelum_pajak : 0,
                ppn_asli : 0,
                ppn : 0,
                pph : 0,
                net_premi : 0,
                hut_premi : 0,
                pajak : 0,
                dpp : 0,
                nilai_klaim : 0,
                keterangan : ''
            };

            if(index == 0){
                asuransi_leader = item.find("#nama_asuransi_modal_asuransi :selected", tr).text();
                asuransi_id = item.find("#nama_asuransi_modal_asuransi", tr).val();
                rate_ppn = item.find("#ppn_asli_modal_asuransi", tr).val();
            }



            var asuransi = item.find("#nama_asuransi_modal_asuransi :selected", tr).text();
            var id_asuransi = item.find("#nama_asuransi_modal_asuransi", tr).val();
            var share = item.find("input#share_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var tsi = item.find("input#tsi_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(tsi);
            var premi_gross = item.find("input#gross_premi_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(premi_gross);
            var diskon = item.find("input#diskon_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(diskon_asd);
            var premi = item.find("input#premi_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(premi);
            var rate_broker = item.find("input#fee_broker_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(diskon_broker);
            var gross_broker = item.find("input#gross_brokerage_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(biaya_polis);
            var hut_premi = item.find("input#hut_premi_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(biaya_materai);
            var biaya_polis = item.find("input#biaya_polis_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(biaya_adm);
            var biaya_materai = item.find("input#biaya_materai_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(total_tsi);
            var sebelum_pajak = item.find("input#jumlah_sebelum_pajak_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var ppn_asli = item.find("input#ppn_asli_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var ppn = item.find("input#ppn_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var net_premi = item.find("input#net_premi_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var pph = item.find("input#pph_23_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var pajak = item.find("input#jn_pajak_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var dpp = item.find("input#dpp_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var nilai_klaim = item.find("input#nilai_klaim_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            var keterangan = item.find("input#keterangan_klaim_modal_asuransi", tr).val().replace(/\./g,"").replace(",",".");
            // console.log(total_tsi);

            sum_tsi = parseFloat(tsi) + sum_tsi;
            sum_share = parseFloat(share) + sum_share;
            sum_premi_gross = parseFloat(premi_gross) + sum_premi_gross;
            sum_diskon = parseFloat(diskon) + sum_diskon;
            sum_premi = parseFloat(premi) + sum_premi;
            sum_rate_broker = parseFloat(rate_broker) + sum_rate_broker;
            sum_gross_brokerage = parseFloat(gross_broker) + sum_gross_brokerage;
            sum_hut_premi = parseFloat(hut_premi) + sum_hut_premi;
            sum_biaya_materai = parseFloat(biaya_materai) + sum_biaya_materai;
            sum_biaya_polis = parseFloat(biaya_polis) + sum_biaya_polis;
            sum_sebelum_pajak = parseFloat(sebelum_pajak) + sum_sebelum_pajak;
            sum_ppn_asli = parseFloat(ppn_asli) + sum_ppn_asli;
            sum_ppn = parseFloat(ppn) + sum_ppn;
            sum_pph = parseFloat(pph) + sum_pph;
            sum_net_premi = parseFloat(net_premi) + sum_net_premi;
            sum_pajak = parseFloat(pajak) + sum_pajak;
            sum_dpp = parseFloat(dpp) + sum_dpp;
            sum_nilai_klaim = parseFloat(nilai_klaim) + sum_nilai_klaim;

            arrItem.asuransi = asuransi;
            arrItem.id_asuransi = id_asuransi;
            arrItem.tsi = parseFloat(tsi);
            arrItem.share = parseFloat(share);
            arrItem.premi_gross = parseFloat(premi_gross);
            arrItem.diskon = parseFloat(diskon);
            arrItem.premi = parseFloat(premi);
            arrItem.rate_broker = parseFloat(rate_broker);
            arrItem.gross_broker = parseFloat(gross_broker);
            arrItem.hut_premi = parseFloat(hut_premi);
            arrItem.biaya_polis = parseFloat(biaya_polis);
            arrItem.biaya_materai = parseFloat(biaya_materai);
            arrItem.sebelum_pajak = parseFloat(sebelum_pajak);
            arrItem.ppn_asli = parseFloat(ppn_asli);
            arrItem.ppn = parseFloat(ppn);
            arrItem.net_premi = parseFloat(net_premi);
            arrItem.pph = parseFloat(pph);
            arrItem.pajak = parseFloat(pajak);
            arrItem.dpp = parseFloat(dpp);
            arrItem.nilai_klaim = parseFloat(nilai_klaim);
            arrItem.keterangan = parseFloat(keterangan);

            arrAsd.data.push(arrItem);
            total_count = total_count + 1;
        });

        arrAsd.totalData.sum_tsi = sum_tsi;
        arrAsd.totalData.sum_share = sum_share;
        arrAsd.totalData.sum_premi_gross = sum_premi_gross;
        arrAsd.totalData.sum_premi = sum_diskon;
        arrAsd.totalData.sum_rate_broker = sum_rate_broker;
        arrAsd.totalData.sum_gross_brokerage = sum_gross_brokerage;
        arrAsd.totalData.sum_hut_premi = sum_hut_premi;
        arrAsd.totalData.sum_biaya_materai = sum_biaya_materai;
        arrAsd.totalData.sum_biaya_polis = sum_biaya_polis;
        arrAsd.totalData.sum_sebelum_pajak = sum_sebelum_pajak;
        arrAsd.totalData.sum_ppn_asli = sum_ppn_asli;
        arrAsd.totalData.sum_ppn = sum_ppn;
        arrAsd.totalData.sum_pph = sum_pph;
        arrAsd.totalData.sum_net_premi = sum_net_premi;
        arrAsd.totalData.sum_pajak = sum_pajak;
        arrAsd.totalData.sum_dpp = sum_dpp;
        arrAsd.totalData.sum_nilai_klaim = sum_nilai_klaim;
        arrAsd.totalData.count_asd = total_count;
        arrAsd.totalData.sum_diskon = sum_diskon;

        console.log(arrAsd);

        arrAsd.data.sort((a, b) => parseFloat(b.share) - parseFloat(a.share));

    
        var json = JSON.stringify(arrAsd);
        console.log(json);
        $('#asuransi').val(arrAsd.data[0].asuransi);
        $('#json_data_asd').val(json);
        $('#asuransi_leader').val(arrAsd.data[0].asuransi);
        $('#share_leader').val(arrAsd.data[0].share + ' %');
        $('#asuransi_leader_id').val(arrAsd.data[0].id_asuransi);
        $('#gross_broker_rate').val(arrAsd.data[0].rate_broker + ' %');
        $('#rate_ppn').val(formatRupiah(arrAsd.totalData.sum_ppn.toString(),'Rp. '));
        $('#gross_broker').val(formatRupiah(arrAsd.totalData.sum_gross_brokerage.toString(),'Rp. '));
        $('#net_brokerage').val(formatRupiah(arrAsd.totalData.sum_gross_brokerage.toString(),'Rp. '));
        $('#premi_to_ceding').val(formatRupiah(arrAsd.totalData.sum_net_premi.toString(),'Rp. '));

        $('#modalRincianAsuransi').modal('hide');
    }

    function saveInstallment()
    {
        warning('warning','this function under development');
        $('#modalInstallment').modal('hide');
    }

    function saveToform()
    {
        var checklistData = [];
        var checklistId = [];
        
        var sumTsi = 0;
        $("#buttonTsi").each(function (){
            var tr = $(this).parent().parent()[0];
            var rowIndex = $("#tableTsi").dataTable().fnGetPosition(tr);
            var tsina = data[5].replace(/\,/g,'');
           
            // var datas = {}
            // var datasId = {}

            console.log(tsina);

            datas.id = data[1];
            // datasId.id = data[1];
            datas.tsi = tsina;

            checklistData.push(datas);
            // checklistId.push(datasId);
        });
        // console.log(checklistData);

        for (var i = 0; i < checklistData.length; i++) {
             const needle = checklistData[i];

             sumTsi = sumTsi + parseFloat(needle.gross_kontribusi);
         }
         var sumTsi = parseFloat(sumTsi) * 0.85;

        $("#tsi").val(parseFloat(sumUtang.toFixed(0)).toLocaleString());

        // $("#tampung").val("");
        // $("#tampung").val(JSON.stringify(checklistData));
        // $("#tampung_id").val(JSON.stringify(checklistId));

     }

    function saveInputTsi()
    {
        $('#no_polis').val($('#no_polis_modal_tsi').val());
        $('#tsi').val($('#tsi_modal_tsi').val());
        $('#jangka_waktu_dari').val($('#tanggal_awal_modal_tsi').val());
        $('#jangka_waktu_sampai').val($('#tanggal_akhir_modal_tsi').val());
        $('#keterangan').val($('#keterangan_modal_tsi').val());
        $('#biaya_polis').val($('#biaya_polis_modal_tsi').val());
        $('#materai').val($('#materai_modal_tsi').val());
        $('#biaya_polis').val($('#biaya_polis_modal_tsi').val());

        $('#modalTsi').modal('hide');
    }

    function addRowTsi()
    {
        markup = '<tr> <td><button type="button" class="btn btn-danger btn-sm" onclick="removeRowTsi()"><i class=" las la-minus"></i></button></td><td><input type="text" name="no_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="no_polis_modal_tsi"></td><td><input type="text" name="keterangan_modal_tsi[]" class="form-control" style="width: 300px;" id="keterangan_modal_tsi"></td> <td><input type="date" name="tanggal_awal_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_awal_modal_tsi"></td><td><input type="date" name="tanggal_akhir_modal_tsi[]" class="form-control" style="width: 300px;" id="tanggal_akhir_modal_tsi"></td><td><input type="text" name="tsi_modal_tsi[]" class="form-control" style="width: 300px;" id="tsi_modal_tsi"></td><td><input type="text" name="rate_premi_modal_tsi[]" class="form-control" style="width: 300px;" id="rate_premi_modal_tsi"></td><td><input type="text" name="premi_gross_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_gross_modal_tsi"> </td> <td><input type="text" name="diskon_asd_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_persen_modal_tsi"> </td><td><input type="text" name="diskon_asd_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_asd_modal_tsi"> </td><td><input type="text" name="premi_modal_tsi[]" class="form-control" style="width: 300px;" id="premi_modal_tsi"> </td><td><input type="text" name="diskon_broker_persen_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_persen_modal_tsi"></td> <td> <input type="text" name="diskon_broker_modal_tsi[]" class="form-control" style="width: 300px;" id="diskon_broker_modal_tsi">  </td> <td><input type="text" name="biaya_polis_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_polis_modal_tsi"></td> <td><input type="text" name="biaya_materai_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_materai_modal_tsi"></td> <td>  <input type="text" name="biaya_adm_modal_tsi[]" class="form-control" style="width: 300px;" id="biaya_adm_modal_tsi"> </td>  <td><input type="text" name="jumlah_modal_tsi[]" class="form-control" style="width: 300px;" id="jumlah_modal_tsi"> </td></tr>'; 
                tableBody = $("#tableTsi"); 
                tableBody.append(markup); 
    }

    

    function addRowRincianAsuransi()
    {
        rincians = '<tr> <td>  <button type="button" class="btn btn-danger btn-sm" onclick="removeRowRincianAsuransi()"><i class="las la-minus"></i></button>  </td> <td><select class="form-select" name="nama_asuransi_modal_asuransi[]" id="nama_asuransi_modal_asuransi"  style="width: 300px;"> <option></option> <option value="1">TAKAFUL UMUM</option> <option value="2">BRINS</option> </select>  </td> <td> <input type="text"  class="form-control" style="width: 300px;" id="share_modal_asuransi" name="share_modal_asuransi[]"> </td>  <td>  <input type="text"  class="form-control" style="width: 300px;" id="tsi_modal_asuransi" name="tsi_modal_asuransi[]"> </td> <td>  <input type="text"  class="form-control" style="width: 300px;" id="gross_premi_modal_asuransi" name="gross_premi_modal_asuransi[]"> </td> <td>  <input type="text"  class="form-control" style="width: 300px;" id="diskon_modal_asuransi" name="diskon_modal_asuransi[]"> </td> <td> <input type="text"  class="form-control" style="width: 300px;" id="premi_modal_asuransi" name="premi_modal_asuransi[]"> </td>  <td> <input type="text"  class="form-control" style="width: 300px;" id="fee_broker_modal_asuransi" name="fee_broker_modal_asuransi[]">  </td>  <td> <input type="text"  class="form-control" style="width: 300px;" id="gross_brokerage_modal_asuransi" name="gross_brokerage_modal_asuransi[]"> </td>  <td>  <input type="text"  class="form-control" style="width: 300px;" id="hut_premi_modal_asuransi" name="hut_premi_modal_asuransi[]">  </td> <td> <input type="text"  class="form-control" style="width: 300px;" id="biaya_polis_modal_asuransi" name="biaya_polis_modal_asuransi[]">  </td>  <td><input type="text" name="biaya_materai_modal_asuransi[]" class="form-control" style="width: 300px;" id="biaya_materai_modal_asuransi" name="biaya_materai_modal_asuransi[]"></td> <td> <input type="text"  class="form-control" style="width: 300px;" id="jumlah_sebelum_pajak_modal_asuransi" name="jumlah_sebelum_pajak_modal_asuransi[]">  </td> <td>  <input type="text"  class="form-control" style="width: 300px;" id="ppn_asli_modal_asuransi" name="ppn_asli_modal_asuransi[]">  </td> <td>  <input type="text"  class="form-control" style="width: 300px;" id="ppn_modal_asuransi" name="ppn_modal_asuransi[]">  </td><td><input type="text"  class="form-control" style="width: 300px;" id="pph_23_modal_asuransi" name="pph_23_modal_asuransi[]"> </td>  <td> <input type="text"  class="form-control" style="width: 300px;" id="net_premi_modal_asuransi" name="net_premi_modal_asuransi[]"> </td><td><input type="text"  class="form-control" style="width: 300px;" id="jn_pajak_modal_asuransi" name="jn_pajak_modal_asuransi[]"></td><td><input type="text"  class="form-control" style="width: 300px;" id="dpp_modal_asuransi" name="dpp_modal_asuransi[]"></td><td><input type="text"  class="form-control" style="width: 300px;" id="nilai_klaim_modal_asuransi" name="nilai_klaim_modal_asuransi[]"> </td> <td> <input type="text"  class="form-control" style="width: 300px;" id="keterangan_klaim_modal_asuransi" name="keterangan_klaim_modal_asuransi[]"> </td></tr>'; 
                tableBody = $("#tabelRincianAsuransi"); 
                tableBody.append(rincians); 
    }

    function removeRowTsi()
    {
        $('#tableTsi tbody tr:last-child').remove(); 
    }

    function removeRowRincianAsuransi()
    {
        $('#tabelRincianAsuransi tbody tr:last-child').remove(); 
    }

    function save()
    {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('produksi/save')?>',
            data: $("#form_produksi").serialize(),
            beforeSend: function(){
                Swal.fire({
                    title: 'Mohon Tunggu ...',
                    html: 'Prosess menyimpan Data',// add html attribute if you want or remove
                    allowOutsideClick : false,
                    showConfirmButton : false,
                    didOpen: () => {
                        Swal.showLoading()
                    }
                });
            },
            success: function(response) {
                swal.close();
                

                if (response.status == 'success') {
                    message('success', 'Berhasil disimpan');
                    window.setTimeout(function(){ 
                        window.location.href = '<?=base_url('produksi/index')?>';
                    } ,2000);
                } else {
                    warning('warning','Oopss... Data gagal disimpan, periksa kembali inputan anda!');
                }
                
            }, error: function(){
                 warning('error', 'Gagal disimpan');
            }
        });
    }

    function print_installment(id)
    {
        var url = "<?php echo base_url('produksi/print_installment/');?>" + id;
        window.open(url);
    }

    function print_invoice(id)
    {
        var url = "<?php echo base_url('produksi/print_invoice/');?>" + id;
        window.open(url);
    }

    function print_lampiran(id)
    {
        var url = "<?php echo base_url('produksi/print_lampiran/');?>" + id;
        window.open(url);
    }

    function print_objek_pertanggungan(id)
    {
        var url = "<?php echo base_url('produksi/print_objek_pertanggungan/');?>" + id;
        window.open(url);
    }

    function print_kwitansi(id)
    {
        var url = "<?php echo base_url('produksi/print_kwitansi/');?>" + id;
        window.open(url);
    }

</script>









