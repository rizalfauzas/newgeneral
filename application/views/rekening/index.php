<div class="container-fluid">

    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                <h4 class="mb-sm-0"><?=$title?></h4>

                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Penutupan Asuransi</a></li>
                        <li class="breadcrumb-item active"><?=$title?></li>
                    </ol>
                </div>

            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row" >
        <div class="col-xxl-12">
            <!-- <h5 class="mb-3">Border Top Nav</h5> -->
            <div class="card">
                <div class="card-body" >
                   
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    
                                    <ul class="nav nav-tabs nav-border-top nav-border-top-primary mb-3" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-bs-toggle="tab" href="#nav-border-top-home" role="tab" aria-selected="true">
                                                Data
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-bs-toggle="tab" href="#nav-border-top-profile" role="tab" aria-selected="false">
                                                Tambah
                                            </a>
                                        </li>
                                        
                                    </ul>

                                    <div class="tab-content text-muted">
                                        <div class="tab-pane active" id="nav-border-top-home" role="tabpanel">
                                            <div class="d-flex">
                                                <div class="flex-grow-1 ms-2">
                                                    <div class="table-responsive">
                                                        <?= $this->session->flashdata('message');?>
                                                        

                                                        <table id="tablena" style="width: 100%;" class="table table-hover align-middle table-nowrap mb-0">
                                                            <thead>
                                                                <tr>
                                                                    <th >No</th>
                                                                    <th >Aksi</th>
                                                                    <th >Bank</th>
                                                                    <th >No. Rekening</th>
                                                                    <th >Atas Nama</th>
                                                                    <th >Status</th>

                                                                </tr>
                                                            </thead>

                                                            <tbody>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane" id="nav-border-top-profile" role="tabpanel">
                                            <div class="d-flex">
                                                
                                                <div class="flex-grow-1 ms-2">
                                                    <br>
                                                   <form action="<?=base_url('rekening/save')?>" id="formna" method="post">
                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="nameInput" class="form-label">Bank</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" name="namaBank" id="namaBank" placeholder="Masukan Nama Bank" required>
                                                                <div id="namaBank-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="dateInput" class="form-label">No. Rekening</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" id="noRekening" name="noRekening" placeholder="Masukan No. Rekening">
                                                                <div id="noRekening-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="timeInput" class="form-label">Atas Nama</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <input type="text" class="form-control" id="atasNama" name="atasNama" placeholder="Masukan Atas Nama">
                                                                <div id="atasNama-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="leaveemails" class="form-label">Status</label>
                                                            </div>
                                                            <div class="col-lg-6">
                                                               <select class="js-example-basic-single" name="status" id="status" >
                                                                    <option></option>
                                                                    <?php
                                                                    $status = $this->db->query("select * from status")->result_array();
                                                                      foreach ($status as $value) {
                                                                            echo"<option value='".$value['id']."'>".$value['status']."</option>"; 
                                                                        }
                                                                    ?> 
                                                                </select>
                                                                <div id="status-err" style="display: none;"><i style="color: red;">* Wajib Diisi</i></div>
                                                            </div>
                                                        </div>

                                                        <div class="row mb-3">
                                                            <div class="col-lg-3">
                                                                <label for="leaveemails" class="form-label"></label>
                                                            </div>
                                                            <div class="col-lg-9">
                                                                <button type="reset" class="btn btn-warning btn-label waves-effect waves-light" >
                                                                    <i class=" ri-refresh-line label-icon align-middle fs-16 me-2">
                                                                    </i> Refresh
                                                                </button>
                                                                <button type="button" onclick="save();" class="btn btn-primary btn-label waves-effect waves-light" >
                                                                    <i class=" bx bx-save label-icon align-middle fs-16 me-2">
                                                                    </i> Simpan
                                                                </button>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div><!-- end card-body -->
                            </div>
                            
                        </div><!--end col-->
                    </div>
                </div><!-- end card-body -->
            </div>
        </div>
        <!--end col-->

    </div>


</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>


<script type="text/javascript">

    $(document).ready( function(){
        $("#tablena").DataTable({
            "processing": true,
            "serverSide": true,
            "order": [],
            "ajax": {
                "url": '<?=base_url('rekening/getData')?>',
                "type": "POST",
                "data": function ( data ) {
                    data.bulan          = $('#bulan').val();
                    data.tahun          = $('#tahun').val();
                }
            },
            "columnDefs": [{
                "target": [-1],
                "orderable": false
            }],
        });

        $('#btn-filter').click(function(){ 
            reloadTable();
            // $('#btn-generate').show();
            // $('#btn-generate-rekap').show();
            $('#vr-generate').show();
        });
        $('#btn-reset').click(function(){ 
            $('#form-filter')[0].reset();
            reloadTable();
        });   
    });

    function save()
    {
        let err = 0;

        var namaBank = $("#namaBank").val();
        if (namaBank == '') {
            $("#namaBank-err").show();
            err = 1;
        } else{
            $('#namaBank-err').hide();
        }

        var noRekening = $("#noRekening").val();
        if (noRekening == '') {
            $("#noRekening-err").show();
            err = 1;
        } else{
            $('#noRekening-err').hide();
        }

        var atasNama = $("#atasNama").val();
        if (atasNama == '') {
            $("#atasNama-err").show();
            err = 1;
        } else{
            $('#atasNama-err').hide();
        }

        var status = $("#status").val();
        if (status == '') {
            $("#status-err").show();
            err = 1;
        } else{
            $('#status-err').hide();
        }

        if(err == 1){
            warning('warning','Mohon Isi data yg belum lengkap');
            return false;
        }

        $.ajax({
            type: "POST",
            url: '<?=base_url('rekening/save')?>',
            data: $("#formna").serialize(),
            dataType: "JSON",
            success: function(response) {
                if (response.status == 'success') {
                    reloadTable();
                    message('success','Berhasil Disimpan');
                } else {
                    warning('warning','Gagal Disimpan, periksa kembali inputan Anda!');
                }
                
            }, error: function(response){
                warning('warning',reponse.message);
            },
        })
    }

   function deleteRek(id,type)
    {
        Swal.fire({
          title: 'Anda yakin akan menghapus Data Rekening ini?',
          showDenyButton: true,
          showCancelButton: true,
          confirmButtonText: 'Hapus!',
          denyButtonText: `Tidak`,
        }).then((result) => {
          /* Read more about isConfirmed, isDenied below */
          if (result.isConfirmed) {
            $.ajax({
                type: "POST", 
                url: "<?php echo base_url("rekening/delete/"); ?>" + id, 
                dataType: "json",
                beforeSend: function(){
                    Swal.fire({
                        title: 'Waits',
                        html: 'Deleting data in Progress',
                        allowOutsideClick : false,
                        showConfirmButton : false,
                        didOpen: () => {
                            Swal.showLoading()
                        }
                    });
                },
                success: function(response){ // Ketika proses pengiriman berhasil
                    swal.close();

                   if (response.status == 'success') {
                        message('success','Berhasil Dihapus!');
                         reloadTable();
                   } else {
                        warning('warning','Gagal Dihapus');
                   }

                    
                },
                error: function (xhr, ajaxOptions, thrownError) { // Ketika ada error
                    swal.close();
                   warning('warning','Ooppss Gagal Disimpan!!')
                }
            });
          } else if (result.isDenied) {
           warning('warning','Gagal Dihapus');
          }
        })
    }

</script>
