<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produksi_m extends CI_Model
{

    var $table = 'produksi a';
    var $column_order = array(null,null,'a.create_date','a.status_akad','a.statusData','a.nama_nasabah','a.no_aplikasi','b.branch_name','ff.asuransi','a.no_identitas','a.tenor','b.branch_name','a.tgl_akad','a.tglakhir_dariapi','e.perluasan','c.okupasi','d.kelas','a.plafond','a.rate','a.total_gross','a.fee_ujroh');
    var $column_search = array('a.create_date','a.status_akad','a.statusData','a.nama_nasabah','a.no_aplikasi','b.branch_name');
    var $order = array('a.create_date' => 'desc');

    function _get_datatables_query()
    {
        $this->db->select('a.*, b.nama, c.jenis_asuransi jenis_asuransina, d.nama namana');
        $this->db->from('produksi a');
        $this->db->join('users b','a.account_officer = b.id','left');
        $this->db->join('jenis_asuransi c','a.jenis_asuransi = c.id','left');
        $this->db->join('client d','a.tertanggung = d.id','left');

        if($this->input->post('jenis_asuransi_filter'))
        {
            $this->db->where('a.jenis_asuransi', $this->input->post('jenis_asuransi_filter'));
        }

        if($this->input->post('tertanggung_filter'))
        {
            $this->db->where('a.tertanggung', $this->input->post('tertanggung_filter'));
        }

        if ($this->input->post('dari') and $this->input->post('sampai')) {
            $darina = " '".$this->input->post('dari')."' ";
            $sampaina = " '".$this->input->post('sampai')."' ";
            $wherebetween = "a.tgl_nota between  $darina and $sampaina ";
            $this->db->where($wherebetween);
        } else if($this->input->post('dari')) {
            $darina = " '".$this->input->post('dari')."' ";
            $wherebetween = "a.tgl_nota =  $darina";
            $this->db->where($wherebetween);
        } else if($this->input->post('sampai')) {
            $sampaina = " '".$this->input->post('sampai')."' ";
            $wherebetween = "a.tgl_nota =  $sampaina";
            $this->db->where($wherebetween);
        } 

        $i = 0;

        foreach ($this->column_search as $item) 
        {
            if ($_POST['search']['value']) 
            {

                if ($i === 0) 
                {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }

        if (isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function getDataTable()
    {
        $this->_get_datatables_query();

        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->select('a.*, b.nama, c.jenis_asuransi jenis_asuransina, d.nama namana');
        $this->db->from('produksi a');
        $this->db->join('users b','a.account_officer = b.id','left');
        $this->db->join('jenis_asuransi c','a.jenis_asuransi = c.id','left');
        $this->db->join('client d','a.tertanggung = d.id','left');

        if($this->input->post('jenis_asuransi_filter'))
        {
            $this->db->where('a.jenis_asuransi', $this->input->post('jenis_asuransi_filter'));
        }

        if($this->input->post('tertanggung_filter'))
        {
            $this->db->where('a.tertanggung', $this->input->post('tertanggung_filter'));
        }

        if ($this->input->post('dari') and $this->input->post('sampai')) {
            $darina = " '".$this->input->post('dari')."' ";
            $sampaina = " '".$this->input->post('sampai')."' ";
            $wherebetween = "a.tgl_nota between  $darina and $sampaina ";
            $this->db->where($wherebetween);
        } else if($this->input->post('dari')) {
            $darina = " '".$this->input->post('dari')."' ";
            $wherebetween = "a.tgl_nota =  $darina";
            $this->db->where($wherebetween);
        } else if($this->input->post('sampai')) {
            $sampaina = " '".$this->input->post('sampai')."' ";
            $wherebetween = "a.tgl_nota =  $sampaina";
            $this->db->where($wherebetween);
        } 

        return $this->db->count_all_results();
    }

    function get_id($id)
    {
        $q = $this->db->query("select * from produksi where id = $id ")->row();

        return $q;
    }

    function invoice($id)
    {
        $q = $this->db->query("select a.*, b.jenis_asuransi jenis_asuransina, c.nama namana 
                          from produksi a 
                          join jenis_asuransi b on a.jenis_asuransi = b.id
                          join client c on a.tertanggung = c.id
                          where a.id = $id
            ")->row();

        return $q;
    }

   

}