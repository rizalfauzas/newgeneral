<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_m extends CI_Model
{

    function userna()
    {
        $q = $this->db->query("select a.*, b.groups from users a join groups b on a.id_groups = b.id");
        return $q;
    }

    function groups()
    {
        $q = $this->db->query("select * from groups ");
        return $q;
    }

    function status()
    {
        $q = $this->db->query("select * from status ");
        return $q;
    }

    function update($id,$data)
    {
        $this->db->where('id',$id);
        $this->db->update('users',$data);
        return $this->db->affected_rows();
    }

    function delete($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('users');
        return $this->db->affected_rows();
    }

    public function kodena()
    {
        $this->db->select('RIGHT(penutupan.reffnumber,3) as kode', FALSE);
        $this->db->order_by('id','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('penutupan');
            if($query->num_rows() <> 0){      
                 $data = $query->row();
                 $kode = intval($data->kode) + 1; 
            }
            else{      
                 $kode = 1;  
            }
        $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);


        $tahun = date('Y');
        $bulan = date('m');

        $kodetampil = 'PJM-MANUAL2022-'.$batas;
        return $kodetampil;  
    }


   
}